from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


app_name = 'profile'
urlpatterns = [
    path('', dashboard.index, name='home'),
    ################################################# PATH URL UNTUK ADMIN################################################# 
    path('manage_panel/', include(
        [
            path('', admin.index, name='admin_home'),
            path('verification/', user.verification, name='admin_verification'),
            path('send_verification/', user.send_verification, name='admin_send_verification'),
            path('email/verify/<uidb64>/<token>/',user.email_verify, name='admin_email_verify'),

            #DATA UMKM===============================
            path('penanggungjawab/', include(
                [
                    path('', penanggungjawab.admin_index, name='admin_penanggungjawab'),   
                    path('create/', penanggungjawab.create, name='admin_penanggungjawab_create'), 
                    path('edit/<int:id>', penanggungjawab.edit, name='admin_penanggungjawab_edit'),
                    path('soft_delete/<int:id>', penanggungjawab.softDelete, name='admin_penanggungjawab_soft_delete'),
                    path('restore/<int:id>', penanggungjawab.restore, name='admin_penanggungjawab_restore'),
                ]
            )),

            #DATA DISTRIK===============================
            path('distrik/', include(
                [
                    path('', distrik.admin_index, name='admin_distrik'),   
                    path('create/', distrik.create, name='admin_distrik_create'), 
                    path('edit/<int:id>', distrik.edit, name='admin_distrik_edit'),
                    path('soft_delete/<int:id>', distrik.softDelete, name='admin_distrik_soft_delete'),
                    path('restore/<int:id>', distrik.restore, name='admin_distrik_restore'),
                ]
            )),
            #DATA KAMPUNG===============================
            path('kampung/', include(
                [
                    path('', kampung.admin_index, name='admin_kampung'),   
                    path('create/', kampung.create, name='admin_kampung_create'), 
                    path('edit/<int:id>', kampung.edit, name='admin_kampung_edit'),
                    path('soft_delete/<int:id>', kampung.softDelete, name='admin_kampung_soft_delete'),
                    path('restore/<int:id>', kampung.restore, name='admin_kampung_restore'),
                ]
            )),
             #DATA UMKM===============================
            path('umkm/', include(
                [
                    path('', umkm.admin_index, name='admin_umkm'),   
                    path('create/', umkm.create, name='admin_umkm_create'), 
                    path('edit/<int:id>', umkm.edit, name='admin_umkm_edit'),
                    path('soft_delete/<int:id>', umkm.softDelete, name='admin_umkm_soft_delete'),
                    path('restore/<int:id>', umkm.restore, name='admin_umkm_restore'),
                ]
            )),
           
            #DATA KELEMBAGAAN===============================
            path('kelembagaan/', include(
                [
                    path('', lembaga.admin_index, name='admin_kelembagaan'),   
                    path('create/', lembaga.create, name='admin_kelembagaan_create'), 
                    path('edit/<int:id>', lembaga.edit, name='admin_kelembagaan_edit'),
                    path('soft_delete/<int:id>', lembaga.softDelete, name='admin_kelembagaan_soft_delete'),
                    path('restore/<int:id>', lembaga.restore, name='admin_kelembagaan_restore'),
                ]
            )),

            #DATA KOMINFO===============================
            path('kominfo/', include(
                [
                    path('', kominfo_.admin_index, name='admin_kominfo'),   
                    path('create/', kominfo_.create, name='admin_kominfo_create'), 
                    path('edit/<int:id>', kominfo_.edit, name='admin_kominfo_edit'),
                    path('soft_delete/<int:id>', kominfo_.softDelete, name='admin_kominfo_soft_delete'),
                    path('restore/<int:id>', kominfo_.restore, name='admin_kominfo_restore'),
                ]
            )),

            #DATA PU===============================
            path('pu/', include(
                [
                    path('', pu.admin_index, name='admin_pu'),   
                    path('create/', pu.create, name='admin_pu_create'), 
                    path('edit/<int:id>', pu.edit, name='admin_pu_edit'),
                    path('detail/<int:id>', pu.detail, name='admin_pu_detail'),
                    path('detail_/<int:id>', pu.detail_, name='admin_pu_detail_'),
                    path('soft_delete/<int:id>', pu.softDelete, name='admin_pu_soft_delete'),
                    path('restore/<int:id>', pu.restore, name='admin_pu_restore'),
                ]
            )),

            #DATA PERHUBUNGAN===============================
            path('perhubungan/', include(
                [
                    path('', perhubungan.admin_index, name='admin_perhubungan'),   
                    path('create/', perhubungan.create, name='admin_perhubungan_create'), 
                    path('edit/<int:id>', perhubungan.edit, name='admin_perhubungan_edit'),
                    path('soft_delete/<int:id>', perhubungan.softDelete, name='admin_perhubungan_soft_delete'),
                    path('restore/<int:id>', perhubungan.restore, name='admin_perhubungan_restore'),
                ]
            )),
            #DATA SOSIAL===============================
            path('sosial/', include(
                [
                    path('', sosial.admin_index, name='admin_sosial'),   
                    path('create/', sosial.create, name='admin_sosial_create'), 
                    path('edit/<int:id>', sosial.edit, name='admin_sosial_edit'),
                    path('soft_delete/<int:id>', sosial.softDelete, name='admin_sosial_soft_delete'),
                    path('restore/<int:id>', sosial.restore, name='admin_sosial_restore'),
                ]
            )),

            #DATA PERIKANAN===============================
            path('perikanan/', include(
                [
                    path('', perikanan.admin_index, name='admin_perikanan'),   
                    path('create/', perikanan.create, name='admin_perikanan_create'), 
                    path('edit/<int:id>', perikanan.edit, name='admin_perikanan_edit'),
                    path('edit_detail/<int:id>', perikanan.edit_kub, name='admin_perikanan_edit_detail'),
                    path('create_data/<int:id>', perikanan.detailPerikanan, name='admin_perikanan_create_data'),
                    path('soft_delete/<int:id>', perikanan.softDelete, name='admin_perikanan_soft_delete'),
                    path('soft_delete_detail/<int:id>', perikanan.softDelete_detail, name='admin_perikanan_soft_delete_detail'),
                    path('restore/<int:id>', perikanan.restore, name='admin_perikanan_restore'),
                    path('restore_detail/<int:id>', perikanan.restore_detail, name='admin_perikanan_restore_detail'),
                ]
            )),
            #DATA FASILITAS OLAHRAGA===============================
            path('fasilitasolahraga/', include(
                [
                    path('', fasilitasolahraga.admin_index, name='admin_fasilitas_olahraga'),   
                    path('create/', fasilitasolahraga.create, name='admin_fasilitas_olahraga_create'), 
                    path('edit/<int:id>', fasilitasolahraga.edit, name='admin_fasilitas_olahraga_edit'),
                    path('create_data/<int:id>', fasilitasolahraga.detailFasilitas, name='admin_fasilitas_olahraga_create_data'),
                    path('soft_delete/<int:id>', fasilitasolahraga.softDelete, name='admin_fasilitas_olahraga_soft_delete'),
                    path('soft_delete_detail/<int:id>', fasilitasolahraga.softDelete_detail, name='admin_fasilitas_olahraga_soft_delete_detail'),
                    path('restore/<int:id>', fasilitasolahraga.restore, name='admin_fasilitas_olahraga_restore'),
                    path('restore_detail/<int:id>', fasilitasolahraga.restore_detail, name='admin_fasilitas_olahraga_restore_detail'),
                    # save atlet
                    path('create_data_atlet/<int:id>', fasilitasolahraga.dataAtlet_create, name='admin_fasilitas_olahraga_create_data_atlet'),
                    path('soft_delete_atlet/<int:id>', fasilitasolahraga.softDelete_atlit, name='admin_fasilitas_olahraga_soft_delete_atlit'),
                    path('restore_atlit/<int:id>', fasilitasolahraga.restore_atlit, name='admin_fasilitas_olahraga_restore_atlit'),

                    path('edit_taruna/<int:id>', fasilitasolahraga.edit_karang_taruna, name='admin_taruna_edit'),
                    path('edit_data_atlet/<int:id>', fasilitasolahraga.edit_data_atlet, name='edit_data_atlet'),
                ]
            )),

            #DATA PERTANIAN===============================
            path('pertanian/', include(
                [
                    path('', pertanian.admin_index, name='admin_pertanian'),   
                    path('create/', pertanian.create, name='admin_pertanian_create'), 
                    path('edit/<int:id>', pertanian.edit, name='admin_pertanian_edit'),
                    path('create_data/<int:id>', pertanian.detailpertanian, name='admin_pertanian_create_data'),
                    path('edit_kelompok_tani/<int:id>', pertanian.edit_kelompok_tani, name='edit_data_kelompoktani'),
                    path('edit_komoditas_tani/<int:id>', pertanian.edit_komoditas_pertanian, name='edit_komoditas'),
                    path('soft_delete/<int:id>', pertanian.softDelete, name='admin_pertanian_soft_delete'),
                    path('soft_delete_detail/<int:id>', pertanian.softDelete_detail, name='admin_pertanian_soft_delete_detail'),
                    path('restore/<int:id>', pertanian.restore, name='admin_pertanian_restore'),
                    path('restore_detail/<int:id>', pertanian.restore_detail, name='admin_pertanian_restore_detail'),
                    # save atlet
                    path('create_data_komoditas/<int:id>', pertanian.dataKomoditas_create, name='admin_pertanian_create_data_komiditas'),
                    path('soft_delete_komoditas/<int:id>', pertanian.softDelete_atlit, name='admin_pertanian_soft_delete_komoditas'),
                    path('restore_komoditas/<int:id>', pertanian.restore_atlit, name='admin_pertanian_restore_komoditas'),
                ]
            )),                 
             #DATA KEPENDUDUKAN===============================
            path('kependudukan/', include(
                [
                    path('', kependudukan.admin_index, name='admin_kependudukan'),   
                    path('create/', kependudukan.create, name='admin_kependudukan_create'), 
                    path('edit/<int:id>', kependudukan.edit, name='admin_kependudukanedit'),
                    path('soft_delete/<int:id>', kependudukan.softDelete, name='admin_kependudukansoft_delete'),
                    path('restore/<int:id>', kependudukan.restore, name='admin_kependudukan_restore'),
                ]
            )),

            #DATA PEMERINTAHAN===============================
            path('pemerintahan/', include(
                [
                    path('', pemerintahan.admin_index, name='admin_pemerintahan'),   
                    path('create/', pemerintahan.create, name='admin_pemerintahan_create'), 
                    path('edit/<int:id>', pemerintahan.edit, name='admin_pemerintahanedit'),
                    path('soft_delete/<int:id>', pemerintahan.softDelete, name='admin_pemerintahansoft_delete'),
                    path('restore/<int:id>', pemerintahan.restore, name='admin_pemerintahan_restore'),
                ]
            )),
            
           #DATA KESEHATAN===============================
            path('kesehatan/', include(
                [
                    path('', kesehatan.admin_index, name='admin_kesehatan'),   
                    path('create/', kesehatan.create, name='admin_kesehatan_create'), 
                    path('create_posyandu/', kesehatan.create_posyandu, name='admin_kesehatan_create_posyandu'), 
                    path('edit/<int:id>', kesehatan.edit, name='admin_kesehatan_edit'),
                    path('edit_posyandu/<int:id>', kesehatan.edit_posyandu, name='admin_posyandu'),
                    path('soft_delete/<int:id>', kesehatan.softDelete, name='admin_kesehatansoft_delete'),
                    path('soft_delete_posyandu/<int:id>', kesehatan.softDelete_posyandu, name='admin_kesehatansoft_delete_posyandu'),
                    path('restore/<int:id>', kesehatan.restore, name='admin_kesehatan_restore'),
                    path('restore_posyandu/<int:id>', kesehatan.restore_posyandu, name='admin_kesehatan_restore_posyandu'),
                ]
            )),
            #DATA PERUMAHAN===============================
            path('perumahan/', include(
                [
                    path('', perumahan.admin_index, name='admin_perumahan'),   
                    path('create/', perumahan.create, name='admin_perumahan_create'), 
                    path('edit/<int:id>', perumahan.edit, name='admin_perumahan_edit'),
                    path('soft_delete/<int:id>', perumahan.softDelete, name='admin_perumahansoft_delete'),
                    path('restore/<int:id>', perumahan.restore, name='admin_perumahan_restore'),
                   
                ]
            )),

            #DATA PENDIDIKAN===============================
            path('pendidikan/', include(
                [
                    path('', pendidikan.admin_index, name='admin_pendidikan'),   
                    path('create/', pendidikan.create, name='admin_pendidikan_create'), 
                    path('create_detail/<int:id>', pendidikan.detail_pendidikan, name='admin_pendidikan_detail'),
                    path('create_detail_sd/<int:id>', pendidikan.detail_pendidikan_sd, name='admin_pendidikan_detail_sd'),
                    path('create_detail_smp/<int:id>', pendidikan.detail_pendidikan_smp, name='admin_pendidikan_detail_smp'),
                    path('edit/<int:id>', pendidikan.edit, name='admin_pendidikanedit'),
                    path('edit_paud/<int:id>', pendidikan.edit_paud, name='admin_edit_paud'),
                    path('edit_data_sd/<int:id>', pendidikan.edit_data_sd, name='admin_edit_data_sd'),
                    path('edit_data_smp/<int:id>', pendidikan.edit_data_smp, name='admin_edit_data_smp'),
                    
                    
                    
                    path('soft_delete/<int:id>', pendidikan.softDelete, name='admin_pendidikansoft_delete'),
                    path('restore/<int:id>', pendidikan.restore, name='admin_pendidikan_restore'),
                    path('soft_delete_paud/<int:id>', pendidikan.softDeletePaud, name='admin_pendidikan_softdelete_paud'),
                    path('restore_paud/<int:id>', pendidikan.restorePaud, name='admin_pendidikan_restore_paud'),

                    path('soft_delete_sd/<int:id>', pendidikan.softDeleteSD, name='admin_pendidikan_softdelete_sd'),
                    path('restore_sd/<int:id>', pendidikan.restoreSD, name='admin_pendidikan_restore_sd'),

                    path('soft_delete_smp/<int:id>', pendidikan.softDeleteSMP, name='admin_pendidikan_softdelete_smp'),
                    path('restore_smp/<int:id>', pendidikan.restoreSMP, name='admin_pendidikan_restore_smp'),
                   
                ]
            )),
            
           
           path('laporan/', include(
                [
                    path('', laporan.admin_index, name='laporan'),
                    path('laporan_pemerintahan_print/', laporan.print_pemerintahan, name='laporan_pemerintahan_filter'),
                    path('show_filter_pemerintah/', laporan.show_data_filter_pemerintah, name='show_filter_pemerintah'),
                    path('laporan_pemerintahan/', laporan.admin_index_pemerintahan, name='laporan_pemerintahan'),
                 
                 
                   
                   
                    
                   
                   
                   
                    path('laporan_perikanan/', laporan.admin_index__laporan_perikanan, name='laporan_perikanan'),
                    path('laporan_pertanian/', laporan.admin_index__laporan_pertanian, name='laporan_pertanian'),
                    path('laporan_fasilitas_olahraga/', laporan.admin_index__laporan_fasilitas_olahraga, name='laporan_fasilitas_olahraga'),
                    
                    path('laporan_pendidikan/', laporan.admin_index__laporan_pendidikan, name='laporan_pendidikan'),
                    path('laporan_print_pendidikan/', laporan.print_pendidikan, name='laporan_pendidikan_filter'),
                    path('show_filter_pendidikan/', laporan.show_data_filter_pendidikan, name='show_filter_pendidikan'),

                    path('laporan_kependudukan/', laporan.admin_index_kependudukan, name='laporan_kependudukan'),
                    path('laporan_print_kependudukan/', laporan.print_kependudukan, name='laporan_kependudukan_filter'),
                    path('show_filter_kependudukan/', laporan.show_data_filter_kependudukan, name='show_filter_kependudukan'),

                    path('laporan_kesehatan/', laporan.admin_index_kesehatan, name='laporan_kesehatan'),
                    path('laporan_print_kesehatan/', laporan.print_kesehatan, name='laporan_kesehatan_filter'),
                    path('show_filter_kesehatan/', laporan.show_data_filter_kesehatan, name='show_filter_kesehatan'),

                    path('laporan_sosial/', laporan.admin_index__laporan_sosial, name='laporan_sosial'),
                    path('laporan_print_sosial/', laporan.print_sosial, name='laporan_sosial_filter'),
                    path('show_filter_sosial/', laporan.show_data_filter_sosial, name='show_filter_sosial'),

                    path('laporan_pu/', laporan.admin_index__laporan_pu, name='laporan_pu'),
                    path('laporan_print_pu/', laporan.print_pu, name='laporan_pu_filter'),
                    path('show_filter_pu/', laporan.show_data_filter_pu, name='show_filter_pu'),

                    path('laporan_umkm/', laporan.admin_index__laporan_umkm, name='laporan_umkm'),
                    path('laporan_print_umkm/', laporan.print_umkm, name='laporan_umkm_filter'),
                    path('show_filter_umkm/', laporan.show_data_filter_umkm, name='show_filter_umkm'),

                    path('laporan_perhubungan/', laporan.admin_index__laporan_perhubungan, name='laporan_perhubungan'),
                    path('laporan_print_perhubungan/', laporan.print_perhubungan, name='laporan_perhubungan_filter'),
                    path('show_filter_perhubungan/', laporan.show_data_filter_perhubungan, name='show_filter_perhubungan'),

                    path('laporan_perumahan/', laporan.admin_index_perumahan, name='laporan_perumahan'),
                    path('laporan_print_perumahan/', laporan.print_perumahan, name='laporan_perumahan_filter'),
                    path('show_filter_perumahan/', laporan.show_data_filter_perumahan, name='show_filter_perumahan'),

                    path('laporan_kominfo/', laporan.admin_index__laporan_kominfo, name='laporan_kominfo'),
                    path('laporan_print_kominfo/', laporan.print_kominfo, name='laporan_kominfo_filter'),
                    path('show_filter_kominfo/', laporan.show_data_filter_kominfo, name='show_filter_kominfo'),

                    path('laporan_lembaga/', laporan.admin_index__laporan_kelembagaan, name='laporan_lembaga'),
                    path('laporan_print_kelembagaan/', laporan.print_kelembagaan, name='laporan_kelembagaan_filter'),
                    path('show_filter_kelembagaan/', laporan.show_data_filter_kelembagaan, name='show_filter_kelembagaan'),

                    
                ]
            )),       
           
           
           
           
            #LAYANAN PROFIL SKPD===============================
            path('profilskpd/', include(
                [
                    path('', profilskpd.admin_index, name='admin_profilskpd'),
                    path('create/', profilskpd.create, name='admin_profilskpd_create'),
                    path('edit/<int:id>', profilskpd.edit, name='admin_profilskpd_edit'),
                    path('soft_delete/<int:id>', profilskpd.softDelete, name='admin_profilskpd_soft_delete'),
                    path('restore/<int:id>', profilskpd.restore, name='admin_profilskpd_restore'),
                    
                ]
            )),

            #LAYANAN DINAS PENDIDIKAN===============================
            path('layanandinas/', include(
                [
                    path('', layanan_.admin_index, name='admin_layanan'),
                    path('create/', layanan_.create, name='admin_layanan_create'),
                    path('edit/<int:id>', layanan_.edit, name='admin_layanan_edit'),
                    path('soft_delete/<int:id>', layanan_.softDelete, name='admin_layanan_soft_delete'),
                    path('restore/<int:id>', layanan_.restore, name='admin_layanan_restore'),
                    
                ]
            )),

             #AKSESBILITAS EXTERNAL LINK DINAS PENDIDIKAN===============================
            path('aksesbilitas/', include(
                [
                    path('', aksesbilitas.admin_index, name='admin_aksesbilitas'),
                    path('create/', aksesbilitas.create, name='admin_aksesbilitas_create'),
                    path('edit/<int:id>', aksesbilitas.edit, name='admin_aksesbilitas_edit'),
                    path('soft_delete/<int:id>', aksesbilitas.softDelete, name='admin_aksesbilitas_soft_delete'),
                    path('restore/<int:id>', aksesbilitas.restore, name='admin_aksesbilitas_restore'),
                    
                ]
            )),

             #INFORMASI TERPADU DINAS PENDIDIKAN===============================
            path('infoterpadu/', include(
                [
                    # ROUTE INFOGRAFIS
                    path('infografis/', infoterpadu.admin_index_infografis, name='admin_infoterpadu_infografis'),
                    path('infografis/create/', infoterpadu.admin_index_infografis_create, name='admin_infoterpadu_infografis_cretae'),
                    # ROUTE INFOPUBLIK
                    path('infopublik/', infoterpadu.admin_index_infopublik, name='admin_infoterpadu_infopublik'),
                    path('infopublik/create/', infoterpadu.admin_index_infopublik_create, name='admin_infoterpadu_infopublik_cretae'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                    # ROUTE REGULASI
                    path('regulasi/', infoterpadu.admin_index_regulasi, name='admin_infoterpadu_regulasi'),
                    path('regulasi/create/', infoterpadu.admin_index_regulasi_create, name='admin_infoterpadu_regulasi_cretae'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                    # ROUTE KALENDER PENDIDIKAN
                    path('kalender_pendidikan/', infoterpadu.admin_index_kalender_pendidikan, name='admin_infoterpadu_kalender_pendidikan'),
                    path('kalender_pendidikan/create/', infoterpadu.admin_index_kalender_pendidikan_create, name='admin_kalender_pendidikan_create'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),
                     # ROUTE AGENDA
                    path('agenda/', infoterpadu.admin_index_agenda, name='admin_infoterpadu_agenda'),
                    path('agenda/create/', infoterpadu.admin_index_agenda_create, name='admin_infoterpadu_agenda_cretae'),
                    path('soft_delete/<int:id>', infoterpadu.softDelete, name='admin_infoterpadu_soft_delete'),
                    path('restore/<int:id>', infoterpadu.restore, name='admin_infoterpadu_restore'),

                ]
            )),
           
            # BERITA================================================
            path('news/', include(
                [
                    path('', news.admin_index, name='admin_news'),
                    path('create/', news.create, name='admin_news_create'),
                    path('edit/<str:slug>', news.edit, name='admin_news_edit'),
                    path('detail/<str:slug>', news.admin_detail, name='admin_news_detail'),
                    path('soft_delete/<str:slug>', news.softDelete, name='admin_news_soft_delete'),
                    path('permanent_delete/<str:slug>', news.permanentDelete, name='admin_news_permanent_delete'),
                    path('restore/<str:slug>', news.restore, name='admin_news_restore'),
                ]
            )),

            path('kata_mereka/', include(
                [
                    path('', kata.admin_index, name='admin_KM'),
                    path('create/', kata.create, name='admin_KM_create'),
                    path('edit/<int:id>', kata.edit, name='admin_KM_edit'),
                    path('detail/<int:id>', kata.admin_detail, name='admin_KM_detail'),
                    path('soft_delete/<int:id>', kata.softDelete, name='admin_KM_soft_delete'),
                    path('permanent_delete/<int:id>', kata.permanentDelete, name='admin_KM_permanent_delete'),
                    path('restore/<int:id>', kata.restore, name='admin_KM_restore'),
                ]
            )),
            
            # GALLERY
            path('galeri/', include(
                [
                    path('', gallery.admin_index, name='admin_gallery'),
                    path('create/', gallery.create, name='admin_gallery_create'),
                    path('edit/<str:slug>', gallery.edit, name='admin_gallery_edit'),
                    path('detail/<str:slug>', gallery.admin_detail, name='admin_gallery_detail'),
                    path('soft_delete/<str:slug>', gallery.softDelete, name='admin_gallery_soft_delete'),
                    path('permanent_delete/<str:slug>', gallery.permanentDelete, name='admin_gallery_permanent_delete'),
                    path('restore/<str:slug>', gallery.restore, name='admin_gallery_restore'),
                ]
            )),
            # USERS
            path('user/', include(
                [
                    path('', user.admin_index, name='admin_user'),
                    path('create/', user.create, name='admin_user_create'),
                    path('password/edit/', user.edit_password, name='admin_user_password_edit'),
                    path('profile/edit/<str:username>', user.edit_profile, name='admin_user_edit'),
                    path('avatar/edit/<str:username>', user.edit_avatar, name='admin_user_avatar_edit'),
                    path('detail/', user.admin_detail, name='admin_user_detail'),
                    path('soft_delete/<str:username>', user.softDelete, name='admin_user_soft_delete'),
                    path('permanent_delete/<str:slug>', user.permanentDelete, name='admin_user_permanent_delete'),
                    path('restore/<str:username>', user.restore, name='admin_user_restore'),
                    path('edit_role/', user.edit_role, name='admin_user_edit_role'),
                    path('non_aktif/<int:id>', user.non_aktif, name='non_aktif'),
                    path('re_aktif/<int:id>', user.re_aktif, name='aktif'),
                ]
            )),

            # SETTING APLIKASI
            path('setting/', include(
                [
                    # path('', user.admin_index, name='admin_setting'),
                    path('category/', setting.admin_index_category, name='admin_setting_category'),
                    path('category/edit/', setting.admin_index_category_edit, name='admin_setting_category_edit'),
                    path('running_text/', setting.admin_index_running_text, name='admin_setting_running_text'),
                    path('running_text/edit/', setting.admin_index_running_text_edit, name='admin_setting_running_text_edit'),
                    path('link/', setting.admin_index_link, name='admin_setting_link'),
                    path('links/edit/', setting.admin_index_link_edit, name='admin_setting_link_edit'),
                    path('infografis/', setting.admin_index_infografis, name='admin_setting_infografis'),
                    path('cara_bayar/', setting.admin_index_carabayar, name='admin_setting_carabayar'),
                    path('carousel', setting.admin_index_carousel, name='admin_setting_carousel'),
                    path('create/carousel/', setting.create_carousel, name='admin_setting_create_carousel'),
                    path('edit/carousel/<int:id>', setting.edit_carousel, name='admin_setting_edit_carousel'),
                    path('running_link/', setting.admin_index_running_link, name='admin_setting_running_link'),
                    path('create/running_link/', setting.create_running_link, name='admin_setting_create_running_link'),
                    path('edit/running_link/<int:id>', setting.edit_running_link, name='admin_setting_edit_running_link'),
                    path('pages/', setting.admin_index_pages, name='admin_setting_pages'),
                    path('create/pages/', setting.create_pages, name='admin_setting_create_pages'),
                    path('edit/pages/<str:slug>', setting.edit_pages, name='admin_setting_edit_pages'),
                    path('detail/pages/<str:slug>', setting.detail_pages, name='admin_setting_detail_pages'),
                    path('soft_delete/<str:jenis>/<int:id>', setting.softDelete, name='admin_setting_soft_delete'),
                    path('permanent_delete/<str:jenis>/<int:id>', setting.permanentDelete, name='admin_setting_permanent_delete'),
                    path('restore/<str:jenis>/<int:id>', setting.restore, name='admin_setting_restore'),
                    path('me/<str:section>', setting.admin_index_aboutme, name='admin_setting_aboutme'),
                    path('create/me/<str:section>', setting.create_aboutme, name='admin_setting_create_aboutme'),
                    path('edit/me/<str:section>/<int:id>', setting.edit_aboutme, name='admin_setting_edit_aboutme'),
                    path('delete/<str:section>/<int:id>', setting.delete_aboutme, name='admin_setting_delete_aboutme'),
          
                ]
            )),
        ]
    ))
]