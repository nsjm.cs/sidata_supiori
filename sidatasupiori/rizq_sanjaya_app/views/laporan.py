from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
         'title' : 'Laporan - Laporan',
          'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/index.html', context)

def admin_index_pemerintahan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pemerintahan = dataPemerintahan.objects.prefetch_related('kampung').filter(status='Aktif')
    context = {
         'title' : 'Laporan Pemerintahan',
         'datakampung_list' : kampung,
         'penanggungjawab':penanggungjawabs,
         'dt_pemerintahan_list' : dt_pemerintahan,
    }
    
    return render(request, 'profile/admin/laporan/laporan_pemerintah.html', context)

def admin_index_kesehatan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_kesehatan = dataKesehatan.objects.prefetch_related('kampung').filter(status='Aktif')
    dt_posyandu = dataPosyandu.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
         'title' : 'Laporan Kesehatan',
         'datakampung_list' : kampung,
         'penanggungjawab':penanggungjawabs,
         'datakampung_list' : kampung,
            'penanggungjawab':penanggungjawabs,
            'dt_kesehatan_list' : dt_kesehatan,
            'dt_posyandu_list' : dt_posyandu,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_kesehatan.html', context)

def admin_index_kependudukan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_kependudukan = datakependudukan.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
         'title' : 'Laporan Kependudukan',
         'datakampung_list' : kampung,
         'penanggungjawab':penanggungjawabs,
         'dt_kependudukan_lis' : dt_kependudukan,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_kependudukan.html', context)


def admin_index__laporan_perikanan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    PERIKANAN = dataPerikanan.objects.prefetch_related('kampung').filter(status='Aktif')
    data_perikanan_kub = dataDetailPerikanan.objects.filter(status='Aktif',jenisinput='KUB')
    data_perikanan_budidaya = dataDetailPerikanan.objects.filter(status='Aktif',jenisinput='BUDIDAYA')

    context = {
        'title' : 'Data Perikanan',
        'PERIKANAN' : PERIKANAN,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'data_perikanan_kub' : data_perikanan_kub,
        'data_perikanan_budidaya' : data_perikanan_budidaya,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_perikanan.html', context)

def admin_index__laporan_pertanian(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pertanian = dataPertanian.objects.prefetch_related('kampung').filter(status='Aktif')
    dt_kelompok_tani = dataKelompoktani.objects.filter(status='Aktif')
    dt_komoditias_tani = dataKomoditipertanian.objects.filter(status='Aktif')

    context = {
        'title' : 'Data Pertanian',
        'dt_pertanian' : dt_pertanian,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_kelompok_tani' : dt_kelompok_tani,
        'dt_komoditias_tani': dt_komoditias_tani,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_pertanian.html', context)

def admin_index__laporan_fasilitas_olahraga(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    fasilitasolahraga = dataFasilitasolahraga.objects.prefetch_related('kampung').filter(status='Aktif')
    data_fasilitas_detail = dataDetailFasilitasolahraga.objects.filter(status='Aktif')
    data_fasilitas_atlet = dataAtlet.objects.filter(status='Aktif')
    

    context = {
        'title' : 'Data Fasilitas olah Raga',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'fasilitasolahraga' : fasilitasolahraga,
        'data_fasilitas_detail' : data_fasilitas_detail,
        'data_atlit': data_fasilitas_atlet,
        
    }
    
    return render(request, 'profile/admin/laporan/laporan_fasilitas_olahraga.html', context)

def admin_index__laporan_pendidikan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pendidikan = dataPendidikan.objects.prefetch_related('kampung').filter(status='Aktif')
    dt_paud = dataPendidikanPaud.objects.filter(status='Aktif')
    dt_sd = dataPendidikanSD.objects.filter(status='Aktif')
    dt_smp = dataPendidikanSMP.objects.filter(status='Aktif')
    

    context = {
        'title' : 'Data Pendidikan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_pendidikan_list' : dt_pendidikan,
        'loop_paud' : dt_paud,
        'loop_sd' :dt_sd,
        'loop_smp' :dt_smp,
        
    }
    
    return render(request, 'profile/admin/laporan/laporan_pendidikan.html', context)




@login_required
@is_verified()
def show_data_filter_pemerintah(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data.html', context)
@login_required
@is_verified()
def print_pemerintahan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        filterdata_pemerintahan = dataPemerintahan.objects.prefetch_related('kampung').filter(status='Aktif', tahun=param_tahun, kampung_id=param_kampung)
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        
        context = {
            'title': 'Laporan Pemerintahan',
            'datakampung_list': kampung,
            'dt_pemerintahan_list': filterdata_pemerintahan,
        }
        
        return render(request, 'profile/admin/laporan/laporan_pemerintah.html', context)
    
@login_required
@is_verified()
def show_data_filter_pendidikan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_pendidikan.html', context)
@login_required
@is_verified()
def print_pendidikan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        dt_pendidikan = dataPendidikan.objects.prefetch_related('kampung').filter(status='Aktif', tahun=param_tahun, kampung_id=param_kampung)
        dt_paud = dataPendidikanPaud.objects.filter(status='Aktif')
        dt_sd = dataPendidikanSD.objects.filter(status='Aktif')
        dt_smp = dataPendidikanSMP.objects.filter(status='Aktif')
        

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'dt_pendidikan_list' : dt_pendidikan,
            'loop_paud' : dt_paud,
            'loop_sd' :dt_sd,
            'loop_smp' :dt_smp,
            
        }
        return render(request, 'profile/admin/laporan/laporan_pendidikan.html', context)
    
@login_required
@is_verified()
def show_data_filter_kesehatan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_kesehatan.html', context)

@login_required
@is_verified()
def print_kesehatan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        dt_kesehatan = dataKesehatan.objects.prefetch_related('kampung').filter(status='Aktif' , tahun=param_tahun, kampung_id = param_kampung)
        dt_posyandu = dataPosyandu.objects.prefetch_related('kampung').filter(status='Aktif' , tahun=param_tahun, kampung_id = param_kampung)

        context = {
            'title' : 'Laporan Kesehatan',
            'datakampung_list' : kampung,
            'dt_kesehatan_list' : dt_kesehatan,
            'dt_posyandu_list' : dt_posyandu,
                
        }
        
        return render(request, 'profile/admin/laporan/laporan_kesehatan.html', context)
@login_required
@is_verified()
def show_data_filter_kependudukan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_kependudukan.html', context)

@login_required
@is_verified()
def print_kependudukan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        dt_kependudukan = datakependudukan.objects.prefetch_related('kampung').filter(status='Aktif',tahun=param_tahun, kampung_id = param_kampung)
        

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'dt_kependudukan_lis' : dt_kependudukan,
            
        }
        return render(request, 'profile/admin/laporan/laporan_kependudukan.html', context)
    
@login_required
@is_verified()
def show_data_filter_fasilitas_olahraga(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data.html', context)

@login_required
@is_verified()
def show_data_filter_pertanian(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data.html', context)

@login_required
@is_verified()
def show_data_filter_perikanan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data.html', context)

def admin_index_perumahan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_perumahan = dataPerumahan.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
         'title' : 'Laporan Perumahan',
         'datakampung_list' : kampung,
         'penanggungjawab':penanggungjawabs,
         'dt_perumahan_list' : dt_perumahan,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_perumahan.html', context)


@login_required
@is_verified()
def show_data_filter_perumahan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_perumahan.html', context)

def admin_index__laporan_perhubungan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_perhubungan = dataPerhubungan.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
        'title' : 'Laporan Perhubungan',
        'data_perhubungan_list' : data_perhubungan,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_perhubungan.html', context)
@login_required
@is_verified()
def print_perumahan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        dt_perumahan = dataPerumahan.objects.prefetch_related('kampung').filter(status='Aktif',tahun= param_tahun, kampung_id = param_kampung)
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'dt_perumahan_list' : dt_perumahan,
            
        }
        return render(request, 'profile/admin/laporan/laporan_perumahan.html', context)

@login_required
@is_verified()
def show_data_filter_perhubungan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_perhubungan.html', context)

@login_required
@is_verified()
def print_perhubungan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        data_perhubungan = dataPerhubungan.objects.prefetch_related('kampung').filter(status='Aktif',tahun= param_tahun, kampung_id = param_kampung)
        
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'data_perhubungan_list' : data_perhubungan,
            
        }
        return render(request, 'profile/admin/laporan/laporan_perhubungan.html', context)

def admin_index__laporan_sosial(request):
   
    data_sosial = dataSosial.objects.prefetch_related('kampung').filter(status='Aktif')
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()

    context = {
         'title' : 'Laporan Sosial',
         'data_sosial_list' : data_sosial,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_sosial.html', context)

@login_required
@is_verified()
def print_sosial(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        data_sosial = dataSosial.objects.prefetch_related('kampung').filter(status='Aktif',tahun= param_tahun, kampung_id = param_kampung)
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'data_sosial_list' : data_sosial,
            
        }
        return render(request, 'profile/admin/laporan/laporan_sosial.html', context)
    
@login_required
@is_verified()
def show_data_filter_sosial(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_sosial.html', context)

def admin_index__laporan_umkm(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    UMKM = dataUMKM.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
        'title' : 'Laporan UMKM',
        'UMKM' : UMKM,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_umkm.html', context)

@login_required
@is_verified()
def show_data_filter_umkm(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_umkm.html', context)

@login_required
@is_verified()
def print_umkm(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        UMKM = dataUMKM.objects.prefetch_related('kampung').filter(status='Aktif', tahun = param_tahun, kampung_id = param_kampung)
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'UMKM' : UMKM,
            
        }
        return render(request, 'profile/admin/laporan/laporan_umkm.html', context)
    

def admin_index__laporan_kominfo(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_kominfo = dataKominfo.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
        'title' : 'Laporan Kominfo',
        'data_kominfo_list' : data_kominfo,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_kominfo.html', context)
@login_required
@is_verified()
def print_kominfo(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        
        data_kominfo = dataKominfo.objects.prefetch_related('kampung').filter(status='Aktif' , tahun = param_tahun, kampung_id = param_kampung)
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'data_kominfo_list' : data_kominfo,
            
        }
        return render(request, 'profile/admin/laporan/laporan_kominfo.html', context)
    
@login_required
@is_verified()
def show_data_filter_kominfo(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_kominfo.html', context)


def admin_index__laporan_pu(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_pu = dataPU.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
        'title' : 'Data PU',
        'data_pu_list' : data_pu,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_pu.html', context)

@login_required
@is_verified()
def show_data_filter_pu(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_pu.html', context)

@login_required
@is_verified()
def print_pu(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        data_pu = dataPU.objects.prefetch_related('kampung').filter(status='Aktif', tahun = param_tahun, kampung_id = param_kampung)
    

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'data_pu_list' : data_pu,
            
        }
        return render(request, 'profile/admin/laporan/laporan_pu.html', context)
    
@login_required
@is_verified()
def show_data_filter_kelembagaan(request):
    kampung = dataKampung.objects.select_related('distrik').all()
    context = {
        'title' : 'Filter Data',
        'datakampung_list' : kampung,
    }
    
    return render(request, 'profile/admin/laporan/filter_data_kelembagaan.html', context)

def admin_index__laporan_kelembagaan(request):
   
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    lembaga_ = dataKelembagaan.objects.prefetch_related('kampung').filter(status='Aktif')

    context = {
        'title' : 'Laporan Kelembagaan',
        'lembaga_list' : lembaga_,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
            
    }
    
    return render(request, 'profile/admin/laporan/laporan_kelembagaan.html', context)

@login_required
@is_verified()
def print_kelembagaan(request):

    if request.method == 'POST':
        param_tahun = request.POST.get('tahun')
        param_kampung = request.POST.get('kampung')
        kampung = dataKampung.objects.select_related('distrik').filter(id=param_kampung)
        lembaga_ = dataKelembagaan.objects.prefetch_related('kampung').filter(status='Aktif' , tahun = param_tahun, kampung_id = param_kampung)

        context = {
            'title' : 'Data Pendidikan',
            'datakampung_list' : kampung,
            'lembaga_list' : lembaga_,
            
        }
        return render(request, 'profile/admin/laporan/laporan_kelembagaan.html', context)








