# Generated by Django 3.2 on 2023-10-22 18:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0033_datakesehatan_namapuskesmas'),
    ]

    operations = [
        migrations.RenameField(
            model_name='datakesehatan',
            old_name='jml_balita_dipantau',
            new_name='jml_kematian_ibu_melahirkan',
        ),
        migrations.RemoveField(
            model_name='datakesehatan',
            name='jml_ibu_hamil_pantau',
        ),
        migrations.RemoveField(
            model_name='datakesehatan',
            name='jml_kematian_ibu_hamil_melahirkan',
        ),
    ]
