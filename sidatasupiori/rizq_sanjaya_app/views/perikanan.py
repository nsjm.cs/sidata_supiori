from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)

    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    PERIKANAN = dataPerikanan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPerikanan.objects.filter(status='Tidak aktif')
    paginator = Paginator(PERIKANAN, 15)
    try:
        PERIKANAN = paginator.page(page)
    except PageNotAnInteger:
        PERIKANAN = paginator.page(1)
    except EmptyPage:
        PERIKANAN = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data PERIKANAN',
        'PERIKANAN' : PERIKANAN,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/perikanan/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataPerikanan.objects.all()
        data_perikanan = dataPerikanan.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        id = data_perikanan = dataPerikanan.objects.order_by('-id')[:1]
        context = {
            'title' : 'Data Perikanan',
            'form' : form,
            'data_perikanan':data_perikanan,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
            'lates_id':id
        }

        template = 'profile/admin/perikanan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        kodeperikanan = request.POST.get('kodeperikanan')
        jml_rt_nelayan = request.POST.get('jml_rt_nelayan')
        jml_kartu_nelayan = request.POST.get('jml_kartu_nelayan')
        jml_rtp = request.POST.get('jml_rtp')
       
        if kampung is not None:
            data_perikanan = dataPerikanan()
            data_perikanan.tahun = tahun
            data_perikanan.penanggungjawab_id = penanggungjawab_id
            data_perikanan.kampung_id = kampung
            data_perikanan.kodeperikanan = kodeperikanan
            data_perikanan.jml_rt_nelayan = jml_rt_nelayan
            data_perikanan.jml_kartu_nelayan = jml_kartu_nelayan
            data_perikanan.jml_rtp = jml_rtp
            

            data_perikanan.save()
            messages.success(request, 'Data perikanan berhasil disimpan.')
            return redirect('profile:admin_perikanan_create_data',kodeperikanan)
        messages.error(request, 'Data perikanan gagal disimpan.')
        return render(request, 'profile/admin/perikanan/create.html', {'form': form,})


@login_required
@is_verified()
def detailPerikanan(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_perikanan = dataPerikanan.objects.get(id = id)
        data_perikanan_kub = dataDetailPerikanan.objects.filter(kodeperikanan = id,status='Aktif',jenisinput='KUB')
        data_perikanan_budidaya = dataDetailPerikanan.objects.filter(kodeperikanan = id,status='Aktif',jenisinput='BUDIDAYA')
        data_perikanan_arsip = dataDetailPerikanan.objects.filter(status='Tidak aktif')
        
        context = {
            'title' : 'DETAIL',
            'form' : form,
            'edit' : 'true',
            'data_perikanan' : data_perikanan,
            'data_perikanan_kub' : data_perikanan_kub,
            'data_perikanan_budidaya' : data_perikanan_budidaya,
            'data_perikanan_arsip' : data_perikanan_arsip,
        }
        template = 'profile/admin/perikanan/create_data.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        kodeperikanan = request.POST.get('kodeperikanan')
        perikanan = request.POST.get('perikanan')
        namakelompok = request.POST.get('namakelompok')
        jenisinput = request.POST.get('jenisinput')
        ketua = request.POST.get('ketua')
        bantuan = request.POST.get('bantuan')
        sk_pelatihan = request.POST.get('sk_pelatihan')
        foto = request.FILES['foto']

        if kodeperikanan is not None:
            data_perikanan = dataDetailPerikanan()
            data_perikanan.kodeperikanan = kodeperikanan
            data_perikanan.perikanan_id = perikanan
            data_perikanan.namakelompok = namakelompok
            data_perikanan.jenisinput = jenisinput
            data_perikanan.ketua = ketua
            data_perikanan.bantuan = bantuan
            data_perikanan.sk_pelatihan = sk_pelatihan
            data_perikanan.foto = foto
            
            data_perikanan.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_perikanan_create_data',kodeperikanan)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/perikanan/create_data.html',kodeperikanan, {'form': form,})
# arsip datadetail
@login_required
@is_verified()
def softDelete_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataDetailPerikanan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataDetailPerikanan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataDetailPerikanan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataDetailPerikanan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
   

@login_required
@is_verified()
def edit(request, id):

    edit_dataperikanan = get_object_or_404(dataPerikanan, id=id)
    form = ''

    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        kodeperikanan = request.POST.get('kodeperikanan')
        jml_rt_nelayan = request.POST.get('jml_rt_nelayan')
        jml_kartu_nelayan = request.POST.get('jml_kartu_nelayan')
        jml_rtp = request.POST.get('jml_rtp')

        if edit_dataperikanan is not None:
            edit_dataperikanan.tahun = tahun
            edit_dataperikanan.penanggungjawab_id = penanggungjawab_id
            edit_dataperikanan.kampung_id = kampung
            edit_dataperikanan.kodeperikanan = kodeperikanan
            edit_dataperikanan.jml_rt_nelayan = jml_rt_nelayan
            edit_dataperikanan.jml_kartu_nelayan = jml_kartu_nelayan
            edit_dataperikanan.jml_rtp = jml_rtp
            edit_dataperikanan.save()
            

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_perikanan')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

@login_required
@is_verified()
def edit_kub(request, id):

    edit_dataperikanan_kub = get_object_or_404(dataDetailPerikanan, id=id)
    form = ''

    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataDetailPerikanan.foto}"
        foto_old = bool(dataDetailPerikanan.foto)
        image = request.FILES.get('foto')

        kodeperikanan = request.POST.get('kodeperikanan')
        namakelompok = request.POST.get('namakelompok')
        jenisinput = request.POST.get('jenisinput')
        ketua = request.POST.get('ketua')
        bantuan = request.POST.get('bantuan')
        sk_pelatihan = request.POST.get('sk_pelatihan')
       
       
        

        if edit_dataperikanan_kub is not None:
            edit_dataperikanan_kub.kodeperikanan = kodeperikanan
            edit_dataperikanan_kub.namakelompok = namakelompok
            edit_dataperikanan_kub.jenisinput = jenisinput
            edit_dataperikanan_kub.ketua = ketua
            edit_dataperikanan_kub.bantuan = bantuan
            edit_dataperikanan_kub.sk_pelatihan = sk_pelatihan
            
            edit_dataperikanan_kub.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataDetailPerikanan.objects.get(id = id)
                form_foto.foto = image
                form_foto.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_perikanan_create_data',kodeperikanan)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerikanan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPerikanan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerikanan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPerikanan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)