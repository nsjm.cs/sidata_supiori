from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    fasilitasolahraga = dataFasilitasolahraga.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataFasilitasolahraga.objects.filter(status='Tidak aktif')
    paginator = Paginator(fasilitasolahraga, 15)
    try:
        fasilitasolahraga = paginator.page(page)
    except PageNotAnInteger:
        fasilitasolahraga = paginator.page(1)
    except EmptyPage:
        fasilitasolahraga = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data fasilitas olahraga',
        'fasilitasolahraga' : fasilitasolahraga,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/fasilitasolahraga/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataFasilitasolahraga.objects.all()
        data_fasilitas = dataFasilitasolahraga.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        id =  dataFasilitasolahraga.objects.order_by('-id')[:1]
        context = {
            'title' : 'Data Fasilitas',
            'form' : form,
            'data_fasilitas':data_fasilitas,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
            'lates_id':id
        }

        template = 'profile/admin/fasilitasolahraga/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        kodefasilitas = request.POST.get('kodefasilitas')
        jml_lapangan_voly = request.POST.get('jml_lapangan_voly')
        jml_lapangan_bola = request.POST.get('jml_lapangan_bola')
        jml_lapangan_basket = request.POST.get('jml_lapangan_basket')
        jml_lapangan_takraw = request.POST.get('jml_lapangan_takraw')
        jml_lapangan_bulutangkis = request.POST.get('jml_lapangan_bulutangkis')
        foto_jml_lapangan_voly 		=  request.FILES['foto_jml_lapangan_voly']
        foto_jml_lapangan_bola 		=  request.FILES['foto_jml_lapangan_bola']
        foto_jml_lapangan_basket 		=  request.FILES['foto_jml_lapangan_basket']
        foto_jml_lapangan_takraw 		=  request.FILES['foto_jml_lapangan_takraw']
        foto_jml_lapangan_bulutangkis 		=  request.FILES['foto_jml_lapangan_bulutangkis']
            

       
        if kampung is not None:
            data_fasilitas_ = dataFasilitasolahraga()
            data_fasilitas_.tahun = tahun
            data_fasilitas_.penanggungjawab_id = penanggungjawab_id
            data_fasilitas_.kampung_id = kampung
            data_fasilitas_.kodefasilitas = kodefasilitas
            data_fasilitas_.jml_lapangan_voly = jml_lapangan_voly
            data_fasilitas_.jml_lapangan_bola = jml_lapangan_bola
            data_fasilitas_.jml_lapangan_basket = jml_lapangan_basket
            data_fasilitas_.jml_lapangan_takraw = jml_lapangan_takraw
            data_fasilitas_.jml_lapangan_bulutangkis = jml_lapangan_bulutangkis
            data_fasilitas_.foto_jml_lapangan_voly = foto_jml_lapangan_voly
            data_fasilitas_.foto_jml_lapangan_bola = foto_jml_lapangan_bola
            data_fasilitas_.foto_jml_lapangan_basket = foto_jml_lapangan_basket
            data_fasilitas_.foto_jml_lapangan_takraw = foto_jml_lapangan_takraw
            data_fasilitas_.foto_jml_lapangan_bulutangkis = foto_jml_lapangan_bulutangkis
            

            data_fasilitas_.save()
            messages.success(request, 'Data fasilitas berhasil disimpan.')
            return redirect('profile:admin_fasilitas_olahraga_create_data',kodefasilitas)
        messages.error(request, 'Data fasilitas gagal disimpan.')
        return render(request, 'profile/admin/fasilitasolahraga/create.html', {'form': form,})


@login_required
@is_verified()
def detailFasilitas(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_fasilitas = dataFasilitasolahraga.objects.get(id = id)
        data_fasilitas_detail = dataDetailFasilitasolahraga.objects.filter(kodefasilitas = id,status='Aktif')
        data_arsip_fasilitas_detail = dataDetailFasilitasolahraga.objects.filter(kodefasilitas = id,status='Tidak aktif')
        data_fasilitas_atlet = dataAtlet.objects.filter(kodefasilitas = id,status='Aktif')
        data_arsip_fasilitas_atlit = dataAtlet.objects.filter(kodefasilitas = id,status='Tidak aktif')
        
        context = {
            'title' : 'DETAIL',
            'form' : form,
            'edit' : 'true',
            'data_fasilitas' : data_fasilitas,
            'data_fasilitas_detail' : data_fasilitas_detail,
            'data_arsip_fasilitas_detail': data_arsip_fasilitas_detail,
            'data_atlit': data_fasilitas_atlet,
            'data_atlit_arsip': data_arsip_fasilitas_atlit,
            
            
        }
        template = 'profile/admin/fasilitasolahraga/create_data.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        kodefasilitas = request.POST.get('kodefasilitas')
        fasilitas = request.POST.get('fasilitas')
        namakelompok = request.POST.get('namakelompok')
        ketua = request.POST.get('ketua')
        jml_anggota = request.POST.get('jml_anggota')
        pelatihan = request.POST.get('pelatihan')
        

        if kodefasilitas is not None:
            data_detail_fasilitas = dataDetailFasilitasolahraga()
            data_detail_fasilitas.kodefasilitas = kodefasilitas
            data_detail_fasilitas.fasilitas_id = fasilitas
            data_detail_fasilitas.namakelompok = namakelompok
            data_detail_fasilitas.ketua = ketua
            data_detail_fasilitas.jml_anggota = jml_anggota
            data_detail_fasilitas.pelatihan = pelatihan
            
            data_detail_fasilitas.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_fasilitas_olahraga_create_data',kodefasilitas)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/fasilitasolahraga/create_data.html',kodefasilitas, {'form': form,})
#save data atlet
@login_required
@is_verified()
def dataAtlet_create(request, id):

    if request.method == 'POST':
       
        kodefasilitas = request.POST.get('kodefasilitas')
        fasilitas = request.POST.get('fasilitas')
        nama = request.POST.get('nama')
        event = request.POST.get('event')
        prestasi = request.POST.get('prestasi')
        

        if kodefasilitas is not None:
            data_detail_atlit = dataAtlet()
            data_detail_atlit.kodefasilitas = kodefasilitas
            data_detail_atlit.fasilitas_id = fasilitas
            data_detail_atlit.nama = nama
            data_detail_atlit.event = event
            data_detail_atlit.prestasi = prestasi
            
            data_detail_atlit.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_fasilitas_olahraga_create_data',kodefasilitas)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/fasilitasolahraga/create_data.html',kodefasilitas)



# arsip datadetail
@login_required
@is_verified()
def softDelete_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataDetailFasilitasolahraga.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataDetailFasilitasolahraga.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataDetailFasilitasolahraga.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataDetailFasilitasolahraga.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

# atlit
# arsip datadetail
@login_required
@is_verified()
def softDelete_atlit(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataAtlet.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataAtlet.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_atlit(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataAtlet.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataAtlet.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
   

@login_required
@is_verified()
def edit(request, id):
    edit_datakefasilitas_olahrg = get_object_or_404(dataFasilitasolahraga, id=id)
    form = ''
    
    
    if request.method == 'POST':

        # voly
        path_file_lama_voly = f"{settings.MEDIA_ROOT}/{dataFasilitasolahraga.foto_jml_lapangan_voly}"
        foto_old_voly = bool(dataFasilitasolahraga.foto_jml_lapangan_voly)
        image_voly = request.FILES.get('foto_jml_lapangan_voly')

        # bola
        path_file_lama_bola = f"{settings.MEDIA_ROOT}/{dataFasilitasolahraga.foto_jml_lapangan_bola}"
        foto_old_bola = bool(dataFasilitasolahraga.foto_jml_lapangan_bola)
        image_bola = request.FILES.get('foto_jml_lapangan_bola')

        # basket
        path_file_lama_basket = f"{settings.MEDIA_ROOT}/{dataFasilitasolahraga.foto_jml_lapangan_basket}"
        foto_old_basket = bool(dataFasilitasolahraga.foto_jml_lapangan_basket)
        image_basket = request.FILES.get('foto_jml_lapangan_basket')

        # takraw
        path_file_lama_tkw = f"{settings.MEDIA_ROOT}/{dataFasilitasolahraga.foto_jml_lapangan_takraw}"
        foto_old_tkw = bool(dataFasilitasolahraga.foto_jml_lapangan_takraw)
        image_tkw = request.FILES.get('foto_jml_lapangan_takraw')

        # foto_jml_lapangan_bulutangkis
        path_file_lama_bt = f"{settings.MEDIA_ROOT}/{dataFasilitasolahraga.foto_jml_lapangan_bulutangkis}"
        foto_old_bt = bool(dataFasilitasolahraga.foto_jml_lapangan_bulutangkis)
        image_bt = request.FILES.get('foto_jml_lapangan_bulutangkis')

        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        kodefasilitas = request.POST.get('kodefasilitas')
        jml_lapangan_voly = request.POST.get('jml_lapangan_voly')
        jml_lapangan_bola = request.POST.get('jml_lapangan_bola')
        jml_lapangan_basket = request.POST.get('jml_lapangan_basket')
        jml_lapangan_takraw = request.POST.get('jml_lapangan_takraw')
        jml_lapangan_bulutangkis = request.POST.get('jml_lapangan_bulutangkis')

        if edit_datakefasilitas_olahrg is not None:
            edit_datakefasilitas_olahrg.tahun                 = tahun
            edit_datakefasilitas_olahrg.penanggungjawab_id    = penanggungjawab_id
            edit_datakefasilitas_olahrg.kampung_id            = kampung

            edit_datakefasilitas_olahrg.kodefasilitas = kodefasilitas
            edit_datakefasilitas_olahrg.jml_lapangan_voly = jml_lapangan_voly
            edit_datakefasilitas_olahrg.jml_lapangan_bola = jml_lapangan_bola
            edit_datakefasilitas_olahrg.jml_lapangan_basket = jml_lapangan_basket
            edit_datakefasilitas_olahrg.jml_lapangan_takraw = jml_lapangan_takraw
            edit_datakefasilitas_olahrg.jml_lapangan_bulutangkis = jml_lapangan_bulutangkis
            

            edit_datakefasilitas_olahrg.save()
            if len(request.FILES) > 0:
                if foto_old_voly : 
                    try:
                        os.remove(path_file_lama_voly)
                    except:
                        pass
                form_foto = dataFasilitasolahraga.objects.get(id = id)
                form_foto.foto_jml_lapangan_voly = image_voly
                form_foto.save()

            if len(request.FILES) > 0:
                if foto_old_bola : 
                    try:
                        os.remove(path_file_lama_bola)
                    except:
                        pass
                form_foto = dataFasilitasolahraga.objects.get(id = id)
                form_foto.foto_jml_lapangan_bola = image_bola
                form_foto.save()

            if len(request.FILES) > 0:
                if foto_old_basket : 
                    try:
                        os.remove(path_file_lama_basket)
                    except:
                        pass
                form_foto = dataFasilitasolahraga.objects.get(id = id)
                form_foto.foto_jml_lapangan_basket = image_basket
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_tkw : 
                    try:
                        os.remove(path_file_lama_tkw)
                    except:
                        pass
                form_foto = dataFasilitasolahraga.objects.get(id = id)
                form_foto.foto_jml_lapangan_takraw = image_tkw
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_bt : 
                    try:
                        os.remove(path_file_lama_bt)
                    except:
                        pass
                form_foto = dataFasilitasolahraga.objects.get(id = id)
                form_foto.foto_jml_lapangan_bulutangkis = image_bt
                form_foto.save()

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_fasilitas_olahraga')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataFasilitasolahraga.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataFasilitasolahraga.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataFasilitasolahraga.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataFasilitasolahraga.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)


@login_required
@is_verified()
def edit_karang_taruna(request, id):
    edit_data_karangtaruna = get_object_or_404(dataDetailFasilitasolahraga, id=id)
    form = ''
    
    
    if request.method == 'POST':
        kodefasilitas = request.POST.get('kodefasilitas')
        namakelompok = request.POST.get('namakelompok')
        ketua = request.POST.get('ketua')
        jml_anggota = request.POST.get('jml_anggota')
        pelatihan = request.POST.get('pelatihan')

        if edit_data_karangtaruna is not None:

            edit_data_karangtaruna.namakelompok = namakelompok
            edit_data_karangtaruna.ketua = ketua
            edit_data_karangtaruna.jml_anggota = jml_anggota
            edit_data_karangtaruna.pelatihan = pelatihan

            edit_data_karangtaruna.save()
            

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_fasilitas_olahraga_create_data',kodefasilitas)

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit_data_atlet(request, id):
    edit_data_atlit = get_object_or_404(dataAtlet, id=id)
    form = ''
    
    
    if request.method == 'POST':
        kodefasilitas = request.POST.get('kodefasilitas')
        nama = request.POST.get('nama')
        event = request.POST.get('event')
        prestasi = request.POST.get('prestasi')
        

        if edit_data_atlit is not None:
            edit_data_atlit.nama = nama
            edit_data_atlit.event = event
            edit_data_atlit.prestasi = prestasi
            edit_data_atlit.save()
            

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_fasilitas_olahraga_create_data',kodefasilitas)

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})
