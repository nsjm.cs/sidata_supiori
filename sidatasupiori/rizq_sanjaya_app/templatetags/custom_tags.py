from django import template

register = template.Library()

@register.filter
def lower(value):
    return value.lower()
    
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def batch(sequence, count):
    result = []
    for i in range(0, len(sequence), count):
        result.append(sequence[i:i + count])
    return result

register = template.Library()

@register.filter(name='capital_case')
def capital_case(value):
    words = value.split()
    return ' '.join(word.capitalize() for word in words)