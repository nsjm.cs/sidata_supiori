from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News, Category
from datetime import datetime
import os
from django.conf import settings
from ..decorators import *

from django.http import JsonResponse

from django.db import connections
from ..helpers import get_category, dictfetchall

@require_http_methods(["GET"])
def index(request):
	return detail(request, 'all')

@require_http_methods(["GET"])
def detail(request, jenis):
	if_jenis = jenis.lower()
	# dt_paket_detail = is_kode = ''
	# delayx = 100

	# if if_jenis == 'trail':
	# 	bg_paket = 'bg-paket-trail.jpg'
	# elif if_jenis == 'atv':
	# 	bg_paket = 'bg-paket-atv.jpg'
	# elif if_jenis == 'jeep':
	# 	bg_paket = 'bg-paket-jeep.jpg'
	# else:
	# 	bg_paket = 'bg-paket.jpg'

	if if_jenis in ['padi','coklat','kayu']:
		nm_layanan = 'Komoditas '+jenis.title()
	else:
		nm_layanan = 'Komoditas Lamban Tani'

	# try:
	# 	if if_jenis == 'all':
	# 		dt_paket = PaketWisata.objects.all().filter(status=True).order_by('id','jenis')
	# 	else:
	# 		dt_paket = PaketWisata.objects.filter(jenis=jenis, status=True).order_by('id')
	# except Exception as e:
	# 	dt_paket = ''

	# for x in dt_paket:
	# 	x.pesan = 'Halo Admin rizq_sanjaya mohon informasi Paket Wisata '+x.jenis.upper()+' ('+x.kode+'/'+x.nama+')'
	# 	x.kelas = 'featured' if x.is_favorit else ''
	# 	is_kode += ",'"+(x.kode)+"'"
	# 	x.delay = delayx
	# 	delayx = delayx + 100

	# if dt_paket :
	# 	with connections['default'].cursor() as cursor:
	# 		cursor.execute("SELECT *, case when is_aktiv then '' else 'na' end as kelas \
	# 			FROM rizq_sanjaya_app_paketwisata_detail WHERE kode in ("+is_kode[1:]+") ORDER BY id")
	# 		dt_paket_detail = dictfetchall(cursor)

# ==============================================================================================================

	# runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
	# for x in runlink:
	# 	x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

	context = {
		'title' : 'Layanan - '+nm_layanan,
		# 'dt_paket' : dt_paket,
		# 'dt_paket_det' : dt_paket_detail,
		# 'bg_paket' : bg_paket,
		'nm_layanan' : nm_layanan,
		# 'runlink' : runlink,
		# 'pagex' : if_jenis,
	}
	return render(request, 'profile/layanan/index.html', context)

@require_http_methods(["GET"])
def detail_komoditas(request, jenis):
	nm_layanan = 'Kelompok Tani '+jenis.title()
	context = {
		'title' : 'Komoditas - '+nm_layanan,
		# 'dt_paket' : dt_paket,
		# 'dt_paket_det' : dt_paket_detail,
		# 'bg_paket' : bg_paket,
		'nm_layanan' : nm_layanan,
		# 'runlink' : runlink,
		# 'pagex' : if_jenis,
	}
	return render(request, 'profile/layanan/detail.html', context)


#### BAGIAN ADMIN ===================================================================================================

@login_required
@is_verified()
@require_http_methods(["GET", "POST"])
def admin_index(request):
	### GET paket wisata induk =========================================
	if request.method == 'GET':
		with connections['default'].cursor() as cursor:
			cursor.execute("SELECT *, (select count(b.id) from rizq_sanjaya_app_paketwisata_detail b \
			where b.kode = a.kode) as child FROM rizq_sanjaya_app_paketwisata a ORDER BY a.kode, a.id")
			paket = dictfetchall(cursor)

		for x in paket:
			x['icon'] = 'bi bi-check-square' if x['is_favorit'] else 'bi bi-square'
			x['warna'] = 'green' if x['is_favorit'] else ''

		context = {
			'title' : 'Paket Wisata - Admin',
			'paket' : paket,
		}

		return render(request, 'profile/admin/paket/index.html', context)

	### POST paket wisata induk =========================================
	if request.method == 'POST':
		jenis = request.POST.get('jns').lower()

		with connections['default'].cursor() as cursor:
			cursor.execute("SELECT count(id)+1 as new FROM rizq_sanjaya_app_paketwisata WHERE jenis = %s ",[jenis])
			hasil = dictfetchall(cursor)[0]

		context = {'hasil':hasil['new']}
		return JsonResponse(context)


@login_required
@is_verified()
def admin_create(request):
	context = {} 
	form = ''
	template = 'profile/admin/paket/create.html'
	category = get_category('galeri')

	if request.method == 'GET':
		form = PaketWisataForm()
		context = {
			'title' : 'ADD Paket Wisata',
			'form' : form,
			'kategori' : category,
		}

		return render(request, template, context)

	if request.method == 'POST':
		print(request.POST)
		form = PaketWisataForm(data=request.POST)
		if form.is_valid():
			new_paket = form.save(commit=False)
			new_paket.save()
			messages.success(request, 'Data berhasil disimpan.')
			return redirect('profile:admin_paket')

		messages.error(request, 'Data gagal disimpan.')
		context = {
			'title' : 'ADD Paket Wisata',
			'form' : form,
			'kategori' : category, 
		} 
		return render(request, template, context)


@login_required
@is_verified()
def admin_edit(request, id):
	context = {} 
	form = ''
	template = 'profile/admin/paket/create.html'
	category = get_category('galeri')
	
	if request.method == 'GET':
		try:
			paket_get = PaketWisata.objects.get(id=id)
			form = PaketWisataForm(instance=paket_get)
		except:
			paket_get = None
			form = PaketWisataForm()
			
		context = {
			'title' : 'EDIT Paket Wisata',
			'form' : form,
			'edit' : 'true',
			'id' : id,
			'paket' : paket_get,
			'kategori' : category,
		}
		return render(request, template, context)
	
	if request.method == 'POST':
		paket_get = PaketWisata.objects.get(id=id)
		form = PaketWisataForm(data=request.POST, instance=paket_get)

		if form.is_valid():
			paket_update = form.save(commit=False)
			paket_update.kode = form.cleaned_data.get('kode')
			paket_update.nama = form.cleaned_data.get('nama')
			paket_update.harga = form.cleaned_data.get('harga')
			paket_update.durasi = form.cleaned_data.get('durasi')
			paket_update.jenis = form.cleaned_data.get('jenis')
			paket_update.is_favorit = form.cleaned_data.get('is_favorit')
			paket_update.status = form.cleaned_data.get('status')
			paket_update.is_idr = form.cleaned_data.get('is_idr')
			paket_update.save()

			messages.success(request, 'Data berhasil disimpan.')
			return redirect('profile:admin_paket')

		context = {
			'title' : 'EDIT Paket Wisata',
			'form' : form,
			'edit' : 'true',
			'id' : id,
			'paket' : paket_get,
			'kategori' : category,
		}
		messages.error(request, 'Data gagal disimpan.')
		return render(request, template, context)


@login_required
@is_verified()
def admin_Delete(request, id):
	message = ''

	try:
		PaketWisata_del = PaketWisata.objects.get(id=id)
		PaketWisata_del.delete()
		message = 'success'
	except PaketWisata.DoesNotExist:
		message = 'error'

	context = { 'message' : message, }
	return JsonResponse(context)


@login_required
@is_verified()
def admin_fasilitas(request, aksi, kode, id=0):

	form = ''
	is_edit = False if aksi.lower() == 'add' else True
	fasilitas = PaketWisata_Detail.objects.filter(kode=kode).order_by('id')

	for x in fasilitas:
		x.icon = 'bi bi-check-square' if x.is_aktiv else 'bi bi-square'
		x.warna = 'green' if x.is_aktiv else ''

	if request.method == 'GET':
		if aksi.lower() == 'add':
			form = PaketWisata_DetailForm()
		elif aksi.lower() == 'edit':
			try:
				fast_get = PaketWisata_Detail.objects.get(id=id)
				form = PaketWisata_DetailForm(instance=fast_get)
			except:
				fast_get = None
				form = PaketWisata_DetailForm()
		else:
			return redirect('profile:admin_fasilitas', aksi='add', kode=kode, id=0)


	if request.method == 'POST':
		if aksi.lower() == 'add':
			form = PaketWisata_DetailForm(data=request.POST)
			if form.is_valid():
				new_fast = form.save(commit=False)
				new_fast.save()

				messages.success(request, 'Data berhasil disimpan.')
			else:
				messages.error(request, 'Data gagal disimpan.')

		elif aksi.lower() == 'edit':
			fast_get = PaketWisata_Detail.objects.get(id=id)
			form = PaketWisata_DetailForm(data=request.POST, instance=fast_get)

			if form.is_valid():
				fast_update = form.save(commit=False)
				fast_update.deskripsi = form.cleaned_data.get('deskripsi')
				fast_update.is_aktiv = form.cleaned_data.get('is_aktiv')
				fast_update.save()

				messages.success(request, 'Data berhasil disimpan.')
			else:
				messages.error(request, 'Data gagal disimpan.')

		return redirect('profile:admin_fasilitas', aksi='add', kode=kode, id=0)

	context = {
		'title' : 'Fasilitas Paket Wisata - Admin',
		'form' : form,
		'kode' : kode,
		'aksi' : aksi,
		'aidi' : id,
		'fasilitas' : fasilitas,
		'edit' : is_edit,
	}

	return render(request, 'profile/admin/paket/index_fasilitas.html', context)


@login_required
@is_verified()
def admin_fast_delete(request, id):
    message = ''

    try:
        fast_del = PaketWisata_Detail.objects.get(id=id)
        fast_del.delete()
        message = 'success'
    except PaketWisata_Detail.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }
    return JsonResponse(context)