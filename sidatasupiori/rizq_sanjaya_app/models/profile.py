from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.text import slugify
import string
import random

from requests import request
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.utils import timezone
import datetime
from ckeditor.fields import RichTextField


""""==============================VALIDATOR======================================"""
def validate_file_pdf(value):
	pass

"""VALIDASI UNTUK FILE DOKUMEN"""
def validate_file_dokumen(value):
	dokumen =[
			# 'application/vnd.ms-excel', 
			# 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
			'application/pdf', 
			'image/jpeg',
			'image/png',
			# 'text/csv',
			# 'application/msword',
			# 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			]
	if value.file.content_type not in dokumen:
		# raise ValidationError(u'Pastikan ekstensi file adalah .csv, .doc, .docx, .pdf, .xls atau .xlxs.')
		raise ValidationError(u'Pastikan ekstensi file adalah .jpeg, .png, .pdf')

"""VALIDASI UNTUK FILE GAMBAR"""
def validate_file_gambar(value):
	dokumen =[
				'image/jpeg',
				'image/png',
				'image/webp',
				'image/jpg',
			]
	if value.file.content_type not in dokumen:
		raise ValidationError(u'Pastikan ekstensi file adalah .jpg, .jpeg atau .png.')

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN FILE"""
def validate_file_size_dokumen(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 5242880:
		raise ValidationError("Pastikan ukuran File dibawah 5 MB.")
	else:
		return value

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN GAMBAR"""
def validate_file_size_gambar(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 2242880:
		raise ValidationError("Pastikan ukuran File dibawah 2 MB.")
	else:
		return value


""""==============================FUNGSI======================================"""
"""MEMBERIKAN NILAI RANDOM UNTUK SLUG KALO DUPLIKAT"""
def rand_slug():
	rand = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
	return rand.lower()

COLOR_CHOICES = [
	('#0d6efd', 'Biru'),
	('#6610f2', 'Indigo'),
	('#6f42c1', 'Ungu'),
	('#d63384', 'Merah Muda'),
	('#dc3545', 'Merah'),
	('#fd7e14', 'Oranye'),
	('#ffc107', 'Kuning'),
	('#198754', 'Hijau'),
	('#20c997', 'Teal'),
	('#0dcaf0', 'Cyan'),
	('#adb5bd', 'Abu-Abu'),
	('#000', 'Hitam'),
]
ANNOUNCEMENT_CHOICES = [
	('ASN', 'ASN'),
	('Umum', 'Umum'),
]
ROLE_CHOICES = [
	('admin', 'Admin'),
	('posting', 'Posting'),
]


""""==============================MODEL TABEL======================================"""
"""BASE MODEL BIAR GA NGULANG2 MASUKIN 3 FIELD KETERANGAN WAKTU INI"""
class Time(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	deleted_at = models.DateTimeField(null=True, blank=True)

	class Meta:
		abstract = True
		
class AccountManager(BaseUserManager):
	use_in_migrations = True

	def _create_user(self, email, username, phone, password, **extra_fields):
		values = [email, username, phone,]
		field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
		for field_name, value in field_value_map.items():
			if not value:
				raise ValueError('The {} value must be set'.format(field_name))

		email = self.normalize_email(email)
		user = self.model(
			email=email,
			username=username,
			phone=phone,
			**extra_fields
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(email, username, phone, password, **extra_fields)

	def create_superuser(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_verified', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')
		if extra_fields.get('is_verified') is not True:
			raise ValueError('Superuser must have is_verified=True.')

		return self._create_user(email, username, phone, password, **extra_fields)


"""TABEL AKUN UNTUK SELAIN BAWAANNYA DJANGO YANG DIPAKAI"""
class Account(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(unique=True)
	username = models.CharField(unique=True, max_length=50)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	is_verified = models.BooleanField(default=False)
	date_joined = models.DateTimeField(default=timezone.now)
	last_login = models.DateTimeField(null=True)
	phone = models.CharField(max_length=15)
	date_of_birth = models.DateField(blank=True, null=True)
	avatar = models.ImageField(blank=True, null=True, upload_to='profile/images/avatar/', validators=[validate_file_gambar, validate_file_size_gambar],)
	role = models.CharField(max_length=50, choices=ROLE_CHOICES, default='posting')
	email_verification_token = models.CharField(max_length=100, default='')
	
	objects = AccountManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username', 'phone', 'role']

	def get_full_name(self):
		return f"{self.first_name} {self.last_name}"

	def get_short_name(self):
		return self.first_name

"""TABEL CATEGORY UNTUK BERITA dan GALERI"""
class Categoryitem(models.Model):
	object_id = models.IntegerField(default=0)
	content_type_id = models.IntegerField(default=0)
	categori_id = models.IntegerField(default=0)

class Category(models.Model):
	nama = models.CharField(max_length=100, null=False, blank=False, unique=True)
	typexs = models.CharField(max_length=100,default='product')
	slug = models.SlugField(unique=True, max_length=150)
	foto = models.ImageField(null=True, upload_to='profile/images/kategori', validators=[validate_file_gambar, validate_file_size_gambar])

	def __str__(self):
		return self.nama

	def __init__(self, *args, **kwargs):
		super(Category, self).__init__(*args, **kwargs)
		self.old_nama = self.nama

	def save(self, *args, **kwargs):
		if self.old_nama != self.nama:
			slug = slugify(self.nama)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Category.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Category.DoesNotExist:
					self.slug = slug
					break
		super(Category, self).save(*args, **kwargs)

class RnW(Time):
	jenis = models.CharField(max_length=100)
	deskripsi = RichTextUploadingField()

"""TABEL NEWS UNTUK MENYIMPAN BERITA"""
class News(Time):
	title = models.CharField(max_length=255)
	thumbnail = models.ImageField(upload_to='profile/images/artikel/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/artikel/no-image.jpg')
	content = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=300)
	category = models.ForeignKey(Category, on_delete=models.RESTRICT)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	seen = models.SmallIntegerField(default=0)
	jenis = models.CharField(max_length=255, null=True)

	def __init__(self, *args, **kwargs):
		super(News, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = News.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except News.DoesNotExist:
					self.slug = slug
					break
		super(News, self).save(*args, **kwargs)


"""TABEL GALERI UNTUK MENYIMPAN FOTO DAN LINK YOUTUBE VIDEO"""
class Gallery(Time):
	title = models.CharField(max_length=255)
	keterangan = models.TextField(max_length=600)
	foto = models.ImageField(upload_to='profile/images/galeri/', validators=[validate_file_gambar, validate_file_size_gambar], null=True, blank=True)
	video = models.TextField(null=True, blank=True, max_length=500)
	slug = models.SlugField(unique=True, max_length=300)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	categori = models.CharField(max_length=255, default='')
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)

	def __init__(self, *args, **kwargs):
		super(Gallery, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Gallery.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Gallery.DoesNotExist:
					self.slug = slug
					break
		super(Gallery, self).save(*args, **kwargs)

class RunningText(Time):
	text=models.CharField(max_length=255)

class RunningLink(Time):
	nama = models.CharField(max_length=100)
	logo=models.ImageField(upload_to='profile/images/running_link/', validators=[validate_file_gambar, validate_file_size_gambar])
	link=models.CharField(max_length=500)

class Link(Time):
	nama=models.CharField(max_length=100, unique=True)
	link=models.CharField(max_length=500)

class AppSetting(Time):
	nama=models.CharField(max_length=100, unique=True)
	keterangan=models.TextField(null=True)
	tanggal = models.DateField(null=True)
	image = models.ImageField(upload_to='profile/images/diskon/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)	

class Kata(Time):
	judul = models.TextField(null=True)
	keterangan = models.TextField(null=True)
	image = models.ImageField(upload_to='profile/images/kata/', validators=[validate_file_gambar, validate_file_size_gambar])
	 
class Carousel(Time):
	judul = models.TextField(null=True, blank=True)
	subjudul = models.TextField(null=True)
	foto = models.ImageField(upload_to='profile/images/carousel/', validators=[validate_file_gambar, validate_file_size_gambar])
	link=models.CharField(max_length=200, null=True, blank=True)
	kondisi = models.TextField(default=True, null=True)
	keterangan = RichTextField(null=True)
	harga = models.IntegerField(null=True)
	diskon = models.IntegerField(null=True)
	persen_diskon = models.IntegerField(null=True)

class Foto(models.Model):
    images = models.ImageField(upload_to='profile/images/destinasi/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)

class Destinasi(models.Model):
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True, null=True)
	deleted_at = models.DateTimeField(null=True, blank=True)
	nama = models.TextField(null=True)
	fasilitas = models.TextField(null=True)
	deskripsi = models.TextField(null=True)
	paket = models.ForeignKey(Carousel, on_delete=models.CASCADE)
	images = models.ManyToManyField(Foto, related_name='destinasi_images')

class DestinasiImage(models.Model):
    destinasi = models.ForeignKey(Destinasi, default=None, on_delete=models.CASCADE)
    images = models.ImageField(upload_to='profile/images/destinasi/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)
    

class LogVisitor(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)

class LogAdmin(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	aktivitas = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)

class AboutMe(models.Model):
	judul = models.CharField(max_length=255, default='Tentang Kami.')
	deskripsi = models.CharField(max_length=255, default='-')
	youtube = models.CharField(max_length=255, null=True, default='')
	icon = models.CharField(max_length=255, null=True, default='')
	images = models.ImageField(upload_to='profile/images/about/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
	jenis = models.CharField(max_length=30, default='header')
	section = models.CharField(max_length=30, default='tentang')

class Layanan(models.Model):
	judul = models.CharField(max_length=100, default='Lorem Ipsum')
	keterangan = models.CharField(max_length=255, default='Lorem ipsum dolor sit amet')
	icon = models.CharField(max_length=255, null=True, default='')
	slug = models.CharField(max_length=255, default='')

class FAQ(models.Model):
	pertanyaan = models.TextField(null=True)
	jawaban = models.TextField(null=True)
	highlight = models.TextField(null=True)

class Product(Time):
	product_name = models.CharField(max_length=255)
	images = models.ImageField(upload_to='profile/images/produk/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg')
	description = RichTextUploadingField()
	slug = models.SlugField(unique=True, max_length=300)
	category = models.ForeignKey(Category, on_delete=models.RESTRICT)
	harga = models.IntegerField(null=True)
	diskon = models.IntegerField(null=True)
	persen_diskon = models.IntegerField(null=True)
	tags = TaggableManager()
	created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)
	last_updated_by = models.CharField(max_length=5, null=True, blank=True)
	best_seller = models.BooleanField(max_length=1, default=False)
	
	def __init__(self, *args, **kwargs):
		super(Product, self).__init__(*args, **kwargs)
		self.old_product_name = self.product_name

	def save(self, *args, **kwargs):
		if self.old_product_name != self.product_name:
			slug = slugify(self.product_name)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = Product.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except Product.DoesNotExist:
					self.slug = slug
					break
		super(Product, self).save(*args, **kwargs)

class Announcement(Time):
    title=models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=300)
    description=models.TextField(max_length=1000)
    jenis=models.CharField(max_length=10, choices=ANNOUNCEMENT_CHOICES, default='Umum')
    created_by=models.ForeignKey(Account, on_delete=models.RESTRICT)
    last_updated_by = models.CharField(max_length=5, null=True, blank=True)
    dokumen = models.FileField(upload_to='profile/announcement/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)

    def __init__(self, *args, **kwargs):
        super(Announcement, self).__init__(*args, **kwargs)
        self.old_title = self.title

    def save(self, *args, **kwargs):
        if self.old_title != self.title:
            slug = slugify(self.title)
            slug_exists = True
            counter = rand_slug()
            self.slug = slug
            while slug_exists:
                try:
                    slug_exits = Announcement.objects.get(slug=slug)
                    if slug_exits and slug_exits.id == self.id:
                        self.slug = slug
                        break
                    elif slug_exits:
                        slug = self.slug + '-' + str(counter)
                        counter = rand_slug()
                except Announcement.DoesNotExist:
                    self.slug = slug
                    break
        super(Announcement, self).save(*args, **kwargs)

"""TABEL AGENDA UNTUK MENYIMPAN ACARA DAN KEGIATAN DI TANGGALAN"""
class Agenda(Time):
    name=models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=300)
    start_date=models.DateField()
    finish_date=models.DateField()
    start_time = models.TimeField(null=True, blank=True)
    finish_time = models.TimeField(null=True, blank=True)
    description=models.TextField(max_length=600)
    every_year=models.CharField(max_length=5)
    color=models.CharField(max_length=10, choices=COLOR_CHOICES, default='#fd7e14')
    created_by=models.ForeignKey(Account, on_delete=models.RESTRICT)
    last_updated_by = models.CharField(max_length=5, null=True, blank=True)

    def __init__(self, *args, **kwargs):
        super(Agenda, self).__init__(*args, **kwargs)
        self.old_name = self.name

    def save(self, *args, **kwargs):
        if self.old_name != self.name:
            slug = slugify(self.name)
            slug_exists = True
            counter = rand_slug()
            self.slug = slug
            while slug_exists:
                try:
                    slug_exits = Agenda.objects.get(slug=slug)
                    if slug_exits and slug_exits.id == self.id:
                        self.slug = slug
                        break
                    elif slug_exits:
                        slug = self.slug + '-' + str(counter)
                        counter = rand_slug()
                except Agenda.DoesNotExist:
                    self.slug = slug
                    break
        super(Agenda, self).save(*args, **kwargs)

"""TABEL LAYANAN DINAS DIGUNAKAN UNTUK MENYIMPAN LAYANAN DINAS BERDASARKAN JENIS LAYANAN"""

class LayananDinas(models.Model):
	title = RichTextUploadingField()
	typexs = models.CharField(max_length=500,default='Standar Pelayanan')
	status = models.CharField(max_length=500,default='Publish')
	description = RichTextUploadingField()
	foto = models.ImageField(null=True, upload_to='profile/images/layanan', validators=[validate_file_gambar, validate_file_size_gambar])

"""TABEL AKSESBILITAS EXTERNAL LINK DIGUNAKAN UNTUK MENYIMPAN AKSESBILITAS EXTERNAL LINK"""
class Aksesbilitas(models.Model):
	title = models.TextField(null=True)
	link = models.TextField(null=True)
	status = models.CharField(max_length=500,default='Publish')
	image = models.ImageField(upload_to='profile/images/aksesbilitas/', validators=[validate_file_gambar, validate_file_size_gambar])

"""TABEL INFORMASI TERPADU DIGUNAKAN UNTUK MENYIMPAN INFORMASI TERPADU"""
class InfoTerpadu(models.Model):
	title = RichTextUploadingField()
	create_date = models.DateTimeField(auto_now_add=True)
	start_event_date=models.DateField()
	end_event_date=models.DateField()
	typexs = models.CharField(max_length=500,default='infografis')
	description = RichTextUploadingField()
	foto = models.ImageField(null=True, upload_to='profile/images/infoterpadu', validators=[validate_file_gambar, validate_file_size_gambar])
	dokumen = models.FileField(upload_to='profile/announcement/infoterpadu/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)
	slug = models.SlugField(unique=True, max_length=150)
	status = models.CharField(max_length=500,default='Publish')
	def __init__(self, *args, **kwargs):
		super(InfoTerpadu, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = InfoTerpadu.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except InfoTerpadu.DoesNotExist:
					self.slug = slug
					break
		super(InfoTerpadu, self).save(*args, **kwargs)

"""TABEL PROFILSKPD DIGUNAKAN UNTUK MENYIMPAN SEJARAH,VISI&MISI, TUPOKSI, STRUKTUR ORGANISASI, PROFIL PEJABAT,SAMBUTAN KEPALA DINAS"""
class ProfilSKPD(models.Model):
	title = RichTextUploadingField()
	create_date = models.DateTimeField(auto_now_add=True)
	typexs = models.CharField(max_length=500,default='Sejarah')
	description = RichTextUploadingField()
	foto = models.ImageField(null=True, upload_to='profile/images/profilSKPD', validators=[validate_file_gambar, validate_file_size_gambar])
	dokumen = models.FileField(upload_to='profile/announcement/profilSKPD/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)
	slug = models.SlugField(unique=True, max_length=150)
	status = models.CharField(max_length=500,default='Publish')
	def __init__(self, *args, **kwargs):
		super(ProfilSKPD, self).__init__(*args, **kwargs)
		self.old_title = self.title

	def save(self, *args, **kwargs):
		if self.old_title != self.title:
			slug = slugify(self.title)
			slug_exists = True
			counter = rand_slug()
			self.slug = slug
			while slug_exists:
				try:
					slug_exits = ProfilSKPD.objects.get(slug=slug)
					if slug_exits and slug_exits.id == self.id:
						self.slug = slug
						break
					elif slug_exits:
						slug = self.slug + '-' + str(counter)
						counter = rand_slug()
				except ProfilSKPD.DoesNotExist:
					self.slug = slug
					break
		super(ProfilSKPD, self).save(*args, **kwargs)
######FOR APP SIDATA SUPIORI#############
"""TABEL DATA DISTRIK & KAMPUNG"""
class dataDistrik(models.Model):
	title = models.TextField(null=True)
	create_date = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=500,default='Aktif')
	
class dataKampung(models.Model):
	titles = models.TextField(null=True)
	create_date = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=500,default='Aktif')
	distrik = models.ForeignKey(dataDistrik, on_delete=models.RESTRICT)

"""TABEL DATA"""

	
class datapenanggungjawab(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	nama						= models.TextField(null=True)
	kontak						= models.TextField(null=True)
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')

class dataUMKM(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jeniinput	 			    = models.CharField(max_length=500,default='UMKM')
	title_usaha                 = models.TextField(null=True)
	title_owner                 = models.TextField(null=True)
	jenis_usaha                 = models.TextField(null=True)
	modal_usaha                 = models.IntegerField(null=True)
	bantuan	                    = models.TextField(null=True)
	izin_usaha                  = models.TextField(null=True)
	akta_usaha                  = models.TextField(null=True)
	jml_karyawan                = models.IntegerField(null=True)
	foto = models.ImageField(null=True, upload_to='profile/images/umkm', validators=[validate_file_gambar, validate_file_size_gambar])

class dataKelembagaan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jenisinput	 			    = models.CharField(max_length=500,default='Lembaga Adat')
	title_lembaga               = models.TextField(null=True)
	ketua_lembaga               = models.TextField(null=True)
	jml_anggota                 = models.IntegerField(null=True)
	foto = models.ImageField(null=True, upload_to='profile/images/lembaga', validators=[validate_file_gambar, validate_file_size_gambar])

class dataKominfo(models.Model):
	tahun						=  models.CharField(max_length=500,default='2022')
	penanggungjawab			    =  models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 =  models.DateTimeField(auto_now_add=True)
	status 				        =  models.CharField(max_length=500,default='Aktif')
	kampung 			        =  models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jml_bts	 			        =  models.IntegerField(null=True)
	jml_vsat	 			    =  models.IntegerField(null=True)
	akses_internet	 			=  models.IntegerField(null=True)
	foto = models.ImageField(null=True, upload_to='profile/images/kominfo', validators=[validate_file_gambar, validate_file_size_gambar])

class dataPU(models.Model):
	tahun						=  models.CharField(max_length=500,default='2022')
	penanggungjawab			    =  models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 =  models.DateTimeField(auto_now_add=True)
	status 				        =  models.CharField(max_length=500,default='Aktif')
	kampung 			        =  models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jln_beraspal	 			=   models.TextField(null=True)
	jln_hampar	 			    =   models.TextField(null=True)
	jln_rusak	 			    =   models.TextField(null=True)
	jln_tani	 			    =   models.TextField(null=True)
	jln_tani_blm_aspal	 	    =   models.TextField(null=True)
	foto_jln_beraspal     		=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jln_hampar 			=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jln_rusak 				=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jln_tani 				=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jln_tani_blm_aspal 	=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	jmt_beton	 			    =   models.TextField(null=True)
	kondisi_jmt	 			    =  models.TextField(null=True)
	jmt_kayu	 			    =   models.TextField(null=True)
	kondisi_jmt_kayu	 	    =  models.TextField(null=True)
	foto_jmt_beton     		    =  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_kondisi_jmt 			=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jmt_kayu 				=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_kondisi_jmt_kayu 		=  models.ImageField(null=True, upload_to='profile/images/pu', validators=[validate_file_gambar, validate_file_size_gambar])
	
class dataPerhubungan(models.Model):
	tahun						=  models.CharField(max_length=500,default='2022')
	penanggungjawab			    =  models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 =  models.DateTimeField(auto_now_add=True)
	status 				        =  models.CharField(max_length=500,default='Aktif')
	kampung 			        =  models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jml_halte	 			    =  models.IntegerField(null=True)
	jml_terminal	 			=  models.IntegerField(null=True)
	jml_dermaga		 			=  models.IntegerField(null=True)
	jml_tambatan_perahu		 	=  models.IntegerField(null=True)
	kondisi_dermaga			 	=  models.TextField(null=True)
	kondisi_tambatan_perahu		=  models.TextField(null=True)
	foto_halte 					=  models.ImageField(null=True, upload_to='profile/images/perhubungan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_terminal 				=  models.ImageField(null=True, upload_to='profile/images/perhubungan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_dermaga 				=  models.ImageField(null=True, upload_to='profile/images/perhubungan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_tambatan_perahu 		=  models.ImageField(null=True, upload_to='profile/images/perhubungan', validators=[validate_file_gambar, validate_file_size_gambar])

class dataSosial(models.Model):
	tahun						=  models.CharField(max_length=500,default='2022')
	penanggungjawab			    =  models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 =  models.DateTimeField(auto_now_add=True)
	status 				        =  models.CharField(max_length=500,default='Aktif')
	kampung 			        =  models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	jml_kartu_kip	 			=  models.IntegerField(null=True)
	jml_bantuan_s_miskin	 	=  models.IntegerField(null=True)
	jml_kartu_kis	 			=  models.IntegerField(null=True)
	jml_user_bpjs	 			=  models.IntegerField(null=True)
	jml_user_bpjs_mandiri	 	=  models.IntegerField(null=True)
	jml_bpjs_ketenagakerjaan	=  models.IntegerField(null=True)
	program_keluarga_harapan  	=  models.IntegerField(null=True)  
	bantuan_pangan_non_tunai	=  models.TextField(null=True)
	komunitas_adat_terpencil	=  models.IntegerField(null=True)

class dataPerikanan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	kodeperikanan				= models.TextField(null=True)
	jml_rt_nelayan              = models.IntegerField(null=True)
	jml_kartu_nelayan           = models.IntegerField(null=True)
	jml_rtp           			= models.IntegerField(null=True)

class dataDetailPerikanan(models.Model):
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	jenisinput 				    = models.CharField(max_length=500,default='KUB')
	kodeperikanan				= models.TextField(null=True)

	namakelompok              	= models.TextField(null=True)
	ketua           			= models.TextField(null=True)
	bantuan           			= models.TextField(null=True)
	sk_pelatihan           		= models.TextField(null=True)
	perikanan 			        = models.ForeignKey(dataPerikanan, on_delete=models.RESTRICT)
	foto 						= models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])

class dataFasilitasolahraga(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	kodefasilitas				= models.TextField(null=True)
	jml_lapangan_voly           = models.IntegerField(null=True)
	foto_jml_lapangan_voly 		= models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])
	jml_lapangan_bola           = models.IntegerField(null=True)
	foto_jml_lapangan_bola 		= models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])
	jml_lapangan_basket         = models.IntegerField(null=True)
	foto_jml_lapangan_basket 	= models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])
	jml_lapangan_takraw         = models.IntegerField(null=True)
	foto_jml_lapangan_takraw 	= models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])
	jml_lapangan_bulutangkis    = models.IntegerField(null=True)
	foto_jml_lapangan_bulutangkis = models.ImageField(null=True, upload_to='profile/images/perikanan', validators=[validate_file_gambar, validate_file_size_gambar])

class dataDetailFasilitasolahraga(models.Model):
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodefasilitas				= models.TextField(null=True)

	namakelompok              	= models.TextField(null=True)
	ketua           			= models.TextField(null=True)
	jml_anggota           	    = models.IntegerField(null=True)
	pelatihan           		= models.TextField(null=True)
	fasilitas 			        = models.ForeignKey(dataFasilitasolahraga, on_delete=models.RESTRICT)

class dataAtlet(models.Model):
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodefasilitas				= models.TextField(null=True)

	nama              			= models.TextField(null=True)
	event           			= models.TextField(null=True)
	prestasi           			= models.TextField(null=True)
	fasilitas 			        = models.ForeignKey(dataFasilitasolahraga, on_delete=models.RESTRICT)

# data pertanian
class dataPertanian(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	kodepertanian				= models.TextField(null=True)
	jml_ayam           			= models.IntegerField(null=True)
	jml_babi           			= models.IntegerField(null=True)
	jml_sapi        			= models.IntegerField(null=True)
	jml_kambing         		= models.IntegerField(null=True)
	jml_bebek    				= models.IntegerField(null=True)
	jml_kelinci    				= models.IntegerField(null=True)
	luas_lahan    				= models.TextField(null=True)
	
class dataKelompoktani(models.Model):
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodepertanian				= models.TextField(null=True)

	namakelompok              	= models.TextField(null=True)
	ketua           			= models.TextField(null=True)
	jml_anggota           	    = models.IntegerField(null=True)
	bantuan           			= models.TextField(null=True)
	pertanian 			        = models.ForeignKey(dataPertanian, on_delete=models.RESTRICT)
	foto						= models.ImageField(null=True, upload_to='profile/images/pertanian', validators=[validate_file_gambar, validate_file_size_gambar])

class dataKomoditipertanian(models.Model):
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodepertanian				= models.TextField(null=True)

	nama              			= models.TextField(null=True)
	keterangan           		= models.TextField(null=True)
	pertanian 			        = models.ForeignKey(dataPertanian, on_delete=models.RESTRICT)


class datakependudukan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	

	jml_penduduk_pria 			= models.IntegerField(null=True)
	jml_penduduk_wanita 		= models.IntegerField(null=True)
	jml_kriten					= models.IntegerField(null=True)
	jml_katholik				= models.IntegerField(null=True)
	jml_islam					= models.IntegerField(null=True)
	jml_hindu					= models.IntegerField(null=True)
	jml_budha					= models.IntegerField(null=True)
	jml_OAP					    = models.IntegerField(null=True)
	jml_nonOAP					= models.IntegerField(null=True)
	jml_KK					    = models.IntegerField(null=True)
	jml_KTP					    = models.IntegerField(null=True)
	jml_Akta					= models.IntegerField(null=True)

class dataPemerintahan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	

	luas_wilayah 				= models.TextField(null=True)
	rt 							= models.TextField(null=True)
	dusun						= models.TextField(null=True)
	batas_utara					= models.TextField(null=True)
	batas_selatan				= models.TextField(null=True)
	batas_barat					= models.TextField(null=True)
	batas_timur					= models.TextField(null=True)
	jml_aparatur_kampung		= models.IntegerField(null=True)
	pendidikan_aparatur_sd		= models.IntegerField(null=True)
	pendidikan_aparatur_smp		= models.IntegerField(null=True)
	pendidikan_aparatur_sma		= models.IntegerField(null=True)
	pendidikan_aparatur_d3		= models.IntegerField(null=True)
	pendidikan_aparatur_s1		= models.IntegerField(null=True)
	jml_basmuskam				= models.IntegerField(null=True)
	pendidikan_bamuskam_sd		= models.IntegerField(null=True)
	pendidikan_bamuskam_smp		= models.IntegerField(null=True)
	pendidikan_bamuskam_sma		= models.IntegerField(null=True)
	pendidikan_bamuskam_d3		= models.IntegerField(null=True)
	pendidikan_bamuskam_s1		= models.IntegerField(null=True)
	jml_ketua_rt				= models.IntegerField(null=True)
	jml_kepala_dusun			= models.IntegerField(null=True)
	jml_Babinkamtibmas			= models.IntegerField(null=True)
	jml_Babinsa					= models.IntegerField(null=True)
	kantor						= models.TextField(null=True)
	status_kantor				= models.TextField(null=True)


# data kesehatan	
class dataKesehatan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)

	namapuskesmas	 			= models.TextField(null=True)
	putu	 					= models.TextField(null=True)
	status_puskesmas 			= models.CharField(max_length=500,default='Rawat Inap')
	kondisi_puskesmas 			= models.CharField(max_length=500,default='Baik')

	dok_spesialis  				= models.IntegerField(null=True)
	dok_umum 					= models.IntegerField(null=True)
	dok_gigi 					= models.IntegerField(null=True)
	perawat 					= models.IntegerField(null=True)
	bidan 						= models.IntegerField(null=True)
	tenaga_promkes 				= models.IntegerField(null=True)
	tenaga_sanitasi  			= models.IntegerField(null=True)
	apoteker 					= models.IntegerField(null=True)
	tenaga_lab 					= models.IntegerField(null=True)
	jml_rumah_dinas  			= models.IntegerField(null=True)

	jml_balita 							= models.IntegerField(null=True)
	jml_ibu_hamil 						= models.IntegerField(null=True)
	jml_kematian_ibu_hamil 				= models.IntegerField(null=True)
	jml_kematian_ibu_melahirkan 		= models.IntegerField(null=True)
	jml_kematian_bayi  					= models.IntegerField(null=True)
	foto_puskesmas						= models.ImageField(null=True, upload_to='profile/images/kesehatan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_rumah_dinas					= models.ImageField(null=True, upload_to='profile/images/kesehatan', validators=[validate_file_gambar, validate_file_size_gambar])
	

class dataPosyandu(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)

	nama              			= models.TextField(null=True)
	kader 		           		= models.IntegerField(null=True)
	foto_posyandu				= models.ImageField(null=True, upload_to='profile/images/kesehatan', validators=[validate_file_gambar, validate_file_size_gambar])

class dataPerumahan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	
	jml_rumah_layak_huni		= models.IntegerField(null=True)
	jml_rumah_tdk_layak_huni	= models.IntegerField(null=True)
	jml_rumah_semi_permanen		= models.IntegerField(null=True)
	jml_rumah_permanen			= models.IntegerField(null=True)
	foto_rumah_semi_permanen	= models.ImageField(null=True, upload_to='profile/images/perumahan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_rumah_permanen			= models.ImageField(null=True, upload_to='profile/images/perumahan', validators=[validate_file_gambar, validate_file_size_gambar])
	jml_total_rumah				= models.IntegerField(null=True)
	lantai_tanah				= models.IntegerField(null=True)
	lantai_kayu					= models.IntegerField(null=True)
	lantai_semen				= models.IntegerField(null=True)
	lantai_keramik				= models.IntegerField(null=True)
	dinding_kayu				= models.IntegerField(null=True)
	dinding_semen				= models.IntegerField(null=True)
	dinding_anyaman				= models.IntegerField(null=True)
	atap_seng					= models.IntegerField(null=True)
	atap_daun					= models.IntegerField(null=True)
	sumur_gali_pribadi			= models.IntegerField(null=True)
	sumur_umum					= models.IntegerField(null=True)
	spam						= models.IntegerField(null=True)
	air_sungai					= models.IntegerField(null=True)
	jamban_rumah				= models.IntegerField(null=True)
	jamban_umum 				= models.IntegerField(null=True)
	foto_jamban_rumah			= models.ImageField(null=True, upload_to='profile/images/perumahan', validators=[validate_file_gambar, validate_file_size_gambar])
	foto_jamban_umum			= models.ImageField(null=True, upload_to='profile/images/perumahan', validators=[validate_file_gambar, validate_file_size_gambar])
	rumah_listrik				= models.IntegerField(null=True)
	penerangan_lain				= models.IntegerField(null=True)


# data pendidikan	
class dataPendidikan(models.Model):
	tahun						= models.CharField(max_length=500,default='2022')
	penanggungjawab			    = models.ForeignKey(datapenanggungjawab, on_delete=models.RESTRICT)	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kampung 			        = models.ForeignKey(dataKampung, on_delete=models.RESTRICT)
	kodependidikan				= models.TextField(null=True)

	# data paud	
class dataPendidikanPaud(models.Model):

	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodependidikan				= models.TextField(null=True) 

	namasekolah		 			= models.TextField(null=True)
	jml_ruang_belajar	 		= models.IntegerField(null=True)
	jml_murid 					= models.IntegerField(null=True)
	jml_guru 					= models.IntegerField(null=True)
	jml_guru_oap 				= models.IntegerField(null=True)
	jml_guru_nonoap 			= models.IntegerField(null=True)
	jml_meja_kursi 				= models.IntegerField(null=True) 
	
	foto_paud					= models.ImageField(null=True, upload_to='profile/images/pendidikan', validators=[validate_file_gambar, validate_file_size_gambar])
	pendidikan 			        = models.ForeignKey(dataPendidikan, on_delete=models.RESTRICT)

	# data SD	
class dataPendidikanSD(models.Model):
	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodependidikan				= models.TextField(null=True)

	namasekolah		 			= models.TextField(null=True)
	jml_ruang_belajar	 		= models.IntegerField(null=True)
	jml_perpustakaan	 		= models.IntegerField(null=True)
	jml_kantor			 		= models.IntegerField(null=True)
	jml_wc			 			= models.IntegerField(null=True)
	jml_papan_tulis			 	= models.IntegerField(null=True)
	jml_pc			 			= models.IntegerField(null=True)
	
	jml_guru			 		= models.IntegerField(null=True)
	jml_guru_oap			 	= models.IntegerField(null=True)
	jml_guru_non_oap			= models.IntegerField(null=True)


	jml_murid			 				= models.IntegerField(null=True)
	jml_murid_kelas_I			 		= models.IntegerField(null=True)
	jml_murid_kelas_II			 		= models.IntegerField(null=True)
	jml_murid_kelas_III			 		= models.IntegerField(null=True)
	jml_murid_kelas_IV			 		= models.IntegerField(null=True)
	jml_murid_kelas_V			 		= models.IntegerField(null=True)
	jml_murid_kelas_VI			 		= models.IntegerField(null=True)

	jml_meja_kursi			 			= models.IntegerField(null=True)
	jml_meja_kursi_I			 		= models.IntegerField(null=True)
	jml_meja_kursi_II			 		= models.IntegerField(null=True)
	jml_meja_kursi_III			 		= models.IntegerField(null=True)
	jml_meja_kursi_IV			 		= models.IntegerField(null=True)
	jml_meja_kursi_V			 		= models.IntegerField(null=True)
	jml_meja_kursi_VI			 		= models.IntegerField(null=True)

	jml_buku_paket			 			= models.IntegerField(null=True)
	jml_buku_paket_I			 		= models.IntegerField(null=True)
	jml_buku_paket_II			 		= models.IntegerField(null=True)
	jml_buku_paket_III			 		= models.IntegerField(null=True)
	jml_buku_paket_IV			 		= models.IntegerField(null=True)
	jml_buku_paket_V			 		= models.IntegerField(null=True)
	jml_buku_paket_VI			 		= models.IntegerField(null=True)

	foto_sd						= models.ImageField(null=True, upload_to='profile/images/pendidikan', validators=[validate_file_gambar, validate_file_size_gambar])
	pendidikan 			        = models.ForeignKey(dataPendidikan, on_delete=models.RESTRICT)

	# data SMP
class dataPendidikanSMP(models.Model):
	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodependidikan				= models.TextField(null=True)

	namasekolah		 			= models.TextField(null=True)
	jml_ruang_belajar	 		= models.IntegerField(null=True)
	jml_perpustakaan	 		= models.IntegerField(null=True)
	jml_lab			 			= models.IntegerField(null=True)
	jml_kantor			 		= models.IntegerField(null=True)
	jml_wc			 			= models.IntegerField(null=True)
	jml_papan_tulis			 	= models.IntegerField(null=True)
	jml_pc			 			= models.IntegerField(null=True)

	kebutuhan_guru				= models.TextField(null=True)
	kekurangan_guru				= models.TextField(null=True)
	
	jml_guru			 		= models.IntegerField(null=True)
	jml_guru_oap			 	= models.IntegerField(null=True)
	jml_guru_non_oap			= models.IntegerField(null=True)
	
	jml_murid			 				= models.IntegerField(null=True)
	jml_murid_kelas_VII			 		= models.IntegerField(null=True)
	jml_murid_kelas_VIII			 	= models.IntegerField(null=True)
	jml_murid_kelas_IX			 		= models.IntegerField(null=True)

	jml_meja_kursi			 			= models.IntegerField(null=True)
	jml_meja_kursi_VII			 		= models.IntegerField(null=True)
	jml_meja_kursi_VIII			 		= models.IntegerField(null=True)
	jml_meja_kursi_IX			 		= models.IntegerField(null=True)

	jml_buku_paket			 			= models.IntegerField(null=True)
	jml_buku_paket_VII			 		= models.IntegerField(null=True)
	jml_buku_paket_VIII			 		= models.IntegerField(null=True)
	jml_buku_paket_IX			 		= models.IntegerField(null=True)
	
	buku_paket_kurang_matapelajaran		= models.IntegerField(null=True)

	foto_smp							= models.ImageField(null=True, upload_to='profile/images/pendidikan', validators=[validate_file_gambar, validate_file_size_gambar])
	pendidikan 			       			= models.ForeignKey(dataPendidikan, on_delete=models.RESTRICT)


	# data SMK	/ SMA
class dataPendidikanSMA(models.Model):
	
	create_date                 = models.DateTimeField(auto_now_add=True)
	status 				        = models.CharField(max_length=500,default='Aktif')
	kodependidikan				= models.TextField(null=True)

	namasekolah		 			= models.TextField(null=True)
	jml_ruang_belajar	 		= models.IntegerField(null=True)
	jml_perpustakaan	 		= models.IntegerField(null=True)
	jml_lab			 			= models.IntegerField(null=True)
	jml_kantor			 		= models.IntegerField(null=True)
	jml_wc			 			= models.IntegerField(null=True)
	jml_papan_tulis			 	= models.IntegerField(null=True)
	jml_pc			 			= models.IntegerField(null=True)

	kebutuhan_guru				= models.TextField(null=True)
	kekurangan_guru				= models.TextField(null=True)
	
	jml_guru			 		= models.IntegerField(null=True)
	jml_guru_oap			 	= models.IntegerField(null=True)
	jml_guru_non_oap			= models.IntegerField(null=True)
	
	jml_murid			 				= models.IntegerField(null=True)
	jml_murid_kelas_X			 		= models.IntegerField(null=True)
	jml_murid_kelas_XI			 		= models.IntegerField(null=True)
	jml_murid_kelas_XII			 		= models.IntegerField(null=True)

	jml_meja_kursi			 			= models.IntegerField(null=True)

	jml_buku_paket			 			= models.IntegerField(null=True)
	jml_buku_paket_X			 		= models.IntegerField(null=True)
	jml_buku_paket_XI			 		= models.IntegerField(null=True)
	jml_buku_paket_XII			 		= models.IntegerField(null=True)
	
	buku_paket_kurang_matapelajaran		= models.IntegerField(null=True)

	foto_sma							= models.ImageField(null=True, upload_to='profile/images/pendidikan', validators=[validate_file_gambar, validate_file_size_gambar])
	pendidikan 			       			= models.ForeignKey(dataPendidikan, on_delete=models.RESTRICT)




	

