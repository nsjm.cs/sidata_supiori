from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_kesehatan = dataKesehatan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataKesehatan.objects.filter(status='Tidak aktif')
    dt_posyandu = dataPosyandu.objects.prefetch_related('kampung').filter(status='Aktif')
    archives_posyandu = dataPosyandu.objects.filter(status='Tidak aktif')
    paginator = Paginator(dt_kesehatan, 15)
    try:
        dt_kesehatan = paginator.page(page)
    except PageNotAnInteger:
        dt_kesehatan = paginator.page(1)
    except EmptyPage:
        dt_kesehatan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Data Kesehatan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_kesehatan_list' : dt_kesehatan,
        'archives' : archives,
        'dt_posyandu_list' : dt_posyandu,
        'archives_posyandu'  : archives_posyandu,

    }
    
    return render(request, 'profile/admin/kesehatan/index.html', context)

@login_required
@is_verified()
def create(request):
    form = ''
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        namapuskesmas  				= request.POST.get('namapuskesmas') 
        putu 					= request.POST.get('putu') 

        dok_spesialis  				= request.POST.get('dok_spesialis') 
        dok_umum 					= request.POST.get('dok_umum') 
        dok_gigi 					= request.POST.get('dok_gigi') 
        perawat 					= request.POST.get('perawat') 
        bidan 						= request.POST.get('bidan') 
        tenaga_promkes 				= request.POST.get('tenaga_promkes') 
        tenaga_sanitasi  			= request.POST.get('tenaga_sanitasi') 
        apoteker 					= request.POST.get('apoteker') 
        tenaga_lab 					= request.POST.get('tenaga_lab') 
        jml_rumah_dinas  			= request.POST.get('jml_rumah_dinas') 

        jml_balita 							= request.POST.get('jml_balita') 
        jml_ibu_hamil 						= request.POST.get('jml_ibu_hamil') 
        jml_kematian_ibu_hamil 				= request.POST.get('jml_kematian_ibu_hamil') 
        jml_kematian_ibu_melahirkan 		= request.POST.get('jml_kematian_ibu_melahirkan') 
        jml_kematian_bayi  					= request.POST.get('jml_kematian_bayi') 
        foto_puskesmas						= request.FILES['foto_puskesmas']
        foto_rumah_dinas					= request.FILES['foto_rumah_dinas'] 
       
        if kampung is not None:
            insert_data_kesehatan_                       = dataKesehatan()
            insert_data_kesehatan_.tahun                 = tahun
            insert_data_kesehatan_.penanggungjawab_id    = penanggungjawab_id
            insert_data_kesehatan_.kampung_id            = kampung

            insert_data_kesehatan_.namapuskesmas = namapuskesmas
            insert_data_kesehatan_.putu = putu

            insert_data_kesehatan_.dok_spesialis = dok_spesialis
            insert_data_kesehatan_.dok_umum = dok_umum
            insert_data_kesehatan_.dok_gigi = dok_gigi
            insert_data_kesehatan_.perawat = perawat
            insert_data_kesehatan_.bidan = bidan
            insert_data_kesehatan_.tenaga_promkes = tenaga_promkes
            insert_data_kesehatan_.tenaga_sanitasi = tenaga_sanitasi
            insert_data_kesehatan_.apoteker = apoteker
            insert_data_kesehatan_.tenaga_lab = tenaga_lab
            insert_data_kesehatan_.jml_rumah_dinas = jml_rumah_dinas
            insert_data_kesehatan_.jml_balita = jml_balita
            insert_data_kesehatan_.jml_ibu_hamil = jml_ibu_hamil
            insert_data_kesehatan_.jml_kematian_ibu_hamil = jml_kematian_ibu_hamil
            insert_data_kesehatan_.jml_kematian_ibu_melahirkan = jml_kematian_ibu_melahirkan
            insert_data_kesehatan_.jml_kematian_bayi = jml_kematian_bayi
            insert_data_kesehatan_.foto_puskesmas = foto_puskesmas
            insert_data_kesehatan_.foto_rumah_dinas = foto_rumah_dinas
            

            insert_data_kesehatan_.save()
            messages.success(request, 'Data Kesehatan berhasil disimpan.')
            return redirect('profile:admin_kesehatan')
        messages.error(request, 'Data Kesehatan gagal disimpan.')
        return render(request, 'profile/admin/kesehatan/create.html', {'form': form,})
@login_required
@is_verified()
def create_posyandu(request):
    form = ''
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        nama  				    = request.POST.get('nama') 
        kader 					= request.POST.get('kader') 
        foto_posyandu		    = request.FILES['foto_posyandu'] 
       
        if kampung is not None:
            insert_data_posyandu_                       = dataPosyandu()
            insert_data_posyandu_.tahun                 = tahun
            insert_data_posyandu_.penanggungjawab_id    = penanggungjawab_id
            insert_data_posyandu_.kampung_id            = kampung

            insert_data_posyandu_.nama = nama
            insert_data_posyandu_.kader = kader
            insert_data_posyandu_.foto_posyandu        = foto_posyandu
            

            insert_data_posyandu_.save()
            messages.success(request, 'Data Posyandu berhasil disimpan.')
            return redirect('profile:admin_kesehatan')
        messages.error(request, 'Data Posyandu gagal disimpan.')
        return render(request, 'profile/admin/kesehatan/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_distriks = dataDistrik.objects.get(id = id)

        context = {
            'title' : 'EDIT DISTRIK',
            'form' : form,
            'edit' : 'true',
            'data_distriks' : data_distriks,
        }
        template = 'profile/admin/distrik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_distrik = dataDistrik.objects.get(id = id)
        title = request.POST.get('title')

        if data_distrik is not None:
            data_distrik.title = title
            data_distrik.save()
            data_distrik.save()

            messages.success(request, 'Distrik berhasil disimpan')
            return redirect('profile:admin_distrik')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKesehatan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataKesehatan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKesehatan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataKesehatan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def softDelete_posyandu(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPosyandu.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPosyandu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_posyandu(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPosyandu.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPosyandu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)