from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_kependudukan = datakependudukan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = datakependudukan.objects.filter(status='Tidak aktif')
    paginator = Paginator(dt_kependudukan, 15)
    try:
        dt_kependudukan = paginator.page(page)
    except PageNotAnInteger:
        dt_kependudukan = paginator.page(1)
    except EmptyPage:
        dt_kependudukan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Data Kependudukan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_kependudukan_lis' : dt_kependudukan,
        'archives' : archives,

    }
    
    return render(request, 'profile/admin/kependudukan/index.html', context)

@login_required
@is_verified()
def create(request):
    form = ''
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        jml_penduduk_pria 			= request.POST.get('jml_penduduk_pria')
        jml_penduduk_wanita 		= request.POST.get('jml_penduduk_wanita')
        jml_kriten					= request.POST.get('jml_kriten')
        jml_katholik				= request.POST.get('jml_katholik')
        jml_islam					= request.POST.get('jml_islam')
        jml_hindu					= request.POST.get('jml_hindu')
        jml_budha					= request.POST.get('jml_budha')
        jml_OAP					    = request.POST.get('jml_OAP')
        jml_nonOAP					= request.POST.get('jml_nonOAP')
        jml_KK					    = request.POST.get('jml_KK')
        jml_KTP					    = request.POST.get('jml_KTP')
        jml_Akta					= request.POST.get('jml_Akta')

       
        if kampung is not None:
            insert_data_kependudukan_                       = datakependudukan()
            insert_data_kependudukan_.tahun                 = tahun
            insert_data_kependudukan_.penanggungjawab_id    = penanggungjawab_id
            insert_data_kependudukan_.kampung_id            = kampung

            insert_data_kependudukan_.jml_penduduk_pria = jml_penduduk_pria
            insert_data_kependudukan_.jml_penduduk_wanita = jml_penduduk_wanita
            insert_data_kependudukan_.jml_kriten = jml_kriten
            insert_data_kependudukan_.jml_katholik = jml_katholik
            insert_data_kependudukan_.jml_islam = jml_islam
            insert_data_kependudukan_.jml_hindu = jml_hindu
            insert_data_kependudukan_.jml_budha = jml_budha
            insert_data_kependudukan_.jml_OAP = jml_OAP
            insert_data_kependudukan_.jml_nonOAP = jml_nonOAP
            insert_data_kependudukan_.jml_KK = jml_KK
            insert_data_kependudukan_.jml_KTP = jml_KTP
            insert_data_kependudukan_.jml_Akta = jml_Akta

            insert_data_kependudukan_.save()
            messages.success(request, 'Data Kependudukan berhasil disimpan.')
            return redirect('profile:admin_kependudukan')
        messages.error(request, 'Data kependudukan gagal disimpan.')
        return render(request, 'profile/admin/kependudukan/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    edit_datakependudukan = get_object_or_404(datakependudukan, id=id)
    form = ''
    
    # if request.method == 'GET':
    #     data_distriks = dataDistrik.objects.get(id = id)

    #     context = {
    #         'title' : 'EDIT DISTRIK',
    #         'form' : form,
    #         'edit' : 'true',
    #         'data_distriks' : data_distriks,
    #     }
    #     template = 'profile/admin/distrik/create.html'
    #     return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jml_penduduk_pria 			= request.POST.get('jml_penduduk_pria')
        jml_penduduk_wanita 		= request.POST.get('jml_penduduk_wanita')
        jml_kriten					= request.POST.get('jml_kriten')
        jml_katholik				= request.POST.get('jml_katholik')
        jml_islam					= request.POST.get('jml_islam')
        jml_hindu					= request.POST.get('jml_hindu')
        jml_budha					= request.POST.get('jml_budha')
        jml_OAP					    = request.POST.get('jml_OAP')
        jml_nonOAP					= request.POST.get('jml_nonOAP')
        jml_KK					    = request.POST.get('jml_KK')
        jml_KTP					    = request.POST.get('jml_KTP')
        jml_Akta					= request.POST.get('jml_Akta')

        if edit_datakependudukan is not None:

            edit_datakependudukan.tahun                 = tahun
            edit_datakependudukan.penanggungjawab_id    = penanggungjawab_id
            edit_datakependudukan.kampung_id            = kampung

            edit_datakependudukan.jml_penduduk_pria = jml_penduduk_pria
            edit_datakependudukan.jml_penduduk_wanita = jml_penduduk_wanita
            edit_datakependudukan.jml_kriten = jml_kriten
            edit_datakependudukan.jml_katholik = jml_katholik
            edit_datakependudukan.jml_islam = jml_islam
            edit_datakependudukan.jml_hindu = jml_hindu
            edit_datakependudukan.jml_budha = jml_budha
            edit_datakependudukan.jml_OAP = jml_OAP
            edit_datakependudukan.jml_nonOAP = jml_nonOAP
            edit_datakependudukan.jml_KK = jml_KK
            edit_datakependudukan.jml_KTP = jml_KTP
            edit_datakependudukan.jml_Akta = jml_Akta
            edit_datakependudukan.save()
            

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_kependudukan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = datakependudukan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except datakependudukan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = datakependudukan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except datakependudukan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)