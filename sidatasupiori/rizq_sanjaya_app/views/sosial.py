from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    
    data_sosial = dataSosial.objects.prefetch_related('kampung').filter(status='Aktif')
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    archives = dataSosial.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_sosial, 15)
    try:
        data_sosial = paginator.page(page)
    except PageNotAnInteger:
        data_sosial = paginator.page(1)
    except EmptyPage:
        data_sosial = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data data_sosial',
        'data_sosial_list' : data_sosial,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/sosial/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataSosial.objects.all()
        data_kominfo_ = dataSosial.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data Sosial',
            'form' : form,
            'data_kominfo_list':data_kominfo_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/sosial/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        

        jml_kartu_kip	 			        = request.POST.get('jml_kartu_kip') 
        jml_bantuan_s_miskin	 			= request.POST.get('jml_bantuan_s_miskin') 
        jml_kartu_kis		 			    = request.POST.get('jml_kartu_kis') 
        jml_user_bpjs		 	            = request.POST.get('jml_user_bpjs') 
        jml_user_bpjs_mandiri			 	= request.POST.get('jml_user_bpjs_mandiri')
        jml_bpjs_ketenagakerjaan		    = request.POST.get('jml_bpjs_ketenagakerjaan')
        program_keluarga_harapan 			= request.POST.get('program_keluarga_harapan')
        bantuan_pangan_non_tunai 			= request.POST.get('bantuan_pangan_non_tunai')
        komunitas_adat_terpencil 			= request.POST.get('komunitas_adat_terpencil')

        if kampung is not None:
            data_sosial_ = dataSosial()
            data_sosial_.tahun = tahun
            data_sosial_.penanggungjawab_id             = penanggungjawab_id
            data_sosial_.kampung_id                     = kampung		
            data_sosial_.jml_kartu_kip 		            = jml_kartu_kip 	
            data_sosial_.jml_bantuan_s_miskin 		    = jml_bantuan_s_miskin 	
            data_sosial_.jml_kartu_kis 		            = jml_kartu_kis 	
            data_sosial_.jml_user_bpjs 		            = jml_user_bpjs 	
            data_sosial_.jml_user_bpjs_mandiri 		    = jml_user_bpjs_mandiri 	
            data_sosial_.jml_bpjs_ketenagakerjaan 		= jml_bpjs_ketenagakerjaan 	
            data_sosial_.program_keluarga_harapan 		= program_keluarga_harapan 	
            data_sosial_.bantuan_pangan_non_tunai 		= bantuan_pangan_non_tunai 	
            data_sosial_.komunitas_adat_terpencil 		= komunitas_adat_terpencil 	

            data_sosial_.save()
            messages.success(request, 'Data sosial berhasil disimpan.')
            return redirect('profile:admin_sosial')
        messages.error(request, 'Data sosial gagal disimpan.')
        return render(request, 'profile/admin/sosial/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    edit_datasosial = get_object_or_404(dataSosial, id=id)
    
    form = ''
    
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jml_kartu_kip	 			        = request.POST.get('jml_kartu_kip') 
        jml_bantuan_s_miskin	 			= request.POST.get('jml_bantuan_s_miskin') 
        jml_kartu_kis		 			    = request.POST.get('jml_kartu_kis') 
        jml_user_bpjs		 	            = request.POST.get('jml_user_bpjs') 
        jml_user_bpjs_mandiri			 	= request.POST.get('jml_user_bpjs_mandiri')
        jml_bpjs_ketenagakerjaan		    = request.POST.get('jml_bpjs_ketenagakerjaan')
        program_keluarga_harapan 			= request.POST.get('program_keluarga_harapan')
        bantuan_pangan_non_tunai 			= request.POST.get('bantuan_pangan_non_tunai')
        komunitas_adat_terpencil 			= request.POST.get('komunitas_adat_terpencil')

        if edit_datasosial is not None:
            
            edit_datasosial.tahun = tahun
            edit_datasosial.penanggungjawab_id             = penanggungjawab_id
            edit_datasosial.kampung_id                     = kampung		
            edit_datasosial.jml_kartu_kip 		            = jml_kartu_kip 	
            edit_datasosial.jml_bantuan_s_miskin 		    = jml_bantuan_s_miskin 	
            edit_datasosial.jml_kartu_kis 		            = jml_kartu_kis 	
            edit_datasosial.jml_user_bpjs 		            = jml_user_bpjs 	
            edit_datasosial.jml_user_bpjs_mandiri 		    = jml_user_bpjs_mandiri 	
            edit_datasosial.jml_bpjs_ketenagakerjaan 		= jml_bpjs_ketenagakerjaan 	
            edit_datasosial.program_keluarga_harapan 		= program_keluarga_harapan 	
            edit_datasosial.bantuan_pangan_non_tunai 		= bantuan_pangan_non_tunai 	
            edit_datasosial.komunitas_adat_terpencil 		= komunitas_adat_terpencil 
            
            edit_datasosial.save()
            

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_sosial')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataSosial.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataSosial.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataSosial.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataSosial.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)