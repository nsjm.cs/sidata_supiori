from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pemerintahan = dataPemerintahan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPemerintahan.objects.filter(status='Tidak aktif')
    paginator = Paginator(dt_pemerintahan, 15)
    try:
        dt_pemerintahan = paginator.page(page)
    except PageNotAnInteger:
        dt_pemerintahan = paginator.page(1)
    except EmptyPage:
        dt_pemerintahan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Data Pemerinatahan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_pemerintahan_list' : dt_pemerintahan,
        'archives' : archives,

    }
    
    return render(request, 'profile/admin/pemerintahan/index.html', context)

@login_required
@is_verified()
def create(request):
    form = ''
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')


        luas_wilayah 				= request.POST.get('luas_wilayah')
        rt 							= request.POST.get('rt')
        dusun						= request.POST.get('dusun')
        batas_utara					= request.POST.get('batas_utara')
        batas_selatan				= request.POST.get('batas_selatan')
        batas_barat					= request.POST.get('batas_barat')
        batas_timur					= request.POST.get('batas_timur')
        jml_aparatur_kampung		= request.POST.get('jml_aparatur_kampung')
        pendidikan_aparatur_sd		= request.POST.get('pendidikan_aparatur_sd')
        pendidikan_aparatur_smp		= request.POST.get('pendidikan_aparatur_smp')
        pendidikan_aparatur_sma		= request.POST.get('pendidikan_aparatur_sma')
        pendidikan_aparatur_d3		= request.POST.get('pendidikan_aparatur_d3')
        pendidikan_aparatur_s1		= request.POST.get('pendidikan_aparatur_s1')
        jml_basmuskam				= request.POST.get('jml_basmuskam')
        pendidikan_bamuskam_sd		= request.POST.get('pendidikan_bamuskam_sd')
        pendidikan_bamuskam_smp		= request.POST.get('pendidikan_bamuskam_smp')
        pendidikan_bamuskam_sma		= request.POST.get('pendidikan_bamuskam_sma')
        pendidikan_bamuskam_d3		= request.POST.get('pendidikan_bamuskam_d3')
        pendidikan_bamuskam_s1		= request.POST.get('pendidikan_bamuskam_s1')
        jml_ketua_rt				= request.POST.get('jml_ketua_rt')
        jml_kepala_dusun			= request.POST.get('jml_kepala_dusun')
        jml_Babinkamtibmas			= request.POST.get('jml_Babinkamtibmas')
        jml_Babinsa					= request.POST.get('jml_Babinsa')
        kantor						= request.POST.get('kantor')
        status_kantor				= request.POST.get('status_kantor')

       
        if kampung is not None:
            insert_data_pemerintahan_                       = dataPemerintahan()
            insert_data_pemerintahan_.tahun                 = tahun
            insert_data_pemerintahan_.penanggungjawab_id    = penanggungjawab_id
            insert_data_pemerintahan_.kampung_id            = kampung

            insert_data_pemerintahan_.luas_wilayah 				= luas_wilayah
            insert_data_pemerintahan_.rt 							= rt
            insert_data_pemerintahan_.dusun						= dusun
            insert_data_pemerintahan_.batas_utara					= batas_utara
            insert_data_pemerintahan_.batas_selatan				= batas_selatan
            insert_data_pemerintahan_.batas_barat					= batas_barat
            insert_data_pemerintahan_.batas_timur					= batas_timur
            insert_data_pemerintahan_.jml_aparatur_kampung		= jml_aparatur_kampung
            insert_data_pemerintahan_.pendidikan_aparatur_sd		= pendidikan_aparatur_sd
            insert_data_pemerintahan_.pendidikan_aparatur_smp		= pendidikan_aparatur_smp
            insert_data_pemerintahan_.pendidikan_aparatur_sma		= pendidikan_aparatur_sma
            insert_data_pemerintahan_.pendidikan_aparatur_d3		= pendidikan_aparatur_d3
            insert_data_pemerintahan_.pendidikan_aparatur_s1		= pendidikan_aparatur_s1
            insert_data_pemerintahan_.jml_basmuskam				= jml_basmuskam
            insert_data_pemerintahan_.pendidikan_bamuskam_sd		= pendidikan_bamuskam_sd
            insert_data_pemerintahan_.pendidikan_bamuskam_smp		= pendidikan_bamuskam_smp
            insert_data_pemerintahan_.pendidikan_bamuskam_sma		= pendidikan_bamuskam_sma
            insert_data_pemerintahan_.pendidikan_bamuskam_d3		= pendidikan_bamuskam_d3
            insert_data_pemerintahan_.pendidikan_bamuskam_s1		= pendidikan_bamuskam_s1
            insert_data_pemerintahan_.jml_ketua_rt				= jml_ketua_rt
            insert_data_pemerintahan_.jml_kepala_dusun			= jml_kepala_dusun
            insert_data_pemerintahan_.jml_Babinkamtibmas			= jml_Babinkamtibmas
            insert_data_pemerintahan_.jml_Babinsa					= jml_Babinsa
            insert_data_pemerintahan_.kantor						= kantor
            insert_data_pemerintahan_.status_kantor				= status_kantor
            
           

            insert_data_pemerintahan_.save()
            messages.success(request, 'Data Pemerintahan berhasil disimpan.')
            return redirect('profile:admin_pemerintahan')
        messages.error(request, 'Data Pemerintahan gagal disimpan.')
        return render(request, 'profile/admin/pemerintahan/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    # Ambil objek dari database atau tampilkan halaman 404 jika tidak ditemukan
    edit_datapemerintahan = get_object_or_404(dataPemerintahan, id=id)

    if request.method == 'POST':
        # Ambil nilai dari POST request
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        luas_wilayah 				= request.POST.get('luas_wilayah')
        rt 							= request.POST.get('rt')
        dusun						= request.POST.get('dusun')
        batas_utara					= request.POST.get('batas_utara')
        batas_selatan				= request.POST.get('batas_selatan')
        batas_barat					= request.POST.get('batas_barat')
        batas_timur					= request.POST.get('batas_timur')
        jml_aparatur_kampung		= request.POST.get('jml_aparatur_kampung')
        pendidikan_aparatur_sd		= request.POST.get('pendidikan_aparatur_sd')
        pendidikan_aparatur_smp		= request.POST.get('pendidikan_aparatur_smp')
        pendidikan_aparatur_sma		= request.POST.get('pendidikan_aparatur_sma')
        pendidikan_aparatur_d3		= request.POST.get('pendidikan_aparatur_d3')
        pendidikan_aparatur_s1		= request.POST.get('pendidikan_aparatur_s1')
        jml_basmuskam				= request.POST.get('jml_basmuskam')
        pendidikan_bamuskam_sd		= request.POST.get('pendidikan_bamuskam_sd')
        pendidikan_bamuskam_smp		= request.POST.get('pendidikan_bamuskam_smp')
        pendidikan_bamuskam_sma		= request.POST.get('pendidikan_bamuskam_sma')
        pendidikan_bamuskam_d3		= request.POST.get('pendidikan_bamuskam_d3')
        pendidikan_bamuskam_s1		= request.POST.get('pendidikan_bamuskam_s1')
        jml_ketua_rt				= request.POST.get('jml_ketua_rt')
        jml_kepala_dusun			= request.POST.get('jml_kepala_dusun')
        jml_Babinkamtibmas			= request.POST.get('jml_Babinkamtibmas')
        jml_Babinsa					= request.POST.get('jml_Babinsa')
        kantor						= request.POST.get('kantor')
        status_kantor				= request.POST.get('status_kantor')

        # Perbarui nilai pada objek yang sudah ada
        edit_datapemerintahan.tahun = tahun
        edit_datapemerintahan.penanggungjawab_id = penanggungjawab_id
        edit_datapemerintahan.kampung_id = kampung

        edit_datapemerintahan.luas_wilayah 				= luas_wilayah
        edit_datapemerintahan.rt 							= rt
        edit_datapemerintahan.dusun						= dusun
        edit_datapemerintahan.batas_utara					= batas_utara
        edit_datapemerintahan.batas_selatan				= batas_selatan
        edit_datapemerintahan.batas_barat					= batas_barat
        edit_datapemerintahan.batas_timur					= batas_timur
        edit_datapemerintahan.jml_aparatur_kampung		= jml_aparatur_kampung
        edit_datapemerintahan.pendidikan_aparatur_sd		= pendidikan_aparatur_sd
        edit_datapemerintahan.pendidikan_aparatur_smp		= pendidikan_aparatur_smp
        edit_datapemerintahan.pendidikan_aparatur_sma		= pendidikan_aparatur_sma
        edit_datapemerintahan.pendidikan_aparatur_d3		= pendidikan_aparatur_d3
        edit_datapemerintahan.pendidikan_aparatur_s1		= pendidikan_aparatur_s1
        edit_datapemerintahan.jml_basmuskam				= jml_basmuskam
        edit_datapemerintahan.pendidikan_bamuskam_sd		= pendidikan_bamuskam_sd
        edit_datapemerintahan.pendidikan_bamuskam_smp		= pendidikan_bamuskam_smp
        edit_datapemerintahan.pendidikan_bamuskam_sma		= pendidikan_bamuskam_sma
        edit_datapemerintahan.pendidikan_bamuskam_d3		= pendidikan_bamuskam_d3
        edit_datapemerintahan.pendidikan_bamuskam_s1		= pendidikan_bamuskam_s1
        edit_datapemerintahan.jml_ketua_rt				= jml_ketua_rt
        edit_datapemerintahan.jml_kepala_dusun			= jml_kepala_dusun
        edit_datapemerintahan.jml_Babinkamtibmas			= jml_Babinkamtibmas
        edit_datapemerintahan.jml_Babinsa					= jml_Babinsa
        edit_datapemerintahan.kantor						= kantor
        edit_datapemerintahan.status_kantor				= status_kantor

        # Simpan perubahan
        edit_datapemerintahan.save()

        messages.success(request, 'Distrik berhasil Diubah')
        return redirect('profile:admin_pemerintahan')
        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPemerintahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPemerintahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPemerintahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPemerintahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)