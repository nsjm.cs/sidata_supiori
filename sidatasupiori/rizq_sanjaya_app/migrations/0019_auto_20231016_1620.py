# Generated by Django 3.2 on 2023-10-16 09:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0018_dataperikanan'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dataperikanan',
            name='bantuan',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='bantuan_budidaya',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='foto_budidaya',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='foto_kub',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='jenis_komoditi',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='jenisinput',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='kub',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='nama_budidaya',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='nama_ketua',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='pelatihan',
        ),
        migrations.RemoveField(
            model_name='dataperikanan',
            name='sk_kub',
        ),
    ]
