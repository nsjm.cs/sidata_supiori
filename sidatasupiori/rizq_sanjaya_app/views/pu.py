from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_pu = dataPU.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPU.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_pu, 15)
    try:
        data_pu = paginator.page(page)
    except PageNotAnInteger:
        data_pu = paginator.page(1)
    except EmptyPage:
        data_pu = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data PU',
        'data_pu_list' : data_pu,
        'archives' : archives,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
    }
    
    return render(request, 'profile/admin/pu/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataPU.objects.all()
        data_pu_ = dataPU.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Tambah Data Pekerjaan Umum',
            'form' : form,
            'data_pu_list':data_pu_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/pu/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        jln_beraspal	 			=  request.POST.get('jln_beraspal')
        jln_hampar	 			    =  request.POST.get('jln_hampar')
        jln_rusak	 			    =  request.POST.get('jln_rusak')
        jln_tani	 			    =  request.POST.get('jln_tani')
        jln_tani_blm_aspal	 	    =  request.POST.get('jln_tani_blm_aspal')
        foto_jln_beraspal     		=  request.FILES['foto_jln_beraspal']
        foto_jln_hampar 			=  request.FILES['foto_jln_hampar']
        foto_jln_rusak 				=  request.FILES['foto_jln_rusak']
        foto_jln_tani 				=  request.FILES['foto_jln_tani']
        foto_jln_tani_blm_aspal 	=  request.FILES['foto_jln_tani_blm_aspal']
        jmt_beton	 			    =  request.POST.get('jmt_beton')
        kondisi_jmt	 			    =  request.POST.get('kondisi_jmt')
        jmt_kayu	 			    =  request.POST.get('jmt_kayu')
        kondisi_jmt_kayu	 	    =  request.POST.get('kondisi_jmt_kayu')
        foto_jmt_beton     		    =  request.FILES['foto_jmt_beton']
        foto_kondisi_jmt 			=  request.FILES['foto_kondisi_jmt'] 
        foto_jmt_kayu 				=  request.FILES['foto_jmt_kayu']
        foto_kondisi_jmt_kayu 		=  request.FILES['foto_kondisi_jmt_kayu']
        
       
        if kampung is not None:
            data_pu_ = dataPU()
            data_pu_.tahun = tahun
            data_pu_.penanggungjawab_id = penanggungjawab_id
            data_pu_.kampung_id = kampung

            data_pu_.jln_beraspal	 			= jln_beraspal	 			
            data_pu_.jln_hampar	 			    = jln_hampar	 			    
            data_pu_.jln_rusak	 			    = jln_rusak	 			    
            data_pu_.jln_tani	 			    = jln_tani	 			    
            data_pu_.jln_tani_blm_aspal	 	    = jln_tani_blm_aspal	 	    
            data_pu_.foto_jln_beraspal     		= foto_jln_beraspal     		
            data_pu_.foto_jln_hampar 			= foto_jln_hampar 			
            data_pu_.foto_jln_rusak 			= foto_jln_rusak 				
            data_pu_.foto_jln_tani 				= foto_jln_tani 				
            data_pu_.foto_jln_tani_blm_aspal 	= foto_jln_tani_blm_aspal 	
            data_pu_.jmt_beton	 			    = jmt_beton	 			    
            data_pu_.kondisi_jmt	 			= kondisi_jmt	 			    
            data_pu_.jmt_kayu	 			    = jmt_kayu	 			    
            data_pu_.kondisi_jmt_kayu	 	    = kondisi_jmt_kayu	 	    
            data_pu_.foto_jmt_beton     		= foto_jmt_beton     		    
            data_pu_.foto_kondisi_jmt 			= foto_kondisi_jmt 			  
            data_pu_.foto_jmt_kayu 				= foto_jmt_kayu 				
            data_pu_.foto_kondisi_jmt_kayu 		= foto_kondisi_jmt_kayu 		
            
            data_pu_.save()
            messages.success(request, 'Data PU berhasil disimpan.')
            return redirect('profile:admin_pu')
        messages.error(request, 'Data Kominfo gagal disimpan.')
        return render(request, 'profile/admin/pu/create.html', {'form': form,})

@login_required
@is_verified()
def detail(request, id):
    template = ''
    context = {} 
    
    if request.method == 'GET':
        data_detail_pu = dataPU.objects.get(id = id)
        context = {
            'title' : 'Data Pekerjaan Umum',
            'data_detail_pu' : data_detail_pu,
        }
        template = 'profile/admin/pu/detail.html'
        return render(request, template, context)
    
@login_required
@is_verified()
def detail_(request, id):
    template = ''
    context = {} 
    
    if request.method == 'GET':
        data_detail_pu = dataPU.objects.get(id = id)
        context = {
            'title' : 'Data Pekerjaan Umum',
            'data_detail_pu' : data_detail_pu,
        }
        template = 'profile/admin/pu/detail_.html'
        return render(request, template, context)

@login_required
@is_verified()
def edit(request, id):
    edit_data_pekerjaan_umum = get_object_or_404(dataPU, id=id)
    form = ''

    if request.method == 'POST':

        # foto_jln_beraspal     		=  request.FILES['foto_jln_beraspal']
        path_file_lama_foto_jln_beraspal = f"{settings.MEDIA_ROOT}/{dataPU.foto_jln_beraspal}"
        foto_old_foto_jln_beraspal = bool(dataPU.foto_jln_beraspal)
        image_foto_jln_beraspal = request.FILES.get('foto_jln_beraspal')

        # foto_jln_hampar 			=  request.FILES['foto_jln_hampar']
        path_file_lama_foto_jln_hampar = f"{settings.MEDIA_ROOT}/{dataPU.foto_jln_hampar}"
        foto_old_foto_jln_hampar = bool(dataPU.foto_jln_hampar)
        image_foto_jln_hampar = request.FILES.get('foto_jln_hampar')

        # foto_jln_rusak 				=  request.FILES['foto_jln_rusak']
        path_file_lama_foto_jln_rusak = f"{settings.MEDIA_ROOT}/{dataPU.foto_jln_rusak}"
        foto_old_foto_jln_rusak = bool(dataPU.foto_jln_rusak)
        image_foto_jln_rusak = request.FILES.get('foto_jln_rusak')

         # foto_jln_tani 				=  request.FILES['foto_jln_tani']
        path_file_lama_foto_jln_tani = f"{settings.MEDIA_ROOT}/{dataPU.foto_jln_tani}"
        foto_old_foto_jln_tani = bool(dataPU.foto_jln_tani)
        image_foto_jln_tani = request.FILES.get('foto_jln_tani')

        # foto_jln_tani_blm_aspal 	=  request.FILES['foto_jln_tani_blm_aspal']
        path_file_lama_foto_jln_tani_blm_aspal = f"{settings.MEDIA_ROOT}/{dataPU.foto_jln_tani_blm_aspal}"
        foto_old_foto_jln_tani_blm_aspal = bool(dataPU.foto_jln_tani_blm_aspal)
        image_foto_jln_tani_blm_aspal = request.FILES.get('foto_jln_tani_blm_aspal')

        # foto_jmt_beton     		    =  request.FILES['foto_jmt_beton']
        path_file_lama_foto_jmt_beton = f"{settings.MEDIA_ROOT}/{dataPU.foto_jmt_beton}"
        foto_old_foto_jmt_beton = bool(dataPU.foto_jmt_beton)
        image_foto_jmt_beton = request.FILES.get('foto_jmt_beton')

        # foto_kondisi_jmt 			=  request.FILES['foto_kondisi_jmt']
        path_file_lama_foto_kondisi_jmt = f"{settings.MEDIA_ROOT}/{dataPU.foto_kondisi_jmt}"
        foto_old_foto_kondisi_jmt = bool(dataPU.foto_kondisi_jmt)
        image_foto_kondisi_jmt = request.FILES.get('foto_kondisi_jmt')

        # foto_jmt_kayu 				=  request.FILES['foto_jmt_kayu']
        path_file_lama_foto_jmt_kayu = f"{settings.MEDIA_ROOT}/{dataPU.foto_jmt_kayu}"
        foto_old_foto_jmt_kayu = bool(dataPU.foto_jmt_kayu)
        image_foto_jmt_kayu = request.FILES.get('foto_jmt_kayu')   

        # foto_kondisi_jmt_kayu 		=  request.FILES['foto_kondisi_jmt_kayu']
        path_file_lama_foto_kondisi_jmt_kayu = f"{settings.MEDIA_ROOT}/{dataPU.foto_kondisi_jmt_kayu}"
        foto_old_foto_kondisi_jmt_kayu = bool(dataPU.foto_kondisi_jmt_kayu)
        image_foto_kondisi_jmt_kayu = request.FILES.get('foto_kondisi_jmt_kayu')  

        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jln_beraspal	 			=  request.POST.get('jln_beraspal')
        jln_hampar	 			    =  request.POST.get('jln_hampar')
        jln_rusak	 			    =  request.POST.get('jln_rusak')
        jln_tani	 			    =  request.POST.get('jln_tani')
        jln_tani_blm_aspal	 	    =  request.POST.get('jln_tani_blm_aspal')
        
        jmt_beton	 			    =  request.POST.get('jmt_beton')
        kondisi_jmt	 			    =  request.POST.get('kondisi_jmt')
        jmt_kayu	 			    =  request.POST.get('jmt_kayu')
        kondisi_jmt_kayu	 	    =  request.POST.get('kondisi_jmt_kayu')
        
        

        if edit_data_pekerjaan_umum is not None:
            edit_data_pekerjaan_umum.tahun                 = tahun
            edit_data_pekerjaan_umum.penanggungjawab_id    = penanggungjawab_id
            edit_data_pekerjaan_umum.kampung_id            = kampung

            edit_data_pekerjaan_umum.jln_beraspal	 			= jln_beraspal	 			
            edit_data_pekerjaan_umum.jln_hampar	 			    = jln_hampar	 			    
            edit_data_pekerjaan_umum.jln_rusak	 			    = jln_rusak	 			    
            edit_data_pekerjaan_umum.jln_tani	 			    = jln_tani	 			    
            edit_data_pekerjaan_umum.jln_tani_blm_aspal	 	    = jln_tani_blm_aspal	 	    
            
            edit_data_pekerjaan_umum.jmt_beton	 			    = jmt_beton	 			    
            edit_data_pekerjaan_umum.kondisi_jmt	 			= kondisi_jmt	 			    
            edit_data_pekerjaan_umum.jmt_kayu	 			    = jmt_kayu	 			    
            edit_data_pekerjaan_umum.kondisi_jmt_kayu	 	    = kondisi_jmt_kayu	 	    
           	

            edit_data_pekerjaan_umum.save()



            if len(request.FILES) > 0:
                if foto_old_foto_jln_beraspal : 
                    try:
                        os.remove(path_file_lama_foto_jln_beraspal)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jln_beraspal = image_foto_jln_beraspal
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_jln_hampar : 
                    try:
                        os.remove(path_file_lama_foto_jln_hampar)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jln_hampar = image_foto_jln_hampar
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_jln_rusak : 
                    try:
                        os.remove(path_file_lama_foto_jln_rusak)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jln_rusak = image_foto_jln_rusak
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_jln_tani : 
                    try:
                        os.remove(path_file_lama_foto_jln_tani)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jln_tani = image_foto_jln_tani
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_jln_tani_blm_aspal : 
                    try:
                        os.remove(path_file_lama_foto_jln_tani_blm_aspal)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jln_tani_blm_aspal = image_foto_jln_tani_blm_aspal
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_jmt_beton : 
                    try:
                        os.remove(path_file_lama_foto_jmt_beton)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jmt_beton = image_foto_jmt_beton
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_kondisi_jmt : 
                    try:
                        os.remove(path_file_lama_foto_kondisi_jmt)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_kondisi_jmt = image_foto_kondisi_jmt
                form_foto.save()

            if len(request.FILES) > 0:
                if foto_old_foto_jmt_kayu : 
                    try:
                        os.remove(path_file_lama_foto_jmt_kayu)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_jmt_kayu = image_foto_jmt_kayu
                form_foto.save()
            
            if len(request.FILES) > 0:
                if foto_old_foto_kondisi_jmt_kayu : 
                    try:
                        os.remove(path_file_lama_foto_kondisi_jmt_kayu)
                    except:
                        pass
                form_foto = dataPU.objects.get(id = id)
                form_foto.foto_kondisi_jmt_kayu = image_foto_kondisi_jmt_kayu
                form_foto.save()
            

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_pu')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPU.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPU.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPU.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPU.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)