from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    
    data_perhubungan = dataPerhubungan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPerhubungan.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_perhubungan, 15)
    try:
        data_perhubungan = paginator.page(page)
    except PageNotAnInteger:
        data_perhubungan = paginator.page(1)
    except EmptyPage:
        data_perhubungan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Perhubungan',
        'data_perhubungan_list' : data_perhubungan,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/perhubungan/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataPerhubungan.objects.all()
        data_kominfo_ = dataPerhubungan.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data Kominfo',
            'form' : form,
            'data_kominfo_list':data_kominfo_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/perhubungan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        

        jml_halte	 			    = request.POST.get('jml_halte') 
        jml_terminal	 			= request.POST.get('jml_terminal') 
        jml_dermaga		 			= request.POST.get('jml_dermaga') 
        jml_tambatan_perahu		 	= request.POST.get('jml_tambatan_perahu') 
        kondisi_dermaga			 	= request.POST.get('kondisi_dermaga')
        kondisi_tambatan_perahu		= request.POST.get('kondisi_tambatan_perahu')
        foto_halte 					=  request.FILES['foto_halte']
        foto_terminal 				=  request.FILES['foto_terminal']
        foto_dermaga 				=  request.FILES['foto_dermaga']
        foto_tambatan_perahu 		=  request.FILES['foto_tambatan_perahu']

        if kampung is not None:
            data_perhubungan_ = dataPerhubungan()
            data_perhubungan_.tahun = tahun
            data_perhubungan_.penanggungjawab_id = penanggungjawab_id
            data_perhubungan_.kampung_id = kampung

            data_perhubungan_.jml_halte	 			    = jml_halte	 			    
            data_perhubungan_.jml_terminal	 			= jml_terminal	 		
            data_perhubungan_.jml_dermaga		 		= jml_dermaga		 			
            data_perhubungan_.jml_tambatan_perahu		= jml_tambatan_perahu		 	
            data_perhubungan_.kondisi_dermaga			= kondisi_dermaga		 
            data_perhubungan_.kondisi_tambatan_perahu	= kondisi_tambatan_perahu	
            data_perhubungan_.foto_halte 				= foto_halte 				
            data_perhubungan_.foto_terminal 			= foto_terminal 			
            data_perhubungan_.foto_dermaga 				= foto_dermaga 			
            data_perhubungan_.foto_tambatan_perahu 		= foto_tambatan_perahu 	

            data_perhubungan_.save()
            messages.success(request, 'Data perhubungan berhasil disimpan.')
            return redirect('profile:admin_perhubungan')
        messages.error(request, 'Data perhubungan gagal disimpan.')
        return render(request, 'profile/admin/perhubungan/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_distriks = dataDistrik.objects.get(id = id)

        context = {
            'title' : 'EDIT DISTRIK',
            'form' : form,
            'edit' : 'true',
            'data_distriks' : data_distriks,
        }
        template = 'profile/admin/distrik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_distrik = dataDistrik.objects.get(id = id)
        title = request.POST.get('title')

        if data_distrik is not None:
            data_distrik.title = title
            data_distrik.save()
            data_distrik.save()

            messages.success(request, 'Distrik berhasil disimpan')
            return redirect('profile:admin_distrik')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerhubungan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPerhubungan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerhubungan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPerhubungan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)