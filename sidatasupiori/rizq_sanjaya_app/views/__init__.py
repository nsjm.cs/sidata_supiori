from .admin import *
from .dashboard import *
from .gallery import *
from .news import *
from .tags import *
from .user import *
from .setting import *
from .layanan import *
from .services import *

from .halaman import *
from .product import *
from .kata import *
from .layanan_ import *
from .aksesbilitas import *
from .infoterpadu import *
from .profilskpd import *
from .laporan import *
# data unutk aplikasi sidata supiori
from .distrik import *
from .kampung import *
from .umkm import *
from .penanggungjawab import *
from .lembaga import *
from .kominfo_ import *
from .pu import *
from .perhubungan import *
from .sosial import *
from .perikanan import *
from .fasilitasolahraga import *
from .pertanian import *
from .kependudukan import *
from .pemerintahan import *
from .kesehatan import *
from .perumahan import *
from .pendidikan import *



