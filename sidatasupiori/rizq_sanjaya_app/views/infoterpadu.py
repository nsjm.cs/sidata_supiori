from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import InfoTerpadu
from django.utils import timezone
import os
from ..decorators import *
from datetime import datetime
from django.conf import settings


####################################START MANAGEMEN VIEW INFOGRAFIS####################################
@require_http_methods(["GET"])
def admin_index_infografis(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='infografis',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='infografis',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title'            : 'Infografis',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/infografis/index.html', context)

####################################CREATE INFOGRAFIS#################################################
@login_required
@is_verified()
def admin_index_infografis_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpadu.objects.all()
        context = {
            'title' : 'ADD INFOGRAFIS',
            'form' : form,
        }

        template = 'profile/admin/infoterpadu/infografis/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        typesx = 'infografis'
        foto = request.FILES['foto']
        if title is not None:
            informasiterpadu = InfoTerpadu()
            informasiterpadu.title = title
            informasiterpadu.typesx = typesx
            informasiterpadu.description = description
            informasiterpadu.foto = foto
            informasiterpadu.save()
            
            messages.success(request, 'Infografis berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_infografis')

        messages.error(request, 'Infografis gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/infografis/create.html', {'form': form,})
    

####################################START MANAGEMEN VIEW INFORMASI PUBLIK####################################
@require_http_methods(["GET"])
def admin_index_infopublik(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='infopublik',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='infopublik',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title'            : 'Informasi Terpadu',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/infopublik/index.html', context)

####################################CREATE INFOPUBLIK#################################################
@login_required
@is_verified()
def admin_index_infopublik_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpadu.objects.all()
        context = {
            'title' : 'ADD INFO PUBLIK',
            'form' : form,
        }

        template = 'profile/admin/infoterpadu/infopublik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        typexs = 'infopublik'
        foto = request.FILES['foto']
        dokumen = request.FILES['dokumen']
        if title is not None:
            informasiterpadu = InfoTerpadu()
            informasiterpadu.title = title
            informasiterpadu.typexs = typexs
            informasiterpadu.description = description
            informasiterpadu.foto = foto
            informasiterpadu.dokumen = dokumen
            informasiterpadu.save()
            
            messages.success(request, 'infopublik berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_infopublik')

        messages.error(request, 'infopublik gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/infopublik/create.html', {'form': form,})

####################################START MANAGEMEN VIEW REGULASI####################################
@require_http_methods(["GET"])
def admin_index_regulasi(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='regulasi',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='regulasi',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title'            : 'Data Regulasi',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/regulasi/index.html', context)
    
####################################CREATE REGULASI#################################################
@login_required
@is_verified()
def admin_index_regulasi_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpadu.objects.all()
        context = {
            'title' : 'ADD REGULASI',
            'form' : form,
        }

        template = 'profile/admin/infoterpadu/regulasi/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        typexs = 'regulasi'
        foto = request.FILES['foto']
        dokumen = request.FILES['dokumen']
        if title is not None:
            informasiterpadu = InfoTerpadu()
            informasiterpadu.title = title
            informasiterpadu.typexs = typexs
            informasiterpadu.description = description
            informasiterpadu.foto = foto
            informasiterpadu.dokumen = dokumen
            informasiterpadu.save()
            
            messages.success(request, 'Regulasi berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_regulasi')

        messages.error(request, 'Regulasi gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/regulasi/create.html', {'form': form,})

####################################START MANAGEMEN VIEW KALENDER PENDIDIKAN####################################
@require_http_methods(["GET"])
def admin_index_kalender_pendidikan(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='kalender_pendidikan',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='kalender_pendidikan',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title'            : 'Data Kalender Pendidikan',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/kalender_pendidikan/index.html', context)
    
####################################CREATE KALENDER PENDIDIKAN#################################################
@login_required
@is_verified()
def admin_index_kalender_pendidikan_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpadu.objects.all()
        context = {
            'title' : 'ADD KALENDER PENDIDIKAN',
            'form' : form,
        }

        template = 'profile/admin/infoterpadu/kalender_pendidikan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        typexs = 'kalender_pendidikan'
        foto = request.FILES['foto']
        dokumen = request.FILES['dokumen']
        if title is not None:
            informasiterpadu = InfoTerpadu()
            informasiterpadu.title = title
            informasiterpadu.typexs = typexs
            informasiterpadu.description = description
            informasiterpadu.foto = foto
            informasiterpadu.dokumen = dokumen
            informasiterpadu.save()
            
            messages.success(request, 'Kalender Pendidikan berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_kalender_pendidikan')

        messages.error(request, 'Kalender Pendidikan gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/regulasi/create.html', {'form': form,})
    
####################################START MANAGEMEN VIEW AGENDA####################################
@require_http_methods(["GET"])
def admin_index_agenda(request):
    infoterpadu = InfoTerpadu.objects.filter(typexs='agenda',status='Publish').order_by('-id')
    arsip = InfoTerpadu.objects.filter(typexs='agenda',status='Draft').order_by('-id')
    for x in infoterpadu:
        ext = os.path.splitext(settings.MEDIA_ROOT+str(x.dokumen))[-1].lower()

        if ext == '.pdf':
            x.filex = 'file-pdf'
        elif ext in ['.jpg','.jpeg','.png']:
            x.filex = 'file-image'
        else:
            x.filex = 'dash'

    context = {
        'title'            : 'Data Agenda',
        'data_infoterpadu' : infoterpadu,
        'arsip'            : arsip,
    }
    return render(request, 'profile/admin/infoterpadu/agenda/index.html', context)
    
####################################CREATE AGENDA#################################################
@login_required
@is_verified()
def admin_index_agenda_create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = InfoTerpadu.objects.all()
        context = {
            'title' : 'ADD AGENDA',
            'form' : form,
        }

        template = 'profile/admin/infoterpadu/agenda/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        typexs = 'agenda'
        foto = request.FILES['foto']
        dokumen = request.FILES['dokumen']
        start = request.POST.get('start_event_date')
        end = request.POST.get('end_event_date')
        if title is not None:
            informasiterpadu = InfoTerpadu()
            informasiterpadu.title = title
            informasiterpadu.typexs = typexs
            informasiterpadu.description = description
            informasiterpadu.foto = foto
            informasiterpadu.dokumen = dokumen
            informasiterpadu.start_event_date = start
            informasiterpadu.end_event_date = end
            informasiterpadu.save()
            
            messages.success(request, 'Regulasi berhasil disimpan.')
            return redirect('profile:admin_infoterpadu_agenda')

        messages.error(request, 'Regulasi gagal disimpan.')
        return render(request, 'profile/admin/infoterpadu/agenda/create.html', {'form': form,})

####################################END MANAGEMEN VIEW INFOGRAFIS###########################################################
#SOFT DELETE & RESTORE INI DIGUNAKAN UNTUK SOFTDELETE PADA MENU INFOGRAFIS, INFOPUBLIK, AGENDA, REGULASI & KALENDER AKADEMIK
######SOFTDELETE DATA INFOTERPADU###########################################################################################
@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = InfoTerpadu.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except InfoTerpadu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

######RESTORE DATA INFOTERPADU###########################################################################################
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        status = 'Publish'
        doc = InfoTerpadu.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except InfoTerpadu.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)    
######END RESTORE DATA INFOTERPADU#######################################################################################