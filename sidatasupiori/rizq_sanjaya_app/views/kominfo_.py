from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_kominfo = dataKominfo.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataKominfo.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_kominfo, 15)
    try:
        data_kominfo = paginator.page(page)
    except PageNotAnInteger:
        data_kominfo = paginator.page(1)
    except EmptyPage:
        data_kominfo = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data data_kominfo',
        'data_kominfo_list' : data_kominfo,
        'datakampung_list':kampung,
        'datajawab_list':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/kominfo/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataKominfo.objects.all()
        data_kominfo_ = dataKominfo.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data Kominfo',
            'form' : form,
            'data_kominfo_list':data_kominfo_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/kominfo/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        jml_bts = request.POST.get('jml_bts')
        jml_vsat = request.POST.get('jml_vsat')
        akses_internet = request.POST.get('akses_internet')
        foto = request.FILES['foto']
       
        if kampung is not None:
            data_kominfo_ = dataKominfo()
            data_kominfo_.tahun = tahun
            data_kominfo_.penanggungjawab_id = penanggungjawab_id
            data_kominfo_.jml_bts = jml_bts
            data_kominfo_.jml_vsat = jml_vsat
            data_kominfo_.akses_internet = akses_internet
            data_kominfo_.foto = foto
            data_kominfo_.kampung_id = kampung
            data_kominfo_.save()
            messages.success(request, 'Data Kominfo berhasil disimpan.')
            return redirect('profile:admin_kominfo')
        messages.error(request, 'Data Kominfo gagal disimpan.')
        return render(request, 'profile/admin/kominfo/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    edit_data_kominfo = get_object_or_404(dataKominfo, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataKominfo.foto}"
        foto_old = bool(dataKominfo.foto)
        image = request.FILES.get('foto')


        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        jml_bts = request.POST.get('jml_bts')
        jml_vsat = request.POST.get('jml_vsat')
        akses_internet = request.POST.get('akses_internet')

        if edit_data_kominfo is not None:
            edit_data_kominfo.tahun = tahun
            edit_data_kominfo.kampung_id = kampung
            edit_data_kominfo.penanggungjawab_id = penanggungjawab_id

            edit_data_kominfo.jml_bts = jml_bts
            edit_data_kominfo.jml_vsat = jml_vsat
            edit_data_kominfo.akses_internet = akses_internet
            
            

            edit_data_kominfo.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataKominfo.objects.get(id = id)
                form_foto.foto = image
                form_foto.save()

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_kominfo')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKominfo.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataKominfo.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKominfo.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataKominfo.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)