from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Agenda
from django.utils import timezone
import os
import json
from ..decorators import *
from datetime import datetime

@require_http_methods(["GET"])
def index(request):
    agenda = Agenda.objects.filter(deleted_at=None).order_by('start_date')
    date_format = '%Y-%m-%d'
    for x in agenda:
        a = datetime.strptime(str(x.start_date), date_format)
        b = datetime.strptime(str(x.finish_date), date_format)
        delta = b - a
        x.days = delta.days + 1
        x.everys = 'Ya' if x.every_year == 'true' else 'Tidak'

    runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
    for x in runlink:
        x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

    context = {
        'title' : 'Daftar Agenda',
        'agendas' : agenda,
        'runlink' : runlink,
        'is_detail' : False,
    }
    return render(request, 'profile/agenda/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    agenda = Agenda.objects.all().order_by('start_date')
    archives = Agenda.objects.filter(deleted_at__isnull=False).order_by('start_date')
    context = {
        'title' : 'Agenda - Admin',
        'agendas' : agenda,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/agenda/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = AgendaForm()
        context = {
            'title' : 'ADD AGENDA',
            'form' : form,
        }

        template = 'profile/admin/agenda/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = AgendaForm(data=request.POST)
        if form.is_valid():
            new_agenda = form.save(commit=False)
            new_agenda.created_by_id = request.user.id
            new_agenda.last_updated_by = request.user.id
            new_agenda.save()
            agenda =Agenda.objects.get(id = new_agenda.id)
            messages.success(request, 'Agenda berhasil disimpan.')
            return redirect('profile:admin_agenda',)

        messages.error(request, 'Agenda gagal disimpan.')
        return render(request, 'profile/admin/agenda/create.html', {'form': form})

@require_http_methods(["GET"])
def detail(request, slug):
    template = 'profile/agenda/detail.html'
    try:
        agenda = Agenda.objects.get(slug=slug)
    except Agenda.DoesNotExist:
        agenda = None
    context = {
        'title' : agenda.name if agenda != None else 'TIDAK DITEMUKAN',
        'agenda' : agenda,
    }
    return render(request, template, context)

@login_required
@is_verified()
def edit(request, slug):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        try:
            agenda = Agenda.objects.get(slug=slug)
            form = AgendaForm(instance=agenda)
            slug = agenda.slug
        except:
            agenda = None
            form = AgendaForm()
            slug = None

        context = {
            'title' : 'EDIT AGENDA',
            'form' : form,
            'edit' : 'true',
            'slug' : slug,
            'agenda': agenda,
        }
        template = 'profile/admin/agenda/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        agenda = Agenda.objects.get(slug=slug)
        form = AgendaForm(data=request.POST, files=request.FILES, instance=agenda)
        if form.is_valid():
            agenda.last_updated_by = request.user.id
            agenda.save()
            messages.success(request, 'Agenda berhasil disimpan.')
            return redirect('profile:admin_agenda')

        messages.error(request, 'Agenda gagal disimpan.')
        return render(request, 'profile/admin/agenda/create.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/agenda/detail.html'
    try:
        agenda = Agenda.objects.get(slug=slug)
    except Agenda.DoesNotExist:
        agenda = None
    context = {
        'title' : agenda.name if agenda != None else 'TIDAK DITEMUKAN',
        'agenda' : agenda,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        agenda = Agenda.objects.get(slug=slug)
        agenda.deleted_at = sekarang
        agenda.save()
        message = 'success'
    except Agenda.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        agenda = Agenda.objects.get(slug=slug)
        agenda.delete()
        message = 'success'
    except Agenda.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        agenda = Agenda.objects.get(slug=slug)
        agenda.deleted_at = None
        agenda.save()
        message = 'success'
    except Agenda.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@require_http_methods(["GET"])
def get_agenda(request):
    agendas = Agenda.objects.filter(deleted_at=None).order_by('-created_at')
    agenda_json = []
    
    for agenda in agendas:
        mulai = str(agenda.start_date).split('-')
        mulai_agenda = f"{mulai[1]}/{mulai[2]}/{mulai[0]}"
        mulai_badge = f"{mulai[2]}/{mulai[1]}"
        if agenda.finish_date:
            selesai = str(agenda.finish_date).split('-')
            selesai_agenda = f"{selesai[1]}/{selesai[2]}/{selesai[0]}"
            selesai_badge = f"{selesai[2]}/{selesai[1]}"
        badge = 'No Time'
        if agenda.start_time:
            badge = f"{str(agenda.start_time)[:5]} - {str(agenda.finish_time)[:5]}" if agenda.finish_time else str(str(agenda.start_time)[:5])
        dict = { 
                'id': agenda.slug,
                'badge': badge,
                'name': agenda.name,
                'date':  [mulai_agenda, selesai_agenda] if agenda.finish_date else str(mulai_agenda),
                'description': agenda.description,
                'everyYear': agenda.every_year,
                'color' : agenda.color
        }
        agenda_json.append(dict)
    return HttpResponse(json.dumps({'agenda': agenda_json}))

@require_http_methods(["GET"])
def get_agenda_detail(request, slug):
    favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:5]
    runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
    for x in runlink:
        x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

    try:
        date_format = '%Y-%m-%d'
        agenda = Agenda.objects.get(slug=slug)
        a = datetime.strptime(str(agenda.start_date), date_format)
        b = datetime.strptime(str(agenda.finish_date), date_format)
        delta = b - a
        agenda.days = delta.days + 1
        agenda.everys = 'Rutin Setiap Tahun' if agenda.every_year == 'true' else 'Hanya Sekali'

    except Agenda.DoesNotExist:
        agenda = None

    context = {
        'title' : 'Detail Agenda',
        'agendas' : agenda,
        'runlink' : runlink,
        'is_detail' : True,
        'favs' : favorits,
    }

    return render(request, 'profile/agenda/index.html', context)