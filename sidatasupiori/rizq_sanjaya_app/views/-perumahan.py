from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_perumahan = dataPerumahan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPerumahan.objects.filter(status='Tidak aktif')
    
    paginator = Paginator(dt_perumahan, 15)
    try:
        dt_perumahan = paginator.page(page)
    except PageNotAnInteger:
        dt_perumahan = paginator.page(1)
    except EmptyPage:
        dt_perumahan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Perumahan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_perumahan_list' : dt_perumahan,
        'archives' : archives,
        

    }
    
    return render(request, 'profile/admin/perumahan/index.html', context)

@login_required
@is_verified()
def create(request):
    form = ''
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')


        jml_rumah_layak_huni		= request.POST.get('jml_rumah_layak_huni')
        jml_rumah_tdk_layak_huni	= request.POST.get('jml_rumah_tdk_layak_huni')
        jml_rumah_semi_permanen		= request.POST.get('jml_rumah_semi_permanen')
        jml_rumah_permanen			= request.POST.get('jml_rumah_permanen')
        foto_rumah_semi_permanen	= request.FILES['foto_rumah_semi_permanen']
        foto_rumah_permanen			= request.FILES['foto_rumah_permanen']
        jml_total_rumah				= request.POST.get('jml_total_rumah')
        lantai_tanah				= request.POST.get('lantai_tanah')
        lantai_kayu					= request.POST.get('lantai_kayu')
        lantai_semen				= request.POST.get('lantai_semen')
        lantai_keramik				= request.POST.get('lantai_keramik')
        dinding_kayu				= request.POST.get('dinding_kayu')
        dinding_semen				= request.POST.get('dinding_semen')
        dinding_anyaman				= request.POST.get('dinding_anyaman')
        atap_seng					= request.POST.get('atap_seng')
        atap_daun					= request.POST.get('atap_daun')
        sumur_gali_pribadi			= request.POST.get('sumur_gali_pribadi')
        sumur_umum					= request.POST.get('sumur_umum')
        spam						= request.POST.get('spam')
        air_sungai					= request.POST.get('air_sungai')
        jamban_rumah				= request.POST.get('jamban_rumah')
        jamban_umum 				= request.POST.get('jamban_umum')
        foto_jamban_rumah			= request.FILES['foto_jamban_rumah']
        foto_jamban_umum			= request.FILES['foto_jamban_umum']
        rumah_listrik				= request.POST.get('rumah_listrik')
        penerangan_lain				= request.POST.get('penerangan_lain')



       
        if kampung is not None:
            insert_data_perumahan_                       = dataPerumahan()
            insert_data_perumahan_.tahun                 = tahun
            insert_data_perumahan_.penanggungjawab_id    = penanggungjawab_id
            insert_data_perumahan_.kampung_id            = kampung

            insert_data_perumahan_.jml_rumah_layak_huni		    =  jml_rumah_layak_huni
            insert_data_perumahan_.jml_rumah_tdk_layak_huni	    =  jml_rumah_tdk_layak_huni
            insert_data_perumahan_.jml_rumah_semi_permanen		=  jml_rumah_semi_permanen
            insert_data_perumahan_.jml_rumah_permanen			=  jml_rumah_permanen
            insert_data_perumahan_.foto_rumah_semi_permanen	    =  foto_rumah_semi_permanen
            insert_data_perumahan_.foto_rumah_permanen			=  foto_rumah_permanen
            insert_data_perumahan_.jml_total_rumah				=  jml_total_rumah
            insert_data_perumahan_.lantai_tanah				    =  lantai_tanah
            insert_data_perumahan_.lantai_kayu					=  lantai_kayu
            insert_data_perumahan_.lantai_semen				    =  lantai_semen
            insert_data_perumahan_.lantai_keramik				=  lantai_keramik
            insert_data_perumahan_.dinding_kayu				    =  dinding_kayu
            insert_data_perumahan_.dinding_semen				=  dinding_semen
            insert_data_perumahan_.dinding_anyaman				=  dinding_anyaman
            insert_data_perumahan_.atap_seng					=  atap_seng
            insert_data_perumahan_.atap_daun					=  atap_daun
            insert_data_perumahan_.sumur_gali_pribadi			=  sumur_gali_pribadi
            insert_data_perumahan_.sumur_umum					=  sumur_umum
            insert_data_perumahan_.spam						    =  spam
            insert_data_perumahan_.air_sungai					=  air_sungai
            insert_data_perumahan_.jamban_rumah				    =  jamban_rumah
            insert_data_perumahan_.jamban_umum 				    =  jamban_umum
            insert_data_perumahan_.foto_jamban_rumah			=  foto_jamban_rumah
            insert_data_perumahan_.foto_jamban_umum			    =  foto_jamban_umum
            insert_data_perumahan_.rumah_listrik				=  rumah_listrik
            insert_data_perumahan_.penerangan_lain				=  penerangan_lain
            
            

            insert_data_perumahan_.save()
            messages.success(request, 'Data Perumahan berhasil disimpan.')
            return redirect('profile:admin_perumahan')
        messages.error(request, 'Data Perumahan gagal disimpan.')
        return render(request, 'profile/admin/perumahan/create.html', {'form': form,})


@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_distriks = dataDistrik.objects.get(id = id)

        context = {
            'title' : 'EDIT DISTRIK',
            'form' : form,
            'edit' : 'true',
            'data_distriks' : data_distriks,
        }
        template = 'profile/admin/distrik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_distrik = dataDistrik.objects.get(id = id)
        title = request.POST.get('title')

        if data_distrik is not None:
            data_distrik.title = title
            data_distrik.save()
            data_distrik.save()

            messages.success(request, 'Distrik berhasil disimpan')
            return redirect('profile:admin_distrik')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerumahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPerumahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerumahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPerumahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

