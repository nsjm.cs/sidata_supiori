from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    datapenJawab = datapenanggungjawab.objects.all()
    archives = datapenanggungjawab.objects.all()
    paginator = Paginator(datapenJawab, 15)
    try:
        datapenJawab = paginator.page(page)
    except PageNotAnInteger:
        datapenJawab = paginator.page(1)
    except EmptyPage:
        datapenJawab = paginator.page(paginator.num_pages)

    context = {
        'title' : ' Data Penanggung Jawab',
        'datapenJawab' : datapenJawab,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/penanggungjawab/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = datapenanggungjawab.objects.all()
        context = {
            'title' : 'ADD PEANGGUNG JAWAB',
            'form' : form,
        }

        template = 'profile/admin/penanggungjawab/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        nama = request.POST.get('nama')
        kontak = request.POST.get('kontak')
        
        if nama is not None:
            jawab = datapenanggungjawab()
            jawab.tahun = tahun
            jawab.nama = nama
            jawab.kontak = kontak
            jawab.save()
            
            messages.success(request, 'Penanggung Jawab berhasil disimpan.')
            return redirect('profile:admin_penanggungjawab')

        messages.error(request, 'Kata Mereka gagal disimpan.')
        return render(request, 'profile/admin/penanggungjawab/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        news = Kata.objects.get(id = id)

        context = {
            'title' : 'EDIT KATA MEREKA',
            'form' : form,
            'edit' : 'true',
            'news' : news,
        }
        template = 'profile/admin/kata/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        news = Kata.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{news.image}"
        foto_old = bool(news.image)
        judul = request.POST.get('judul')
        keterangan = request.POST.get('keterangan')

        if news is not None:
            news.judul = judul
            news.keterangan = keterangan
            news.save()
    
            if 'image' in request.FILES:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                foto = request.FILES['image']
                news.image = foto
                news.save()

            messages.success(request, 'Kata Mereka berhasil disimpan')
            return redirect('profile:admin_KM')

        messages.error(request, 'Kata Mereka gagal disimpan.')
        return render(request, 'profile/admin/kata/create.html', {'form': form,})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, id):
    template = 'profile/admin/kata/detail.html'
    try:
        news = Kata.objects.get(id=id)
    except Kata.DoesNotExist:
        news = None
    context = {
        'title' : news.judul if news != None else 'TIDAK DITEMUKAN',
        'news' : news,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataUMKM.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak Aktif'
        doc.save()
        message = 'success'
    except dataUMKM.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = Kata.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataUMKM.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataUMKM.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)