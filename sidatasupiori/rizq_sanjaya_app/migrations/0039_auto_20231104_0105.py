# Generated by Django 3.2 on 2023-11-03 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0038_datapendidikan_datapendidikanpaud_datapendidikansd_datapendidikansma_datapendidikansmp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datapemerintahan',
            name='dusun',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='datapemerintahan',
            name='luas_wilayah',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='datapemerintahan',
            name='rt',
            field=models.TextField(null=True),
        ),
    ]
