import os
from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Layanan
from django.utils import timezone
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db import connections
from ..helpers import read_icon, dictfetchall
from django.http import JsonResponse

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    services = Layanan.objects.all().order_by('id')
    context = {
        'title' : 'Layanan - Admin',
        'services' : services,
    }
    
    return render(request, 'profile/admin/services/index.html', context)

@login_required
@is_verified()
def create(request):
    context = {} 
    form = ''
    section = 'about'
    template = 'profile/admin/services/create.html'

     ### SPLIT DATA ICON PER LIMIT ===========================================================
    fonticon = read_icon(section)
    limit = 3
    total = len(fonticon)
    hasil = []

    for i in range(0,total,limit):
        hasil.append(fonticon[i:i+limit])

    if request.method == 'GET':
        form = LayananForm()
        context = {
            'title' : 'ADD Layanan',
            'form' : form,
            'dticon' : hasil,
            'section' : section,
        } 

        return render(request, template, context)
    
    if request.method == 'POST':
        form = LayananForm(data=request.POST)
        if form.is_valid():
            new_servis = form.save(commit=False)
            new_servis.save()
            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_services')

        messages.error(request, 'Data gagal disimpan.')
        context = {
            'title' : 'ADD Layanan',
            'form' : form,
            'dticon' : hasil,
            'section' : section,
        } 
        return render(request, template, context)

@login_required
@is_verified()
def edit(request, id):
    context = {} 
    form = ''
    section = 'about'
    template = 'profile/admin/services/create.html'

    ### SPLIT DATA ICON PER LIMIT ===========================================================
    fonticon = read_icon(section)
    limit = 3
    total = len(fonticon)
    hasil = []

    for i in range(0,total,limit):
        hasil.append(fonticon[i:i+limit])
    
    if request.method == 'GET':
        try:
            servis_get = Layanan.objects.get(id=id)
            form = LayananForm(instance=servis_get)
        except:
            servis_get = None
            form = LayananForm()
            
        context = {
            'title' : 'EDIT Layanan',
            'form' : form,
            'edit' : 'true',
            'id' : id,
            'servis' : servis_get,
            'dticon' : hasil,
            'section' : section,
        }
        return render(request, template, context)
    
    if request.method == 'POST':
        servis_get = Layanan.objects.get(id=id)
        form = LayananForm(data=request.POST, instance=servis_get)

        if form.is_valid():
            servis_update = form.save(commit=False)
            servis_update.judul = form.cleaned_data.get('judul')
            servis_update.keterangan = form.cleaned_data.get('keterangan')
            servis_update.icon = form.cleaned_data.get('icon')
            servis_update.slug = form.cleaned_data.get('slug')
            servis_update.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_services')

        context = {
            'title' : 'EDIT Layanan',
            'form' : form,
            'edit' : 'true',
            'id' : id,
            'servis' : servis_get,
            'dticon' : hasil,
            'section' : section,
        }
        messages.error(request, 'Data gagal disimpan.')
        return render(request, template, context)

@login_required
@is_verified()
def Delete(request, id):
    message = ''

    try:
        Layanan_del = Layanan.objects.get(id=id)
        Layanan_del.delete()
        message = 'success'
    except Layanan.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }
    return JsonResponse(context)