from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Product,Category
from django.utils import timezone
import os, math
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *


from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    prod = Product.objects.all().order_by('-created_at', )
    archives = Product.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    form = ProductForm(data=request.POST)
    form_diskon = ProductDiskonForm(data=request.POST)
    form_foto = ProductImagesForm(data=request.POST, files=request.FILES)
    paginator = Paginator(prod, 15)
    try:
        prod = paginator.page(page)
    except PageNotAnInteger:
        prod = paginator.page(1)
    except EmptyPage:
        prod = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Product - List',
        'products' : prod,
        'archives' : archives,
        'form' : form,
    }
    
    return render(request, 'profile/admin/product/index.html', context)

@login_required
@is_verified()
def create(request):
    template = form = form_foto = ''
    context = {} 
    category = Category.objects.filter(typexs = 'product').order_by('-id')

    if request.method == 'GET':
        form = ProductForm()
        form_foto = ProductImagesForm()
        form_diskon = ProductDiskonForm()
    
    if request.method == 'POST':
        try:
            form = ProductForm(data=request.POST)
            form_diskon = ProductDiskonForm(data=request.POST)
            form_foto = ProductImagesForm(data=request.POST, files=request.FILES)
            try:
                if form.is_valid() and form_foto.is_valid():
                    new_product = form.save(commit=False)
                    new_product.created_by_id = request.user.id
                    new_product.last_updated_by = request.user.id
                    new_product.save()
                    form.save_m2m()
                    
                    # mengambil instance product_diskon
                    product_diskon = Product.objects.get(id=new_product.id)
                    # product_diskon.diskon = request.POST.get('after_diskon', 0)
                    fixdiskon = request.POST.get('fixdiskon', 0)
                    fixdiskon = int(0 if fixdiskon == '' else fixdiskon)
                    print(fixdiskon)
                    product_diskon.diskon = fixdiskon
                    # product_diskon.diskon = int(0 if product_diskon.diskon == '' else product_diskon.diskon)
                    # print(product_diskon.diskon)
                    product_diskon.persen_diskon = request.POST.get('diskon', 0)
                    product_diskon.persen_diskon = float(0 if product_diskon.persen_diskon == '' else product_diskon.persen_diskon)
                    print(product_diskon.persen_diskon)
                    product_diskon.save()

                    product = Product.objects.get(id = new_product.id)
                    form_thumb_product = ProductImagesForm(data=request.POST, files=request.FILES, instance=product)
                    form_thumb_product.save()
                    
                    messages.success(request, 'Product berhasil disimpan.')
                    return redirect('profile:admin_product')
            except Exception as e:
                print(e)
                messages.error(request, 'Something wrong.')
            
        except Exception as e:
            print(e)
            messages.error(request, 'Product gagal disimpan.')

    context = {
        'title' : 'Product - Add',
        'form' : form,
        'form_foto' : form_foto,
        'kategori' : category,
        'form_diskon' : form_diskon, 
    }

    template = 'profile/admin/product/create.html'
    return render(request, template, context)

@login_required
@is_verified()
def edit(request, slug):
    template = form = ''
    context = {} 
    category = get_category('product')
    
    if request.method == 'GET':
        try:
            product = Product.objects.get(slug=slug)
            form = ProductForm(instance=product)
            form_foto = ProductImagesForm()
            tags = ''
            slug = product.slug
            for tag in  product.tags.all():
                tags += f"{tag}, "
        except:
            product = None
            form = ProductForm()
            form_foto = ProductImagesForm()
            tags = ''
            slug = None

        context = {
            'title' : 'Product - Edit',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'slug' : slug,
            'product' : product,
            'tagsnya' : tags,
            'kategori' : category,
        }

        template = 'profile/admin/product/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        product = Product.objects.get(slug=slug)
        path_file_lama = f"{settings.MEDIA_ROOT}/{product.images}"
        foto_old = bool(product.images)

        if bool(request.FILES):
            form = ProductForm(data=request.POST, files=request.FILES, instance=product)
            form_foto = ProductImagesForm(data=request.POST, files=request.FILES, instance=product)
            is_valid = form.is_valid() and form_foto.is_valid()
        else:
            form = ProductForm(data=request.POST, instance=product)
            is_valid = form.is_valid()

        if is_valid:
            new_product = form.save(commit=False)
            new_product.product_name = form.cleaned_data.get('product_name') 
            new_product.description = form.cleaned_data.get('description')
            new_product.category = form.cleaned_data.get('category')
            new_product.best_seller = form.cleaned_data.get('best_seller')
            new_product.last_updated_by = request.user.id
            new_product.save()
            form.save_m2m()

            # mengambil instance product_diskon
            radio = request.POST.get('enablePromo')
            print(radio)
            if radio == 'yes':
                harga = request.POST.get('untukdisk', 0)
                harga = float(0 if harga == '' else harga)
                # product.diskon = int(0 if product.diskon == '' else product.diskon)
                # print(product.diskon)
                persen_diskon = request.POST.get('diskon', 0)
                persen_diskon = float(0 if persen_diskon == '' else persen_diskon)
                print(persen_diskon)
                fixdiskon = request.POST.get('fixdiskon', 0)
                fixdiskon = int(0 if fixdiskon == '' else fixdiskon)
                print(fixdiskon)
                # product.persen_diskon = int(0 if product.persen_diskon == '' else product.persen_diskon)
                # print(harga)
                # diskon_fix_price = harga * (persen_diskon / 100)
                # print(diskon_fix_price)
                # diskon_fix = diskon_fix_price
                # print(diskon_fix)
                # fix = harga - diskon_fix
                # print(fix)
                product.diskon = fixdiskon
                product.persen_diskon = persen_diskon
                product.save()
            else:
                product.diskon = 0
                product.persen_diskon = 0
                product.save()

            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto.save()

            messages.success(request, 'Product berhasil disimpan.')
            return redirect('profile:admin_product')

        messages.error(request, 'Product gagal disimpan.')
        return render(request, 'profile/admin/product/create.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    try:
        prod = Product.objects.get(slug=slug)
    except Product.DoesNotExist:
        prod = None
    context = {
        'title' : prod.product_name if prod != None else 'TIDAK DITEMUKAN',
        'prod' : prod,
    }

    template = 'profile/admin/product/detail.html'
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Product.objects.get(slug=slug)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Product.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        doc = Product.objects.get(slug=slug)
        try:
            doc.images.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Product.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        doc = Product.objects.get(slug=slug)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Product.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }

    return HttpResponse(context)