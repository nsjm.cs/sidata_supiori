from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category,LayananDinas
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    layanan = LayananDinas.objects.filter(status='Publish').order_by('typexs','title')
    archives = LayananDinas.objects.filter(status='Draft').order_by('typexs','title')
    paginator = Paginator(layanan, 15)
    try:
        layanan = paginator.page(page)
    except PageNotAnInteger:
        layanan = paginator.page(1)
    except EmptyPage:
        layanan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Layanan Dinas - Admin',
        'data_layanan' : layanan,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/layanan_/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = LayananDinas.objects.all()
        context = {
            'title' : 'ADD Layanan Dinas',
            'form' : form,
        }

        template = 'profile/admin/layanan_/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        typesx = request.POST.get('typesx')
        description = request.POST.get('description')
        foto = request.FILES['foto']
        if title is not None:
            layananDin = LayananDinas()
            layananDin.title = title
            layananDin.typesx = typesx
            layananDin.description = description
            layananDin.foto = foto
            layananDin.save()
            
            messages.success(request, 'Layanan dinas berhasil disimpan.')
            return redirect('profile:admin_layanan')

        messages.error(request, 'Layanan dinas gagal disimpan.')
        return render(request, 'profile/admin/layanan_/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        layanandinas = LayananDinas.objects.get(id = id)

        context = {
            'title' : 'EDIT Layanan Dinas',
            'form' : form,
            'edit' : 'true',
            'layanandinas' : layanandinas,
        }
        template = 'profile/admin/layanan_/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        layanandinas = LayananDinas.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{layanandinas.foto}"
        foto_old = bool(layanandinas.foto)
        title = request.POST.get('title')
        typexs = request.POST.get('typexs')
        description = request.POST.get('description')

        if layanandinas is not None:
            layanandinas.title = title
            layanandinas.typexs = typexs
            layanandinas.description = description
            layanandinas.save()
    
            if 'foto' in request.FILES:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                foto = request.FILES['foto']
                layanandinas.foto = foto
                layanandinas.save()

            messages.success(request, 'Layanan Dinas berhasil disimpan')
            return redirect('profile:admin_layanan')

        messages.error(request, 'Layanan Dinas gagal disimpan.')
        return render(request, 'profile/admin/layanan_/create.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = LayananDinas.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = LayananDinas.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        status = 'Publish'
        doc = LayananDinas.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except LayananDinas.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)