from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    
    data_pu = dataPU.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPU.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_pu, 15)
    try:
        data_pu = paginator.page(page)
    except PageNotAnInteger:
        data_pu = paginator.page(1)
    except EmptyPage:
        data_pu = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data PU',
        'data_pu_list' : data_pu,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/pu/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataPU.objects.all()
        data_pu_ = dataPU.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data Pu',
            'form' : form,
            'data_pu_list':data_pu_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/pu/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        
        jln_beraspal	 			=  request.POST.get('jln_beraspal')
        jln_hampar	 			    =  request.POST.get('jln_hampar')
        jln_rusak	 			    =  request.POST.get('jln_rusak')
        jln_tani	 			    =  request.POST.get('jln_tani')
        jln_tani_blm_aspal	 	    =  request.POST.get('jln_tani_blm_aspal')
        foto_jln_beraspal     		=  request.FILES['foto_jln_beraspal']
        foto_jln_hampar 			=  request.FILES['foto_jln_hampar']
        foto_jln_rusak 				=  request.FILES['foto_jln_rusak']
        foto_jln_tani 				=  request.FILES['foto_jln_tani']
        foto_jln_tani_blm_aspal 	=  request.FILES['foto_jln_tani_blm_aspal']
        jmt_beton	 			    =  request.POST.get('jmt_beton')
        kondisi_jmt	 			    =  request.POST.get('kondisi_jmt')
        jmt_kayu	 			    =  request.POST.get('jmt_kayu')
        kondisi_jmt_kayu	 	    =  request.POST.get('kondisi_jmt_kayu')
        foto_jmt_beton     		    =  request.FILES['foto_jmt_beton']
        foto_kondisi_jmt 			=  request.FILES['foto_kondisi_jmt'] 
        foto_jmt_kayu 				=  request.FILES['foto_jmt_kayu']
        foto_kondisi_jmt_kayu 		=  request.FILES['foto_kondisi_jmt_kayu']
        
       
        if kampung is not None:
            data_pu_ = dataPU()
            data_pu_.tahun = tahun
            data_pu_.penanggungjawab_id = penanggungjawab_id
            data_pu_.kampung_id = kampung

            data_pu_.jln_beraspal	 			= jln_beraspal	 			
            data_pu_.jln_hampar	 			    = jln_hampar	 			    
            data_pu_.jln_rusak	 			    = jln_rusak	 			    
            data_pu_.jln_tani	 			    = jln_tani	 			    
            data_pu_.jln_tani_blm_aspal	 	    = jln_tani_blm_aspal	 	    
            data_pu_.foto_jln_beraspal     		= foto_jln_beraspal     		
            data_pu_.foto_jln_hampar 			= foto_jln_hampar 			
            data_pu_.foto_jln_rusak 			= foto_jln_rusak 				
            data_pu_.foto_jln_tani 				= foto_jln_tani 				
            data_pu_.foto_jln_tani_blm_aspal 	= foto_jln_tani_blm_aspal 	
            data_pu_.jmt_beton	 			    = jmt_beton	 			    
            data_pu_.kondisi_jmt	 			= kondisi_jmt	 			    
            data_pu_.jmt_kayu	 			    = jmt_kayu	 			    
            data_pu_.kondisi_jmt_kayu	 	    = kondisi_jmt_kayu	 	    
            data_pu_.foto_jmt_beton     		= foto_jmt_beton     		    
            data_pu_.foto_kondisi_jmt 			= foto_kondisi_jmt 			  
            data_pu_.foto_jmt_kayu 				= foto_jmt_kayu 				
            data_pu_.foto_kondisi_jmt_kayu 		= foto_kondisi_jmt_kayu 		
            
            data_pu_.save()
            messages.success(request, 'Data PU berhasil disimpan.')
            return redirect('profile:admin_pu')
        messages.error(request, 'Data Kominfo gagal disimpan.')
        return render(request, 'profile/admin/pu/create.html', {'form': form,})

@login_required
@is_verified()
def detail(request, id):
    template = ''
    context = {} 
    
    if request.method == 'GET':
        data_detail_pu = dataPU.objects.get(id = id)
        context = {
            'title' : 'Data Pekerjaan Umum',
            'data_detail_pu' : data_detail_pu,
        }
        template = 'profile/admin/pu/detail.html'
        return render(request, template, context)
    
@login_required
@is_verified()
def detail_(request, id):
    template = ''
    context = {} 
    
    if request.method == 'GET':
        data_detail_pu = dataPU.objects.get(id = id)
        context = {
            'title' : 'Data Pekerjaan Umum',
            'data_detail_pu' : data_detail_pu,
        }
        template = 'profile/admin/pu/detail_.html'
        return render(request, template, context)

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_distriks = dataDistrik.objects.get(id = id)

        context = {
            'title' : 'EDIT DISTRIK',
            'form' : form,
            'edit' : 'true',
            'data_distriks' : data_distriks,
        }
        template = 'profile/admin/distrik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_distrik = dataDistrik.objects.get(id = id)
        title = request.POST.get('title')

        if data_distrik is not None:
            data_distrik.title = title
            data_distrik.save()
            data_distrik.save()

            messages.success(request, 'Distrik berhasil disimpan')
            return redirect('profile:admin_distrik')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPU.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPU.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPU.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPU.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)