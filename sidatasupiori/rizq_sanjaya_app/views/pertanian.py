from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category




@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
	
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pertanian = dataPertanian.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPertanian.objects.filter(status='Tidak aktif')
    paginator = Paginator(dt_pertanian, 15)
    try:
        dt_pertanian = paginator.page(page)
    except PageNotAnInteger:
        dt_pertanian = paginator.page(1)
    except EmptyPage:
        dt_pertanian = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Pertanian',
        'dt_pertanian' : dt_pertanian,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/pertanian/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''
	
    if request.method == 'GET':
        form = dataPertanian.objects.all()
        list_pertanian = dataPertanian.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        id =  dataPertanian.objects.order_by('-id')[:1]
        context = {
            'title' : 'Data Pertanian',
            'form' : form,
            'list_pertanian':list_pertanian,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
            'lates_id':id
        }

        template = 'profile/admin/pertanian/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        kodepertanian = request.POST.get('kodepertanian')

        jml_ayam = request.POST.get('jml_ayam')
        jml_babi = request.POST.get('jml_babi')
        jml_sapi = request.POST.get('jml_sapi')
        jml_bebek = request.POST.get('jml_bebek')
        jml_kelinci = request.POST.get('jml_kelinci')
        jml_kambing = request.POST.get('jml_kambing')
        luas_lahan = request.POST.get('luas_lahan')
        
        
        if kampung is not None:
            insert_pertanian_ = dataPertanian()
            insert_pertanian_.tahun = tahun
            insert_pertanian_.penanggungjawab_id = penanggungjawab_id
            insert_pertanian_.kampung_id = kampung

            insert_pertanian_.kodepertanian = kodepertanian
            insert_pertanian_.jml_ayam = jml_ayam
            insert_pertanian_.jml_babi = jml_babi
            insert_pertanian_.jml_bebek = jml_bebek
            insert_pertanian_.jml_kambing = jml_kambing
            insert_pertanian_.jml_kelinci = jml_kelinci
            insert_pertanian_.jml_sapi = jml_sapi
            insert_pertanian_.luas_lahan = luas_lahan


            insert_pertanian_.save()
            messages.success(request, 'Data Pertanian berhasil disimpan.')
            return redirect('profile:admin_pertanian_create_data',kodepertanian)
        messages.error(request, 'Data fasilitas gagal disimpan.')
        return render(request, 'profile/admin/pertanian/create.html', {'form': form,})

@login_required
@is_verified()
def detailpertanian(request, id):
    template = ''
    context = {} 
    form = ''

    
    if request.method == 'GET':
        get_data_Pertanian = dataPertanian.objects.get(id = id)
        dt_kelompok_tani = dataKelompoktani.objects.filter(kodepertanian = id,status='Aktif')
        data_arsip_kelompok_tani = dataKelompoktani.objects.filter(kodepertanian = id,status='Tidak aktif')
        dt_komoditias_tani = dataKomoditipertanian.objects.filter(kodepertanian = id,status='Aktif')
        data_arsip_dt_komoditias_tani = dataKomoditipertanian.objects.filter(kodepertanian = id,status='Tidak aktif')
        
        context = {
            'title' : 'DETAIL',
            'form' : form,
            # 'edit' : 'true',
            'get_data_Pertanian' : get_data_Pertanian,
            'dt_kelompok_tani' : dt_kelompok_tani,
            'data_arsip_kelompok_tani': data_arsip_kelompok_tani,
            'dt_komoditias_tani': dt_komoditias_tani,
            'data_arsip_dt_komoditias_tani': data_arsip_dt_komoditias_tani,
            
            
            
        }
        template = 'profile/admin/pertanian/create_data.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        kodepertanian = request.POST.get('kodepertanian')
        pertanian = request.POST.get('pertanian')
        namakelompok = request.POST.get('namakelompok')
        ketua = request.POST.get('ketua')
        jml_anggota = request.POST.get('jml_anggota')
        bantuan = request.POST.get('bantuan')
        foto = request.FILES['foto']

        if kodepertanian is not None:
            insert_data_kelompoktani_ = dataKelompoktani()
            insert_data_kelompoktani_.kodepertanian = kodepertanian
            insert_data_kelompoktani_.pertanian_id = pertanian
            insert_data_kelompoktani_.namakelompok = namakelompok
            insert_data_kelompoktani_.ketua = ketua
            insert_data_kelompoktani_.jml_anggota = jml_anggota
            insert_data_kelompoktani_.bantuan = bantuan
            insert_data_kelompoktani_.foto = foto
            
            insert_data_kelompoktani_.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pertanian_create_data',kodepertanian)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pertanian/create_data.html',kodepertanian, {'form': form,})
#save data atlet
@login_required
@is_verified()
def dataKomoditas_create(request, id):

    if request.method == 'POST':
       
        kodepertanian = request.POST.get('kodepertanian')
        pertanian = request.POST.get('pertanian')
        nama = request.POST.get('nama')
        keterangan = request.POST.get('keterangan')
        
        if kodepertanian is not None:
            insert_data_komoditas = dataKomoditipertanian()
            insert_data_komoditas.kodepertanian = kodepertanian
            insert_data_komoditas.pertanian_id = pertanian
            insert_data_komoditas.nama = nama
            insert_data_komoditas.keterangan = keterangan
            insert_data_komoditas.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pertanian_create_data',kodepertanian)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pertanian/create_data.html',kodepertanian)



# arsip datadetail
@login_required
@is_verified()
def softDelete_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKelompoktani.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataKelompoktani.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_detail(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKelompoktani.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataKelompoktani.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context) 

# atlit
# arsip datadetail
@login_required
@is_verified()
def softDelete_atlit(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKomoditipertanian.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataKomoditipertanian.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore_atlit(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKomoditipertanian.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataKomoditipertanian.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
   

@login_required
@is_verified()
def edit(request, id):
    edit_datapertanian = get_object_or_404(dataPertanian, id=id)
    form = ''

    if request.method == 'POST':
        
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        kodepertanian = request.POST.get('kodepertanian')

        jml_ayam = request.POST.get('jml_ayam')
        jml_babi = request.POST.get('jml_babi')
        jml_sapi = request.POST.get('jml_sapi')
        jml_bebek = request.POST.get('jml_bebek')
        jml_kelinci = request.POST.get('jml_kelinci')
        jml_kambing = request.POST.get('jml_kambing')
        luas_lahan = request.POST.get('luas_lahan')

        if edit_datapertanian is not None:
            edit_datapertanian.tahun = tahun
            edit_datapertanian.penanggungjawab_id = penanggungjawab_id
            edit_datapertanian.kampung_id = kampung

            edit_datapertanian.kodepertanian = kodepertanian
            edit_datapertanian.jml_ayam = jml_ayam
            edit_datapertanian.jml_babi = jml_babi
            edit_datapertanian.jml_bebek = jml_bebek
            edit_datapertanian.jml_kambing = jml_kambing
            edit_datapertanian.jml_kelinci = jml_kelinci
            edit_datapertanian.jml_sapi = jml_sapi
            edit_datapertanian.luas_lahan = luas_lahan
            edit_datapertanian.save()
            

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pertanian')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

# EDIT KELOMPOK PERTANIAN
@login_required
@is_verified()
def edit_kelompok_tani(request, id):
    edit_datapertanian_klmp_tani = get_object_or_404(dataKelompoktani, id=id)
    form = ''

    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataKelompoktani.foto}"
        foto_old = bool(dataKelompoktani.foto)
        image = request.FILES.get('foto')
        
        pertanian = request.POST.get('pertanian')
        namakelompok = request.POST.get('namakelompok')
        ketua = request.POST.get('ketua')
        jml_anggota = request.POST.get('jml_anggota')
        bantuan = request.POST.get('bantuan')

        

        if edit_datapertanian_klmp_tani is not None:
            
            edit_datapertanian_klmp_tani.namakelompok = namakelompok
            edit_datapertanian_klmp_tani.ketua = ketua
            edit_datapertanian_klmp_tani.jml_anggota = jml_anggota
            edit_datapertanian_klmp_tani.bantuan = bantuan

            edit_datapertanian_klmp_tani.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataKelompoktani.objects.get(id = id)
                form_foto.foto = image
                form_foto.save()

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pertanian_create_data',pertanian)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

# EDIT KELOMPOK KOMODITAS
@login_required
@is_verified()
def edit_komoditas_pertanian(request, id):
    edit_komoditas_pertanian = get_object_or_404(dataKomoditipertanian, id=id)
    form = ''

    if request.method == 'POST':
        
        pertanian = request.POST.get('pertanian')
        nama = request.POST.get('nama')
        keterangan = request.POST.get('keterangan')

        

        if edit_komoditas_pertanian is not None:
            
            edit_komoditas_pertanian.nama = nama
            edit_komoditas_pertanian.keterangan = keterangan
            edit_komoditas_pertanian.save()
           
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pertanian_create_data',pertanian)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPertanian.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPertanian.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPertanian.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPertanian.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)