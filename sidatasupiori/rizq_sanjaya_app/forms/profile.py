from django import forms
from ..models import *
from PIL import Image
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from captcha.fields import CaptchaField

###################FORM DATA DISTRIK###################
class Distrik(forms.ModelForm):
	class Meta:
		model = dataDistrik
		fields = ('id', 'title')


###################FORM DATA KAMPUNG###################
class Kampung(forms.ModelForm):
	class Meta:
		model = dataKampung
		fields = ('id', 'titles','distrik')



###################FORM DATA KAMPUNG###################
class Penanggungjawab(forms.ModelForm):
	class Meta:
		model = datapenanggungjawab
		fields = ('id', 'tahun','nama','kontak')


###################FORM DATA UMKM & KOPERASI###################
class Umkm(forms.ModelForm):
	class Meta:
		model = dataUMKM
		fields = ('id','tahun','penanggungjawab', 'kampung','jeniinput','title_usaha','title_owner','jenis_usaha','modal_usaha','bantuan','izin_usaha','akta_usaha','jml_karyawan')

###################FORM DATA KELEMBAGAAN###################
class Kelembagaan(forms.ModelForm):
	class Meta:
		model = dataKelembagaan
		fields = ('id','tahun','penanggungjawab', 'kampung','jenisinput','title_lembaga','ketua_lembaga','jml_anggota')


###################FORM DATA PU###################
class PU(forms.ModelForm):
	class Meta:
		model = dataPU
		fields = ('id','tahun','penanggungjawab', 'kampung','jln_beraspal',	'jln_hampar', 'jln_rusak','jln_tani','jln_tani_blm_aspal','jmt_beton','kondisi_jmt'	,'jmt_kayu' ,'kondisi_jmt_kayu')

###################FORM DATA PERHUBUNGAN###################
class Perhubungan(forms.ModelForm):
	class Meta:
		model = dataPerhubungan
		fields = ('id','tahun','penanggungjawab', 'kampung'  ,'jml_halte','jml_terminal'	,'jml_dermaga' 	,'jml_tambatan_perahu','kondisi_dermaga','kondisi_tambatan_perahu' )

###################FORM DATA SOSIAL###################
class Sosial(forms.ModelForm):
	class Meta:
		model = dataSosial
		fields = ('id','tahun','penanggungjawab', 'kampung' ,'jml_kartu_kip','jml_bantuan_s_miskin','jml_kartu_kis','jml_user_bpjs','jml_user_bpjs_mandiri','jml_bpjs_ketenagakerjaan','program_keluarga_harapan',  'bantuan_pangan_non_tunai','komunitas_adat_terpencil' )

###################FORM DATA SOSIAL###################
class Perikanan(forms.ModelForm):
	class Meta:
		model = dataPerikanan
		fields = ('id','tahun','penanggungjawab', 'kampung' ,'kodeperikanan', 'jml_rt_nelayan','jml_kartu_nelayan','jml_rtp')

###################FORM DATA PERIKANAN###################
class Detail_Perikanan(forms.ModelForm):
	class Meta:
		model = dataDetailPerikanan
		fields = ['jenisinput', 'kodeperikanan', 'namakelompok', 'ketua', 'bantuan', 'sk_pelatihan', 'perikanan']


###################FORM DATA FASILITAS OLAHRAGA###################
class Fasilitasolahraga(forms.ModelForm):
	class Meta:
		model = dataFasilitasolahraga
		fields = ('id','tahun','penanggungjawab', 'kampung' ,'kodefasilitas', 'jml_lapangan_voly','jml_lapangan_bola','jml_lapangan_basket','jml_lapangan_takraw','jml_lapangan_bulutangkis')

###################FORM DATA FASILITAS OLAHRAGA###################
class Detail_fasilitasolahraga(forms.ModelForm):
	class Meta:
		model = dataDetailFasilitasolahraga
		fields = ['kodefasilitas', 'namakelompok', 'ketua', 'jml_anggota', 'pelatihan', 'fasilitas']

###################FORM DATA FASILITAS OLAHRAGA###################
class DataAtlet(forms.ModelForm):
	class Meta:
		model = dataAtlet
		fields = ['kodefasilitas', 'nama', 'event', 'prestasi', 'fasilitas']

##################APLIKASI SIDATA SUPIORI##############


###################FORM DATA PERTANIAN###################
class Pertanian(forms.ModelForm):
	class Meta:
		model = dataPertanian
		fields = ('id','tahun','penanggungjawab', 'kampung' ,'kodepertanian','jml_ayam','jml_babi','jml_sapi','jml_kambing','jml_bebek','jml_kelinci','luas_lahan')

###################FORM DATA PERTANIAN###################
class KelompokTani(forms.ModelForm):
	class Meta:
		model = dataKelompoktani
		fields = ['kodepertanian','namakelompok','ketua','jml_anggota','bantuan','pertanian','foto']

###################FORM DATA PERTANIAN###################
class KommoditasPertanian(forms.ModelForm):
	class Meta:
		model = dataKomoditipertanian
		fields = ['kodepertanian','nama','keterangan','pertanian']

###################FORM DATA KEPENDUDUKAN###################
class Kependudukan(forms.ModelForm):
	class Meta:
		model = datakependudukan
		fields = ['id','tahun','penanggungjawab', 'kampung','jml_penduduk_pria','jml_penduduk_wanita','jml_kriten','jml_katholik','jml_islam','jml_hindu','jml_budha','jml_OAP','jml_nonOAP','jml_KK','jml_KTP','jml_Akta']

###################FORM DATA PEMERINTAHAN###################
class Pemerintahan(forms.ModelForm):
	class Meta:
		model = dataPemerintahan
		fields = ['id','tahun','penanggungjawab', 'kampung','luas_wilayah','rt','dusun','batas_utara','batas_selatan','batas_barat','batas_timur','jml_aparatur_kampung','pendidikan_aparatur_sd','pendidikan_aparatur_smp','pendidikan_aparatur_sma','pendidikan_aparatur_d3','pendidikan_aparatur_s1','jml_basmuskam','pendidikan_bamuskam_sd','pendidikan_bamuskam_smp','pendidikan_bamuskam_sma','pendidikan_bamuskam_d3','pendidikan_bamuskam_s1','jml_ketua_rt','jml_kepala_dusun','jml_Babinkamtibmas','jml_Babinsa','kantor','status_kantor']


###################FORM DATA KESEHATAN###################
class Kesehatan(forms.ModelForm):
	class Meta:
		model = dataKesehatan
		fields = ['id','tahun','penanggungjawab', 'kampung', 'status', 'namapuskesmas','putu','status_puskesmas','kondisi_puskesmas',
					'dok_spesialis','dok_umum','dok_gigi','perawat','bidan','tenaga_promkes','tenaga_sanitasi','apoteker','tenaga_lab',
					'jml_rumah_dinas','jml_balita','jml_ibu_hamil','jml_kematian_ibu_hamil','jml_kematian_ibu_melahirkan','jml_kematian_bayi'  ]

###################FORM DATA POSYANDU###################
class Posyandu(forms.ModelForm):
	class Meta:
		model = dataPosyandu
		fields = ['id','tahun','penanggungjawab', 'kampung', 'status', 'nama', 'kader']

##################APLIKASI SIDATA SUPIORI##############
class Perumahan(forms.ModelForm):
	class Meta:
		model = dataPerumahan
		fields = ['id','tahun','penanggungjawab', 'kampung', 'status','jml_rumah_layak_huni','jml_rumah_tdk_layak_huni','jml_rumah_semi_permanen','jml_rumah_permanen','foto_rumah_semi_permanen','foto_rumah_permanen','jml_total_rumah','lantai_tanah','lantai_kayu','lantai_semen','lantai_keramik',
'dinding_kayu','dinding_semen','dinding_anyaman','atap_seng','atap_daun','sumur_gali_pribadi','sumur_umum','spam','air_sungai','jamban_rumah','jamban_umum','foto_jamban_rumah','foto_jamban_umum','rumah_listrik','penerangan_lain',
]


###################FORM DATA PENDIDIKAN###################
class Pendidikan(forms.ModelForm):
	class Meta:
		model = dataPendidikan
		fields = ['id','tahun','penanggungjawab', 'kampung', 'status']

###################FORM DATA PENDIDIKAN PAUD###################
class PendidikanPaud(forms.ModelForm):
	class Meta:
		model = dataPendidikanPaud
		fields = ['kodependidikan','pendidikan','status','namasekolah','jml_ruang_belajar','jml_murid','jml_guru','jml_guru_oap','jml_guru_nonoap','jml_meja_kursi','foto_paud'
]
		
###################FORM DATA PENDIDIKAN SD###################
class PendidikanSD(forms.ModelForm):
	class Meta:
		model = dataPendidikanSD
		fields = ['kodependidikan','pendidikan','status','namasekolah','jml_ruang_belajar','jml_perpustakaan',
					'jml_kantor',		
					'jml_wc',			
					'jml_papan_tulis',	
					'jml_pc',			
					'jml_guru',		
					'jml_guru_oap',	
					'jml_guru_non_oap',
					'jml_murid',			 
					'jml_murid_kelas_I',		
					'jml_murid_kelas_II',		
					'jml_murid_kelas_III',		
					'jml_murid_kelas_IV',		
					'jml_murid_kelas_V',		
					'jml_murid_kelas_VI',		
					'jml_meja_kursi',			
					'jml_meja_kursi_I',		
					'jml_meja_kursi_II',		
					'jml_meja_kursi_III',		
					'jml_meja_kursi_IV',		
					'jml_meja_kursi_V',		
					'jml_meja_kursi_VI',		
					'jml_buku_paket',			
					'jml_buku_paket_I',		
					'jml_buku_paket_II',		
					'jml_buku_paket_III',		
					'jml_buku_paket_IV',		
					'jml_buku_paket_V',		
					'jml_buku_paket_VI',		
					'foto_sd'
					]
		

###################FORM DATA PENDIDIKAN SMP###################
class PendidikaSMP(forms.ModelForm):
	class Meta:
		model = dataPendidikanSMP
		fields = ['kodependidikan','pendidikan','status','namasekolah','jml_ruang_belajar',
					'jml_perpustakaan',
					'jml_lab',
					'jml_kantor',
					'jml_wc',
					'jml_papan_tulis',
					'jml_pc',
					'kebutuhan_guru',
					'kekurangan_guru',
					'jml_guru',
					'jml_guru_oap',
					'jml_guru_non_oap',
					'jml_murid',
					'jml_murid_kelas_VII',
					'jml_murid_kelas_VIII',
					'jml_murid_kelas_IX',
					'jml_meja_kursi',
					'jml_meja_kursi_VII',
					'jml_meja_kursi_VIII',
					'jml_meja_kursi_IX',
					'jml_buku_paket',
					'jml_buku_paket_VII',
					'jml_buku_paket_VIII',
					'jml_buku_paket_IX',
					'buku_paket_kurang_matapelajaran',
					'foto_smp'
					]

###################FORM DATA PENDIDIKAN SMA###################
class PendidikaSMA(forms.ModelForm):
	class Meta:
		model = dataPendidikanSMA
		fields = ['kodependidikan','pendidikan','status','namasekolah','jml_ruang_belajar',
					'jml_perpustakaan',
					'jml_lab',
					'jml_kantor',
					'jml_wc',
					'jml_papan_tulis',
					'jml_pc',
					'kebutuhan_guru',
					'kekurangan_guru',
					'jml_guru',
					'jml_guru_oap',
					'jml_guru_non_oap',
					'jml_murid',
					'jml_murid_kelas_X',
					'jml_murid_kelas_XI',
					'jml_murid_kelas_XII',
					'jml_meja_kursi',
					'jml_buku_paket',
					'jml_buku_paket_X',
					'jml_buku_paket_XI',
					'jml_buku_paket_XII',
					'buku_paket_kurang_matapelajaran',
					'foto_sma'
				]
	

				
		 			  
			 	  
				 	  
			  
        	 			  
        	 			    
        	 	  
        






















###################FORM PROFIL SKPD###################
class Profilskpd(forms.ModelForm):
	class Meta:
		model = ProfilSKPD
		fields = ('id', 'title','typexs','description')
###################FORM INFO TERPADU###################
class InfoTerpaduForm(forms.ModelForm):
	class Meta:
		model = InfoTerpadu
		fields = ('id', 'title','start_event_date','end_event_date','typexs','description')

###################FORM LAYANAN DINAS###################
class LayananDinasForm(forms.ModelForm):
	class Meta:
		model = LayananDinas
		fields = ('id', 'title','typexs','description',)

###################FORM AKSESBILITAS LINK###############
class AksesbilitasLink(forms.ModelForm):
	class Meta:
		model = Aksesbilitas
		fields = ('id', 'title','link',)

###################FORM KATEGORI#########################
class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ('id', 'nama','typexs',)

###################FORM BERITA#########################
class NewsForm(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class RnWForm(forms.ModelForm):
	class Meta:
		model = RnW
		fields = ('jenis', 'deskripsi',)

###################FORM THUMBNAIL BERITA#########################
class NewsThumbnailForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = News
		fields = ('thumbnail', 'x', 'y', 'width', 'height',)

	def save(self):
		news = super(NewsThumbnailForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(news.thumbnail)
		image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")

		## SAVE THUMBNAIL
		# image = Image.open(news.thumbnail)
		# cropped_image = image.crop((x, y, w + x, h + y))
		# cropped_image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")
		lokasi = f"{news.thumbnail.name}"
		hassss = lokasi.replace('artikel/', 'artikel/thumb/')

		tumbnil = image.crop((x, y, w + x, h + y))
		tumbnis = tumbnil.resize((w, h), Image.ANTIALIAS)
		tumbnis.save(f"{settings.MEDIA_ROOT}"+hassss)

		return news
class NewsFormTanpaFile(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class GalleryForm(forms.ModelForm):
	class Meta:
		model = Gallery
		fields = ('title', 'keterangan', 'video', 'tags',)

class GalleryFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Gallery
		fields = ('foto',)

	def save(self):
		gallery = super(GalleryFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(gallery.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{gallery.foto.name}")

		return gallery

class AvatarForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Account
		fields = ('avatar', 'x', 'y', 'width', 'height',)

	def save(self):
		account = super(AvatarForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(account.avatar)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{account.avatar.name}")

		return account

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('first_name', 'last_name', 'phone', 'date_of_birth')

class RoleForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('role',)

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('email', 'username', 'first_name', 'last_name', 'role', 'phone', 'date_of_birth',)

class RunningTextForm(forms.ModelForm):
	class Meta:
		model = RunningText
		fields = ('text',)

class LinkForm(forms.ModelForm):
	class Meta:
		model = Link
		fields = ('nama', 'link')

class AppSettingForm(forms.ModelForm):
	class Meta:
		model = AppSetting
		fields = ('nama', 'keterangan')

class CarouselForm(forms.ModelForm):
	class Meta:
		model = Carousel
		fields = ('judul', 'subjudul', 'keterangan', 'link')

class CarouselFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Carousel
		fields = ('foto',)

	def save(self):
		carousel = super(CarouselFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(carousel.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{carousel.foto.name}")

		return carousel

class RunningLinkForm(forms.ModelForm):
	class Meta:
		model = RunningLink
		fields = ('nama', 'link')

class RunningLinkFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = RunningLink
		fields = ('logo',)

	def save(self):
		running_link = super(RunningLinkFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(running_link.logo)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{running_link.logo.name}")

		return running_link

class MyCaptcha(forms.Form):
   captcha=CaptchaField()

class AboutMeForm(forms.ModelForm):
	class Meta:
		model = AboutMe
		fields = ('judul', 'deskripsi', 'youtube', 'jenis', 'section',) 

class LayananForm(forms.ModelForm):
	class Meta:
		model = Layanan
		fields = ('judul', 'keterangan', 'icon', 'slug',)

class ProductForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ('product_name', 'category', 'best_seller', 'description', 'tags', 'harga')

class ProductDiskonForm(forms.ModelForm):
    diskon = forms.CharField(required=False)
    persen_diskon = forms.CharField(required=False)

    class Meta:
        model = Product
        fields = ('diskon', 'persen_diskon')


class ProductImagesForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Product
		fields = ('images', 'x', 'y', 'width', 'height',)

	def save(self):
		product = super(ProductImagesForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(product.images)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{product.images.name}")

		return product