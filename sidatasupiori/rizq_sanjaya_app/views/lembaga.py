from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    lembaga_ = dataKelembagaan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataKelembagaan.objects.filter(status='Tidak aktif')
    paginator = Paginator(lembaga_, 15)
    try:
        lembaga_ = paginator.page(page)
    except PageNotAnInteger:
        lembaga_ = paginator.page(1)
    except EmptyPage:
        lembaga_ = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data lembaga_',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'lembaga_list' : lembaga_,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/lembaga/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataKelembagaan.objects.all()
        kelembagaan_ = dataKelembagaan.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data kelembagaan_',
            'form' : form,
            'kelembagaan_list':kelembagaan_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/lembaga/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        jenisinput = request.POST.get('jenisinput')
        title_lembaga = request.POST.get('title_lembaga')
        ketua_lembaga = request.POST.get('ketua_lembaga')
        jml_anggota = request.POST.get('jml_anggota')
        foto = request.FILES['foto']
       
        if kampung is not None:
            data_kelembagaan_ = dataKelembagaan()
            data_kelembagaan_.tahun = tahun
            data_kelembagaan_.penanggungjawab_id = penanggungjawab_id
            data_kelembagaan_.jenisinput = jenisinput
            data_kelembagaan_.jml_anggota = jml_anggota
            data_kelembagaan_.ketua_lembaga = ketua_lembaga
            data_kelembagaan_.title_lembaga = title_lembaga
            data_kelembagaan_.foto = foto
            data_kelembagaan_.kampung_id = kampung
            data_kelembagaan_.save()
            messages.success(request, 'Data umkm berhasil disimpan.')
            return redirect('profile:admin_kelembagaan')
        messages.error(request, 'Data umkm gagal disimpan.')
        return render(request, 'profile/admin/lembaga/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):

    edit_datakelembagaan = get_object_or_404(dataKelembagaan, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataKelembagaan.foto}"
        foto_old = bool(dataKelembagaan.foto)
        image = request.FILES.get('foto')


        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jenisinput = request.POST.get('jenisinput')
        title_lembaga = request.POST.get('title_lembaga')
        ketua_lembaga = request.POST.get('ketua_lembaga')
        jml_anggota = request.POST.get('jml_anggota')

        if edit_datakelembagaan is not None:
            edit_datakelembagaan.tahun                 = tahun
            edit_datakelembagaan.penanggungjawab_id    = penanggungjawab_id
            edit_datakelembagaan.kampung_id            = kampung

            edit_datakelembagaan.jenisinput = jenisinput
            edit_datakelembagaan.jml_anggota = jml_anggota
            edit_datakelembagaan.ketua_lembaga = ketua_lembaga
            edit_datakelembagaan.title_lembaga = title_lembaga

            edit_datakelembagaan.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataKelembagaan.objects.get(id = id)
                form_foto.foto = image
                form_foto.save()

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_kelembagaan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKelembagaan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataKelembagaan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataKelembagaan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataKelembagaan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)