from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    dt_pendidikan = dataPendidikan.objects.prefetch_related('kampung').filter(status='Aktif')
    dt_pendidikan_paud = dataPendidikanPaud.objects.prefetch_related('pendidikan').filter(status='Aktif')
    dt_pendidikan_sd = dataPendidikanSD.objects.prefetch_related('pendidikan').filter(status='Aktif')
    archives = dataPendidikan.objects.filter(status='Tidak aktif')
    id = dataPendidikan.objects.order_by('-id')[:1]
    paginator = Paginator(dt_pendidikan, 15)
    try:
        dt_pendidikan = paginator.page(page)
    except PageNotAnInteger:
        dt_pendidikan = paginator.page(1)
    except EmptyPage:
        dt_pendidikan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Data Pemerinatahan',
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
        'dt_pendidikan_list' : dt_pendidikan,
        'archives' : archives,
        'lates_id' : id,
        'dt_pendidikan_paud':dt_pendidikan_paud,
        'dt_pendidikan_sd' : dt_pendidikan_sd,

    }
    
    return render(request, 'profile/admin/pendidikan/index.html', context)

@login_required
@is_verified()
def create(request):
    form = ''
    if  request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        kodependidikan = request.POST.get('kodependidikan')
       
        if kampung is not None:
            insert_data_pendidikan_                       = dataPendidikan()
            insert_data_pendidikan_.tahun                 = tahun
            insert_data_pendidikan_.penanggungjawab_id    = penanggungjawab_id
            insert_data_pendidikan_.kampung_id            = kampung
            insert_data_pendidikan_.kodependidikan 		  = kodependidikan

            insert_data_pendidikan_.save()
            messages.success(request, 'Data Pemerintahan berhasil disimpan.')
            return redirect('profile:admin_pendidikan_detail',kodependidikan)
        messages.error(request, 'Data Pemerintahan gagal disimpan.')
        return render(request, 'profile/admin/pendidikan/create.html', {'form': form,'id':kodependidikan})


@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_distriks = dataDistrik.objects.get(id = id)

        context = {
            'title' : 'EDIT DISTRIK',
            'form' : form,
            'edit' : 'true',
            'data_distriks' : data_distriks,
        }
        template = 'profile/admin/distrik/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_distrik = dataDistrik.objects.get(id = id)
        title = request.POST.get('title')

        if data_distrik is not None:
            data_distrik.title = title
            data_distrik.save()
            data_distrik.save()

            messages.success(request, 'Distrik berhasil disimpan')
            return redirect('profile:admin_distrik')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPemerintahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPemerintahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPemerintahan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPemerintahan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def detail_pendidikan(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        detail_pendidikan = dataPendidikan.objects.get(id = id)
        dt_paud = dataPendidikanPaud.objects.filter(pendidikan = id)
        dt_sd = dataPendidikanSD.objects.filter(pendidikan = id)
        dt_smp = dataPendidikanSMP.objects.filter(pendidikan = id)
        archives = dataPendidikanPaud.objects.filter(status='Tidak aktif', pendidikan = id)
        arsipsd = dataPendidikanSD.objects.filter(status='Tidak aktif', pendidikan = id)
        arsipsmp = dataPendidikanSMP.objects.filter(status='Tidak aktif', pendidikan = id)
        
        context = {
            'title' : 'DETAIL',
            'form' : form,
            'edit' : 'true',
            'dt_item_pendidikan' : detail_pendidikan,
            'loop_paud' : dt_paud,
            'loop_sd' :dt_sd,
            'loop_smp' :dt_smp,
            'archives_paud' : archives,
            'arsipsd' : arsipsd,
            'arsipsmp' :arsipsmp,
            
        }
        template = 'profile/admin/pendidikan/detail.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        kodependidikan = request.POST.get('kodependidikan')
        pendidikan = request.POST.get('pendidikan')
        namasekolah = request.POST.get('namasekolah')        
        jml_ruang_belajar = request.POST.get('jml_ruang_belajar')
        jml_murid = request.POST.get('jml_murid')
        jml_guru = request.POST.get('jml_guru')
        jml_guru_oap = request.POST.get('jml_guru_oap')
        jml_guru_nonoap = request.POST.get('jml_guru_nonoap')
        jml_meja_kursi = request.POST.get('jml_meja_kursi')
        foto_paud = request.FILES['foto_paud']

        if kodependidikan is not None:
            insert_data_pendidikan_paud = dataPendidikanPaud()
            insert_data_pendidikan_paud.kodependidikan = kodependidikan   
            insert_data_pendidikan_paud.pendidikan_id = pendidikan  
            insert_data_pendidikan_paud.namasekolah = namasekolah          
            insert_data_pendidikan_paud.jml_ruang_belajar = jml_ruang_belajar 
            insert_data_pendidikan_paud.jml_murid = jml_murid  
            insert_data_pendidikan_paud.jml_guru = jml_guru  
            insert_data_pendidikan_paud.jml_guru_oap = jml_guru_oap  
            insert_data_pendidikan_paud.jml_guru_nonoap = jml_guru_nonoap  
            insert_data_pendidikan_paud.jml_meja_kursi = jml_meja_kursi  
            insert_data_pendidikan_paud.foto_paud = foto_paud  
            insert_data_pendidikan_paud.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan_detail',pendidikan)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pendidikan/detail.html',pendidikan, {'form': form,})
    
@login_required
@is_verified()
def edit_paud(request, id):

    edit_dataPaud = get_object_or_404(dataPendidikanPaud, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataPendidikanPaud.foto_paud}"
        foto_old = bool(dataPendidikanPaud.foto_paud)
        image = request.FILES.get('foto_paud')

        pendidikan = request.FILES.get('pendidikan')
        namasekolah = request.POST.get('namasekolah')        
        jml_ruang_belajar = request.POST.get('jml_ruang_belajar')
        jml_murid = request.POST.get('jml_murid')
        jml_guru = request.POST.get('jml_guru')
        jml_guru_oap = request.POST.get('jml_guru_oap')
        jml_guru_nonoap = request.POST.get('jml_guru_nonoap')
        jml_meja_kursi = request.POST.get('jml_meja_kursi')

        if edit_dataPaud is not None:
           
            edit_dataPaud.namasekolah = namasekolah          
            edit_dataPaud.jml_ruang_belajar = jml_ruang_belajar 
            edit_dataPaud.jml_murid = jml_murid  
            edit_dataPaud.jml_guru = jml_guru  
            edit_dataPaud.jml_guru_oap = jml_guru_oap  
            edit_dataPaud.jml_guru_nonoap = jml_guru_nonoap  
            edit_dataPaud.jml_meja_kursi = jml_meja_kursi  
            
           
            edit_dataPaud.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataPendidikanPaud.objects.get(id = id)
                form_foto.foto_paud = image
                form_foto.save()

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

# edit data SD
@login_required
@is_verified()
def edit_data_sd(request, id):

    edit_dataSD = get_object_or_404(dataPendidikanSD, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataPendidikanSD.foto_sd}"
        foto_old = bool(dataPendidikanSD.foto_sd)
        image = request.FILES.get('foto_sd')

        namasekolah = request.POST.get('namasekolah')
        jml_ruang_belajar  = request.POST.get('jml_ruang_belajar')
        jml_perpustakaan  = request.POST.get('jml_perpustakaan')
        jml_kantor  = request.POST.get('jml_kantor')
        jml_wc	  = request.POST.get('jml_wc')
        jml_papan_tulis  = request.POST.get('jml_papan_tulis')
        jml_pc	  = request.POST.get('jml_pc')
        jml_guru  = request.POST.get('jml_guru')
        jml_guru_oap  = request.POST.get('jml_guru_oap')
        jml_guru_non_oap  = request.POST.get('jml_guru_non_oap')
        jml_murid		  = request.POST.get('jml_murid')
        jml_murid_kelas_I  = request.POST.get('jml_murid_kelas_I')
        jml_murid_kelas_II  = request.POST.get('jml_murid_kelas_II')
        jml_murid_kelas_III  = request.POST.get('jml_murid_kelas_III')
        jml_murid_kelas_IV  = request.POST.get('jml_murid_kelas_IV')
        jml_murid_kelas_V  = request.POST.get('jml_murid_kelas_V')
        jml_murid_kelas_VI  = request.POST.get('jml_murid_kelas_VI')
        jml_meja_kursi	  = request.POST.get('jml_meja_kursi')
        jml_meja_kursi_I  = request.POST.get('jml_meja_kursi_I')
        jml_meja_kursi_II  = request.POST.get('jml_meja_kursi_II')
        jml_meja_kursi_III  = request.POST.get('jml_meja_kursi_III')
        jml_meja_kursi_IV  = request.POST.get('jml_meja_kursi_IV')
        jml_meja_kursi_V  = request.POST.get('jml_meja_kursi_V')
        jml_meja_kursi_VI  = request.POST.get('jml_meja_kursi_VI')
        jml_buku_paket	  = request.POST.get('jml_buku_paket')
        jml_buku_paket_I  = request.POST.get('jml_buku_paket_I')
        jml_buku_paket_II  = request.POST.get('jml_buku_paket_II')
        jml_buku_paket_III  = request.POST.get('jml_buku_paket_III')
        jml_buku_paket_IV  = request.POST.get('jml_buku_pake_IV')
        jml_buku_paket_V  = request.POST.get('jml_buku_paket_V')
        jml_buku_paket_VI  = request.POST.get('jml_buku_paket_VI')

        if edit_dataSD is not None:
            edit_dataSD.namasekolah = namasekolah
            edit_dataSD.jml_ruang_belajar = jml_ruang_belajar
            edit_dataSD.jml_perpustakaan = jml_perpustakaan
            edit_dataSD.jml_kantor = jml_kantor
            edit_dataSD.jml_wc	 = jml_wc	
            edit_dataSD.jml_papan_tulis = jml_papan_tulis
            edit_dataSD.jml_pc	 = jml_pc	
            edit_dataSD.jml_guru = jml_guru
            edit_dataSD.jml_guru_oap = jml_guru_oap
            edit_dataSD.jml_guru_non_oap = jml_guru_non_oap
            edit_dataSD.jml_murid		 = jml_murid		
            edit_dataSD.jml_murid_kelas_I = jml_murid_kelas_I
            edit_dataSD.jml_murid_kelas_II = jml_murid_kelas_II
            edit_dataSD.jml_murid_kelas_III = jml_murid_kelas_III
            edit_dataSD.jml_murid_kelas_IV = jml_murid_kelas_IV
            edit_dataSD.jml_murid_kelas_V = jml_murid_kelas_V
            edit_dataSD.jml_murid_kelas_VI = jml_murid_kelas_VI
            edit_dataSD.jml_meja_kursi	 = jml_meja_kursi	
            edit_dataSD.jml_meja_kursi_I = jml_meja_kursi_I
            edit_dataSD.jml_meja_kursi_II = jml_meja_kursi_II
            edit_dataSD.jml_meja_kursi_III = jml_meja_kursi_III
            edit_dataSD.jml_meja_kursi_IV = jml_meja_kursi_IV
            edit_dataSD.jml_meja_kursi_V = jml_meja_kursi_V
            edit_dataSD.jml_meja_kursi_VI = jml_meja_kursi_VI
            edit_dataSD.jml_buku_paket	 = jml_buku_paket	
            edit_dataSD.jml_buku_paket_I = jml_buku_paket_I
            edit_dataSD.jml_buku_paket_II = jml_buku_paket_II
            edit_dataSD.jml_buku_paket_III = jml_buku_paket_III
            edit_dataSD.jml_buku_paket_IV = jml_buku_paket_IV
            edit_dataSD.jml_buku_paket_V = jml_buku_paket_V
            edit_dataSD.jml_buku_paket_VI = jml_buku_paket_VI   
           
            edit_dataSD.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataPendidikanSD.objects.get(id = id)
                form_foto.foto_sd = image
                form_foto.save()

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})
    
@login_required
@is_verified()
def softDeletePaud(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanPaud.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPendidikanPaud.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restorePaud(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanPaud.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPendidikanPaud.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

# edit data SMP
@login_required
@is_verified()
def edit_data_smp(request, id):

    edit_dataSMP = get_object_or_404(dataPendidikanSMP, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataPendidikanSMP.foto_smp}"
        foto_old = bool(dataPendidikanSMP.foto_smp)
        image = request.FILES.get('foto_smp')

        namasekolah = request.POST.get('namasekolah')
        jml_ruang_belajar  = request.POST.get('jml_ruang_belajar')
        jml_perpustakaan  = request.POST.get('jml_perpustakaan')
        jml_kantor  = request.POST.get('jml_kantor')
        jml_wc	  = request.POST.get('jml_wc')
        jml_papan_tulis  = request.POST.get('jml_papan_tulis')
        jml_pc	  = request.POST.get('jml_pc')
        jml_guru  = request.POST.get('jml_guru')
        jml_guru_oap  = request.POST.get('jml_guru_oap')
        jml_guru_non_oap  = request.POST.get('jml_guru_non_oap')
        jml_lab = request.POST.get('jml_lab')
        kebutuhan_guru = request.POST.get('kebutuhan_guru')
        kekurangan_guru = request.POST.get('kekurangan_guru')
        buku_paket_kurang_matapelajaran = request.POST.get('buku_paket_kurang_matapelajaran')
        jml_murid =request.POST.get('jml_murid')
        jml_murid_kelas_VII =request.POST.get('jml_murid_kelas_VII')
        jml_murid_kelas_VIII =request.POST.get('jml_murid_kelas_VIII')
        jml_murid_kelas_IX =request.POST.get('jml_murid_kelas_IX')
        jml_meja_kursi =request.POST.get('jml_meja_kursi')
        jml_meja_kursi_VII =request.POST.get('jml_meja_kursi_VII')
        jml_meja_kursi_VIII =request.POST.get('jml_meja_kursi_VIII')
        jml_meja_kursi_IX =request.POST.get('jml_meja_kursi_IX')
        jml_buku_paket =request.POST.get('jml_buku_paket')
        jml_buku_paket_VII =request.POST.get('jml_buku_paket_VII')
        jml_buku_paket_VIII =request.POST.get('jml_buku_paket_VIII')
        jml_buku_paket_IX =request.POST.get('jml_buku_paket_IX')
        

        if edit_dataSMP is not None:
            edit_dataSMP.namasekolah = namasekolah
            edit_dataSMP.jml_ruang_belajar = jml_ruang_belajar
            edit_dataSMP.jml_perpustakaan = jml_perpustakaan
            edit_dataSMP.jml_kantor = jml_kantor
            edit_dataSMP.jml_wc	 = jml_wc	
            edit_dataSMP.jml_papan_tulis = jml_papan_tulis
            edit_dataSMP.jml_pc	 = jml_pc	
            edit_dataSMP.jml_guru = jml_guru
            edit_dataSMP.jml_guru_oap = jml_guru_oap
            edit_dataSMP.jml_guru_non_oap = jml_guru_non_oap
            edit_dataSMP.jml_lab = jml_lab
            edit_dataSMP.buku_paket_kurang_matapelajaran = buku_paket_kurang_matapelajaran
            edit_dataSMP.kebutuhan_guru = kebutuhan_guru
            edit_dataSMP.kekurangan_guru= kekurangan_guru
            edit_dataSMP.jml_murid = jml_murid
            edit_dataSMP.jml_murid_kelas_VII = jml_murid_kelas_VII
            edit_dataSMP.jml_murid_kelas_VIII = jml_murid_kelas_VIII
            edit_dataSMP.jml_murid_kelas_IX = jml_murid_kelas_IX
            edit_dataSMP.jml_meja_kursi = jml_meja_kursi
            edit_dataSMP.jml_meja_kursi_VII = jml_meja_kursi_VII
            edit_dataSMP.jml_meja_kursi_VIII = jml_meja_kursi_VIII
            edit_dataSMP.jml_meja_kursi_IX = jml_meja_kursi_IX
            edit_dataSMP.jml_buku_paket = jml_buku_paket
            edit_dataSMP.jml_buku_paket_VII = jml_buku_paket_VII
            edit_dataSMP.jml_buku_paket_VIII = jml_buku_paket_VIII
            edit_dataSMP.jml_buku_paket_IX = jml_buku_paket_IX 
           
            edit_dataSMP.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataPendidikanSMP.objects.get(id = id)
                form_foto.foto_smp = image
                form_foto.save()

            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})

@login_required
@is_verified()
def softDeleteSMP(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanSMP.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPendidikanSMP.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restoreSMP(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanSMP.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPendidikanSMP.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def detail_pendidikan_sd(request, id):
    
    if request.method == 'POST':
       
        kodependidikan = request.POST.get('kodependidikan')
        pendidikan = request.POST.get('pendidikan')
        namasekolah = request.POST.get('namasekolah')
        jml_ruang_belajar  = request.POST.get('jml_ruang_belajar')
        jml_perpustakaan  = request.POST.get('jml_perpustakaan')
        jml_kantor  = request.POST.get('jml_kantor')
        jml_wc	  = request.POST.get('jml_wc')
        jml_papan_tulis  = request.POST.get('jml_papan_tulis')
        jml_pc	  = request.POST.get('jml_pc')
        jml_guru  = request.POST.get('jml_guru')
        jml_guru_oap  = request.POST.get('jml_guru_oap')
        jml_guru_non_oap  = request.POST.get('jml_guru_non_oap')
        jml_murid		  = request.POST.get('jml_murid')
        jml_murid_kelas_I  = request.POST.get('jml_murid_kelas_I')
        jml_murid_kelas_II  = request.POST.get('jml_murid_kelas_II')
        jml_murid_kelas_III  = request.POST.get('jml_murid_kelas_III')
        jml_murid_kelas_IV  = request.POST.get('jml_murid_kelas_IV')
        jml_murid_kelas_V  = request.POST.get('jml_murid_kelas_V')
        jml_murid_kelas_VI  = request.POST.get('jml_murid_kelas_VI')
        jml_meja_kursi	  = request.POST.get('jml_meja_kursi')
        jml_meja_kursi_I  = request.POST.get('jml_meja_kursi_I')
        jml_meja_kursi_II  = request.POST.get('jml_meja_kursi_II')
        jml_meja_kursi_III  = request.POST.get('jml_meja_kursi_III')
        jml_meja_kursi_IV  = request.POST.get('jml_meja_kursi_IV')
        jml_meja_kursi_V  = request.POST.get('jml_meja_kursi_V')
        jml_meja_kursi_VI  = request.POST.get('jml_meja_kursi_VI')
        jml_buku_paket	  = request.POST.get('jml_buku_paket')
        jml_buku_paket_I  = request.POST.get('jml_buku_paket_I')
        jml_buku_paket_II  = request.POST.get('jml_buku_paket_II')
        jml_buku_paket_III  = request.POST.get('jml_buku_paket_III')
        jml_buku_paket_IV  = request.POST.get('jml_buku_paket_IV')
        jml_buku_paket_V  = request.POST.get('jml_buku_paket_V')
        jml_buku_paket_VI  = request.POST.get('jml_buku_paket_VI')
               
        
        foto_sd = request.FILES['foto_sd']

        if kodependidikan is not None:
            insert_data_pendidikan_sd = dataPendidikanSD()
            insert_data_pendidikan_sd.kodependidikan = kodependidikan
            insert_data_pendidikan_sd.pendidikan_id =  pendidikan
            insert_data_pendidikan_sd.namasekolah = namasekolah
            insert_data_pendidikan_sd.jml_ruang_belajar = jml_ruang_belajar
            insert_data_pendidikan_sd.jml_perpustakaan = jml_perpustakaan
            insert_data_pendidikan_sd.jml_kantor = jml_kantor
            insert_data_pendidikan_sd.jml_wc	 = jml_wc	
            insert_data_pendidikan_sd.jml_papan_tulis = jml_papan_tulis
            insert_data_pendidikan_sd.jml_pc	 = jml_pc	
            insert_data_pendidikan_sd.jml_guru = jml_guru
            insert_data_pendidikan_sd.jml_guru_oap = jml_guru_oap
            insert_data_pendidikan_sd.jml_guru_non_oap = jml_guru_non_oap
            insert_data_pendidikan_sd.jml_murid		 = jml_murid		
            insert_data_pendidikan_sd.jml_murid_kelas_I = jml_murid_kelas_I
            insert_data_pendidikan_sd.jml_murid_kelas_II = jml_murid_kelas_II
            insert_data_pendidikan_sd.jml_murid_kelas_III = jml_murid_kelas_III
            insert_data_pendidikan_sd.jml_murid_kelas_IV = jml_murid_kelas_IV
            insert_data_pendidikan_sd.jml_murid_kelas_V = jml_murid_kelas_V
            insert_data_pendidikan_sd.jml_murid_kelas_VI = jml_murid_kelas_VI
            insert_data_pendidikan_sd.jml_meja_kursi	 = jml_meja_kursi	
            insert_data_pendidikan_sd.jml_meja_kursi_I = jml_meja_kursi_I
            insert_data_pendidikan_sd.jml_meja_kursi_II = jml_meja_kursi_II
            insert_data_pendidikan_sd.jml_meja_kursi_III = jml_meja_kursi_III
            insert_data_pendidikan_sd.jml_meja_kursi_IV = jml_meja_kursi_IV
            insert_data_pendidikan_sd.jml_meja_kursi_V = jml_meja_kursi_V
            insert_data_pendidikan_sd.jml_meja_kursi_VI = jml_meja_kursi_VI
            insert_data_pendidikan_sd.jml_buku_paket	 = jml_buku_paket	
            insert_data_pendidikan_sd.jml_buku_paket_I = jml_buku_paket_I
            insert_data_pendidikan_sd.jml_buku_paket_II = jml_buku_paket_II
            insert_data_pendidikan_sd.jml_buku_paket_III = jml_buku_paket_III
            insert_data_pendidikan_sd.jml_buku_paket_IV = jml_buku_paket_IV
            insert_data_pendidikan_sd.jml_buku_paket_V = jml_buku_paket_V
            insert_data_pendidikan_sd.jml_buku_paket_VI = jml_buku_paket_VI
    
            insert_data_pendidikan_sd.foto_sd = foto_sd  
            insert_data_pendidikan_sd.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan_detail',pendidikan)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pendidikan/detail.html',pendidikan)
    
@login_required
@is_verified()
def softDeleteSD(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanSD.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPendidikanSD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restoreSD(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPendidikanSD.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPendidikanSD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
@login_required
@is_verified()
def detail_pendidikan_smp(request, id):
    
    if request.method == 'POST':
       
        kodependidikan = request.POST.get('kodependidikan')
        pendidikan = request.POST.get('pendidikan')
        namasekolah = request.POST.get('namasekolah')
        jml_ruang_belajar  = request.POST.get('jml_ruang_belajar')
        jml_perpustakaan  = request.POST.get('jml_perpustakaan')
        jml_kantor  = request.POST.get('jml_kantor')
        jml_wc	  = request.POST.get('jml_wc')
        jml_papan_tulis  = request.POST.get('jml_papan_tulis')
        jml_pc	  = request.POST.get('jml_pc')
        jml_guru  = request.POST.get('jml_guru')
        jml_guru_oap  = request.POST.get('jml_guru_oap')
        jml_guru_non_oap  = request.POST.get('jml_guru_non_oap')
        jml_lab = request.POST.get('jml_lab')
        kebutuhan_guru = request.POST.get('kebutuhan_guru')
        kekurangan_guru = request.POST.get('kekurangan_guru')
        buku_paket_kurang_matapelajaran = request.POST.get('buku_paket_kurang_matapelajaran')
        jml_murid =request.POST.get('jml_murid')
        jml_murid_kelas_VII =request.POST.get('jml_murid_kelas_VII')
        jml_murid_kelas_VIII =request.POST.get('jml_murid_kelas_VIII')
        jml_murid_kelas_IX =request.POST.get('jml_murid_kelas_IX')
        jml_meja_kursi =request.POST.get('jml_meja_kursi')
        jml_meja_kursi_VII =request.POST.get('jml_meja_kursi_VII')
        jml_meja_kursi_VIII =request.POST.get('jml_meja_kursi_VIII')
        jml_meja_kursi_IX =request.POST.get('jml_meja_kursi_IX')
        jml_buku_paket =request.POST.get('jml_buku_paket')
        jml_buku_paket_VII =request.POST.get('jml_buku_paket_VII')
        jml_buku_paket_VIII =request.POST.get('jml_buku_paket_VIII')
        jml_buku_paket_IX =request.POST.get('jml_buku_paket_IX')
               
        
        foto_smp = request.FILES['foto_smp']

        if kodependidikan is not None:
            insert_data_pendidikan_smp = dataPendidikanSMP()
            insert_data_pendidikan_smp.kodependidikan = kodependidikan
            insert_data_pendidikan_smp.pendidikan_id =  pendidikan
            insert_data_pendidikan_smp.namasekolah = namasekolah
            insert_data_pendidikan_smp.jml_ruang_belajar = jml_ruang_belajar
            insert_data_pendidikan_smp.jml_perpustakaan = jml_perpustakaan
            insert_data_pendidikan_smp.jml_kantor = jml_kantor
            insert_data_pendidikan_smp.jml_wc	 = jml_wc	
            insert_data_pendidikan_smp.jml_papan_tulis = jml_papan_tulis
            insert_data_pendidikan_smp.jml_pc	 = jml_pc	
            insert_data_pendidikan_smp.jml_guru = jml_guru
            insert_data_pendidikan_smp.jml_guru_oap = jml_guru_oap
            insert_data_pendidikan_smp.jml_guru_non_oap = jml_guru_non_oap
            insert_data_pendidikan_smp.jml_lab = jml_lab
            insert_data_pendidikan_smp.buku_paket_kurang_matapelajaran = buku_paket_kurang_matapelajaran
            insert_data_pendidikan_smp.kebutuhan_guru = kebutuhan_guru
            insert_data_pendidikan_smp.kekurangan_guru= kekurangan_guru
            insert_data_pendidikan_smp.jml_murid = jml_murid
            insert_data_pendidikan_smp.jml_murid_kelas_VII = jml_murid_kelas_VII
            insert_data_pendidikan_smp.jml_murid_kelas_VIII = jml_murid_kelas_VIII
            insert_data_pendidikan_smp.jml_murid_kelas_IX = jml_murid_kelas_IX
            insert_data_pendidikan_smp.jml_meja_kursi = jml_meja_kursi
            insert_data_pendidikan_smp.jml_meja_kursi_VII = jml_meja_kursi_VII
            insert_data_pendidikan_smp.jml_meja_kursi_VIII = jml_meja_kursi_VIII
            insert_data_pendidikan_smp.jml_meja_kursi_IX = jml_meja_kursi_IX
            insert_data_pendidikan_smp.jml_buku_paket = jml_buku_paket
            insert_data_pendidikan_smp.jml_buku_paket_VII = jml_buku_paket_VII
            insert_data_pendidikan_smp.jml_buku_paket_VIII = jml_buku_paket_VIII
            insert_data_pendidikan_smp.jml_buku_paket_IX = jml_buku_paket_IX
          	
           
    
            insert_data_pendidikan_smp.foto_smp = foto_smp  
            insert_data_pendidikan_smp.save()
            
            messages.success(request, 'Data berhasil disimpan')
            return redirect('profile:admin_pendidikan_detail',pendidikan)

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pendidikan/detail.html',pendidikan)


                    
        
                    