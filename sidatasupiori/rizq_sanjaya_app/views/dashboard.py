from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connections
from django.db.models import Q, Count
from ..helpers import dictfetchall, get_list_berita, get_category, get_list_galeri
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import os
import pprint
from django.template.loader import render_to_string
import requests

from rizq_sanjaya_app.decorators import is_verified

@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
#     profilSKPD_home = ProfilSKPD.objects.exclude(typexs='Misi').values('typexs').annotate(count=Count('typexs')).order_by('typexs')
#     layanan_skpd = LayananDinas.objects.exclude(status='Draft').annotate(count=Count('typexs'))
#     news =  News.objects.filter(deleted_at = None).order_by('-id')
#     news_side_home =  News.objects.filter(deleted_at = None).order_by('-id')[:5]
#     akses_link_home = Aksesbilitas.objects.exclude(status ='Draft')
#     home_agenda = InfoTerpadu.objects.filter(typexs='agenda',status='Publish')
#     context = {
         
#          'home_news':news,
#          'home_news_side':news_side_home,
#          'home_profilSKPD':profilSKPD_home, 
#          'home_starndar_pelayanan' :layanan_skpd,
#          'home_link_external':akses_link_home,
#          'home_agenda': home_agenda,
         
       
#     }
#     return render(request, 'profile/layouts/frontend/base.html', context)
      return render(request, 'profile/admin/index.html')
