from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    data_perhubungan = dataPerhubungan.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataPerhubungan.objects.filter(status='Tidak aktif')
    paginator = Paginator(data_perhubungan, 15)
    try:
        data_perhubungan = paginator.page(page)
    except PageNotAnInteger:
        data_perhubungan = paginator.page(1)
    except EmptyPage:
        data_perhubungan = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data Perhubungan',
        'data_perhubungan_list' : data_perhubungan,
        'archives' : archives,
        'datakampung_list' : kampung,
        'penanggungjawab':penanggungjawabs,
    }
    
    return render(request, 'profile/admin/perhubungan/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataPerhubungan.objects.all()
        data_kominfo_ = dataPerhubungan.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data Kominfo',
            'form' : form,
            'data_kominfo_list':data_kominfo_,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/perhubungan/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        

        jml_halte	 			    = request.POST.get('jml_halte') 
        jml_terminal	 			= request.POST.get('jml_terminal') 
        jml_dermaga		 			= request.POST.get('jml_dermaga') 
        jml_tambatan_perahu		 	= request.POST.get('jml_tambatan_perahu') 
        kondisi_dermaga			 	= request.POST.get('kondisi_dermaga')
        kondisi_tambatan_perahu		= request.POST.get('kondisi_tambatan_perahu')
        foto_halte 					=  request.FILES['foto_halte']
        foto_terminal 				=  request.FILES['foto_terminal']
        foto_dermaga 				=  request.FILES['foto_dermaga']
        foto_tambatan_perahu 		=  request.FILES['foto_tambatan_perahu']

        if kampung is not None:
            data_perhubungan_ = dataPerhubungan()
            data_perhubungan_.tahun = tahun
            data_perhubungan_.penanggungjawab_id = penanggungjawab_id
            data_perhubungan_.kampung_id = kampung

            data_perhubungan_.jml_halte	 			    = jml_halte	 			    
            data_perhubungan_.jml_terminal	 			= jml_terminal	 		
            data_perhubungan_.jml_dermaga		 		= jml_dermaga		 			
            data_perhubungan_.jml_tambatan_perahu		= jml_tambatan_perahu		 	
            data_perhubungan_.kondisi_dermaga			= kondisi_dermaga		 
            data_perhubungan_.kondisi_tambatan_perahu	= kondisi_tambatan_perahu	
            data_perhubungan_.foto_halte 				= foto_halte 				
            data_perhubungan_.foto_terminal 			= foto_terminal 			
            data_perhubungan_.foto_dermaga 				= foto_dermaga 			
            data_perhubungan_.foto_tambatan_perahu 		= foto_tambatan_perahu 	

            data_perhubungan_.save()
            messages.success(request, 'Data perhubungan berhasil disimpan.')
            return redirect('profile:admin_perhubungan')
        messages.error(request, 'Data perhubungan gagal disimpan.')
        return render(request, 'profile/admin/perhubungan/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    edit_dataperhubungan = get_object_or_404(dataPerhubungan, id=id)
    form = ''
    
    
    if request.method == 'POST':

        # foto_halte 					=  request.FILES['foto_halte']
        path_file_lama_foto_halte = f"{settings.MEDIA_ROOT}/{dataPerhubungan.foto_halte}"
        foto_old_foto_halte = bool(dataPerhubungan.foto_halte)
        image_foto_halte = request.FILES.get('foto_halte')

        # foto_terminal 				=  request.FILES['foto_terminal']
        path_file_lama_foto_terminal = f"{settings.MEDIA_ROOT}/{dataPerhubungan.foto_terminal}"
        foto_old_foto_terminal = bool(dataPerhubungan.foto_terminal)
        image_foto_terminal = request.FILES.get('foto_terminal')

        # foto_dermaga 				=  request.FILES['foto_dermaga']
        path_file_lama_foto_dermaga = f"{settings.MEDIA_ROOT}/{dataPerhubungan.foto_dermaga}"
        foto_old_foto_dermaga = bool(dataPerhubungan.foto_dermaga)
        image_foto_dermaga = request.FILES.get('foto_dermaga')

        # foto_tambatan_perahu 		=  request.FILES['foto_tambatan_perahu']
        path_file_lama_foto_tambatan_perahu = f"{settings.MEDIA_ROOT}/{dataPerhubungan.foto_tambatan_perahu}"
        foto_old_foto_tambatan_perahu = bool(dataPerhubungan.foto_tambatan_perahu)
        image_foto_tambatan_perahu = request.FILES.get('foto_tambatan_perahu')

        


        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jml_halte	 			    = request.POST.get('jml_halte') 
        jml_terminal	 			= request.POST.get('jml_terminal') 
        jml_dermaga		 			= request.POST.get('jml_dermaga') 
        jml_tambatan_perahu		 	= request.POST.get('jml_tambatan_perahu') 
        kondisi_dermaga			 	= request.POST.get('kondisi_dermaga')
        kondisi_tambatan_perahu		= request.POST.get('kondisi_tambatan_perahu')
        

        if edit_dataperhubungan is not None:
            edit_dataperhubungan.tahun                 = tahun
            edit_dataperhubungan.penanggungjawab_id    = penanggungjawab_id
            edit_dataperhubungan.kampung_id            = kampung

            edit_dataperhubungan.jml_halte	 			    = jml_halte	 			    
            edit_dataperhubungan.jml_terminal	 			= jml_terminal	 		
            edit_dataperhubungan.jml_dermaga		 		= jml_dermaga		 			
            edit_dataperhubungan.jml_tambatan_perahu		= jml_tambatan_perahu		 	
            edit_dataperhubungan.kondisi_dermaga			= kondisi_dermaga		 
            edit_dataperhubungan.kondisi_tambatan_perahu	= kondisi_tambatan_perahu	
            
            edit_dataperhubungan.save()
            
            # foto_halte 					=  request.FILES['foto_halte']
            if len(request.FILES) > 0:
                if foto_old_foto_halte : 
                    try:
                        os.remove(path_file_lama_foto_halte)
                    except:
                        pass
                form_foto_halte = dataPerhubungan.objects.get(id = id)
                form_foto_halte.foto_halte = image_foto_halte
                form_foto_halte.save()
            # foto_terminal 				=  request.FILES['foto_terminal']
            if len(request.FILES) > 0:
                if foto_old_foto_terminal : 
                    try:
                        os.remove(path_file_lama_foto_terminal)
                    except:
                        pass
                form_foto_terminal = dataPerhubungan.objects.get(id = id)
                form_foto_terminal.foto_terminal = image_foto_terminal
                form_foto_terminal.save()
            # foto_dermaga 				=  request.FILES['foto_dermaga']
            if len(request.FILES) > 0:
                if foto_old_foto_dermaga : 
                    try:
                        os.remove(path_file_lama_foto_dermaga)
                    except:
                        pass
                form_foto_dermaga = dataPerhubungan.objects.get(id = id)
                form_foto_dermaga.foto_dermaga = image_foto_dermaga
                form_foto_dermaga.save()
            # foto_tambatan_perahu 		=  request.FILES['foto_tambatan_perahu']
            if len(request.FILES) > 0:
                if foto_old_foto_tambatan_perahu : 
                    try:
                        os.remove(path_file_lama_foto_tambatan_perahu)
                    except:
                        pass
                form_foto_tambatan_perahu = dataPerhubungan.objects.get(id = id)
                form_foto_tambatan_perahu.foto_tambatan_perahu = image_foto_tambatan_perahu
                form_foto_tambatan_perahu.save()
            

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_perhubungan')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerhubungan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataPerhubungan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataPerhubungan.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataPerhubungan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)