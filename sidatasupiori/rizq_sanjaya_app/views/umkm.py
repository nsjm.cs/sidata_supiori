from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kampung = dataKampung.objects.select_related('distrik').all()
    penanggungjawabs = datapenanggungjawab.objects.all()
    UMKM = dataUMKM.objects.prefetch_related('kampung').filter(status='Aktif')
    archives = dataUMKM.objects.filter(status='Tidak aktif')
    paginator = Paginator(UMKM, 15)
    try:
        UMKM = paginator.page(page)
    except PageNotAnInteger:
        UMKM = paginator.page(1)
    except EmptyPage:
        UMKM = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Data UMKM',
        'UMKM' : UMKM,
        'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/umkm/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = dataUMKM.objects.all()
        umkm = dataUMKM.objects.all()
        kampung = dataKampung.objects.select_related('distrik').all()
        penanggungjawabs = datapenanggungjawab.objects.all()
        context = {
            'title' : 'Data UMKM',
            'form' : form,
            'umkmkoperasi':umkm,
            'datakampung_list':kampung,
            'datajawab_list':penanggungjawabs,
        }

        template = 'profile/admin/umkm/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')
        jeniinput = request.POST.get('jeniinput')
        jenis_usaha = request.POST.get('jenis_usaha')
        title_usaha = request.POST.get('title_usaha')
        title_owner = request.POST.get('title_owner')
        modal_usaha = request.POST.get('modal_usaha')
        izin_usaha = request.POST.get('izin_usaha')
        akta_usaha = request.POST.get('akta_usaha')
        bantuan = request.POST.get('bantuan')
        jml_karyawan = request.POST.get('jml_karyawan')
        foto = request.FILES['foto']
       
        if kampung is not None:
            data_umkm = dataUMKM()
            data_umkm.tahun = tahun
            data_umkm.penanggungjawab_id = penanggungjawab_id
            data_umkm.jeniinput = jeniinput
            data_umkm.title_owner = title_owner
            data_umkm.title_usaha = title_usaha
            data_umkm.jenis_usaha = jenis_usaha
            data_umkm.modal_usaha = modal_usaha
            data_umkm.izin_usaha = izin_usaha
            data_umkm.akta_usaha = akta_usaha
            data_umkm.bantuan = bantuan
            data_umkm.jml_karyawan = jml_karyawan
            data_umkm.foto = foto
            data_umkm.kampung_id = kampung
            data_umkm.save()
            messages.success(request, 'Data umkm berhasil disimpan.')
            return redirect('profile:admin_umkm')
        messages.error(request, 'Data umkm gagal disimpan.')
        return render(request, 'profile/admin/umkm/create.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    edit_dataumkm = get_object_or_404(dataUMKM, id=id)
    form = ''
    
    
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{dataUMKM.foto}"
        foto_old = bool(dataUMKM.foto)
        image = request.FILES.get('foto')


        tahun = request.POST.get('tahun')
        kampung = request.POST.get('kampung')
        penanggungjawab_id = request.POST.get('penanggungjawab')

        jeniinput = request.POST.get('jeniinput')
        jenis_usaha = request.POST.get('jenis_usaha')
        title_usaha = request.POST.get('title_usaha')
        title_owner = request.POST.get('title_owner')
        modal_usaha = request.POST.get('modal_usaha')
        izin_usaha = request.POST.get('izin_usaha')
        akta_usaha = request.POST.get('akta_usaha')
        bantuan = request.POST.get('bantuan')
        jml_karyawan = request.POST.get('jml_karyawan')
        

        if edit_dataumkm is not None:
            edit_dataumkm.tahun                 = tahun
            edit_dataumkm.penanggungjawab_id    = penanggungjawab_id
            edit_dataumkm.kampung_id            = kampung
            edit_dataumkm.jeniinput = jeniinput
            edit_dataumkm.title_owner = title_owner
            edit_dataumkm.title_usaha = title_usaha
            edit_dataumkm.jenis_usaha = jenis_usaha
            edit_dataumkm.modal_usaha = modal_usaha
            edit_dataumkm.izin_usaha = izin_usaha
            edit_dataumkm.akta_usaha = akta_usaha
            edit_dataumkm.bantuan = bantuan
            edit_dataumkm.jml_karyawan = jml_karyawan
           

            edit_dataumkm.save()
            if len(request.FILES) > 0:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_foto = dataUMKM.objects.get(id = id)
                form_foto.foto = image
                form_foto.save()

            messages.success(request, 'Data berhasil diubah')
            return redirect('profile:admin_umkm')

        messages.error(request, 'Distrik gagal disimpan.')
        return render(request, 'profile/admin/distrik/create.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataUMKM.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except dataUMKM.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = dataUMKM.objects.get(id=id)
        doc.create_date = sekarang
        doc.status = 'Aktif'
        doc.save()
        message = 'success'
    except dataUMKM.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)