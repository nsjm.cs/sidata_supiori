function gb_tampilkan(nama, slug){
    Swal.fire({
        title: `Apakah anda yakin ingin menampilkan ${nama}?`,
        showCancelButton: true,
        confirmButtonText: 'Tampilkan',
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/guestbook/tampilkan/${slug}`,
                data: {},
                dataType: "html",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        onOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    Swal.fire(`Buku tamu berhasil ditampilkan!`, '', 'success');
                    setTimeout(function(){
                        window.location.href = `/manage_panel/guestbook`;
                    }, 2000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(`Buku tamu gagal ditampilkan!`, '', 'error');
                }
            });
            
        }
    });
}
function gb_sembunyikan(nama,  slug){
    Swal.fire({
        title: `Apakah anda yakin ingin menyembunyikan ${nama}?`,
        showCancelButton: true,
        confirmButtonText: 'Sembunyikan',
        cancelButtonText: 'Batal',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `/manage_panel/guestbook/sembunyikan/${slug}`,
                data: {},
                dataType: "html",
                timeout: 10000,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Memproses.........',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                    Swal.showLoading();
                },
                success: function(response){
                    Swal.fire(`Buku tamu berhasil disembunyikan!`, '', 'success');
                    setTimeout(function(){
                        window.location.href =  `/manage_panel/guestbook`;
                    }, 2000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(`Buku tamu gagal disembunyikan!`, '', 'error');
                }
            });
            
        }
    });
}